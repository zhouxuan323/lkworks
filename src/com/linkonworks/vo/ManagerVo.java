package com.linkonworks.vo;

public class ManagerVo {
    private String manid;

    private String manname;

    private String manpassword;

    public String getManid() {
        return manid;
    }

    public void setManid(String manid) {
        this.manid = manid == null ? null : manid.trim();
    }

    public String getManname() {
        return manname;
    }

    public void setManname(String manname) {
        this.manname = manname == null ? null : manname.trim();
    }

    public String getManpassword() {
        return manpassword;
    }

    public void setManpassword(String manpassword) {
        this.manpassword = manpassword == null ? null : manpassword.trim();
    }
}
