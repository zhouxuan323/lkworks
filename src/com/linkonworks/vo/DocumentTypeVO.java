package com.linkonworks.vo;

public class DocumentTypeVO {
	private String dtid;

	private String lng;

	private String mid;

	private String describe;

	private String modelid;

	private Long width;

	private Long height;

	private Long istime;

	private Long time;

	private String title;

	private String transferurl;
	
	
	private String pmodelid;


	public String getDtid() {
		return dtid;
	}


	public void setDtid(String dtid) {
		this.dtid = dtid;
	}


	public String getLng() {
		return lng;
	}


	public void setLng(String lng) {
		this.lng = lng;
	}


	public String getMid() {
		return mid;
	}


	public void setMid(String mid) {
		this.mid = mid;
	}


	public String getDescribe() {
		return describe;
	}


	public void setDescribe(String describe) {
		this.describe = describe;
	}


	public String getModelid() {
		return modelid;
	}


	public void setModelid(String modelid) {
		this.modelid = modelid;
	}


	public Long getWidth() {
		return width;
	}


	public void setWidth(Long width) {
		this.width = width;
	}


	public Long getHeight() {
		return height;
	}


	public void setHeight(Long height) {
		this.height = height;
	}


	public Long getIstime() {
		return istime;
	}


	public void setIstime(Long istime) {
		this.istime = istime;
	}


	public Long getTime() {
		return time;
	}


	public void setTime(Long time) {
		this.time = time;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getTransferurl() {
		return transferurl;
	}


	public void setTransferurl(String transferurl) {
		this.transferurl = transferurl;
	}


	public String getPmodelid() {
		return pmodelid;
	}


	public void setPmodelid(String pmodelid) {
		this.pmodelid = pmodelid;
	}

	

	
}