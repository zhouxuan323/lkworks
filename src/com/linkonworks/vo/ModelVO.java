package com.linkonworks.vo;

public class ModelVO {
	 private String modelid;

	    private String describe;

	    private String url;

	    private String filename;

	    private String dispatcher;

	    private String pmodelid;

	    public String getModelid() {
	        return modelid;
	    }

	    public void setModelid(String modelid) {
	        this.modelid = modelid == null ? null : modelid.trim();
	    }

	    public String getDescribe() {
	        return describe;
	    }

	    public void setDescribe(String describe) {
	        this.describe = describe == null ? null : describe.trim();
	    }

	    public String getUrl() {
	        return url;
	    }

	    public void setUrl(String url) {
	        this.url = url == null ? null : url.trim();
	    }

	    public String getFilename() {
	        return filename;
	    }

	    public void setFilename(String filename) {
	        this.filename = filename == null ? null : filename.trim();
	    }

	    public String getDispatcher() {
	        return dispatcher;
	    }

	    public void setDispatcher(String dispatcher) {
	        this.dispatcher = dispatcher == null ? null : dispatcher.trim();
	    }

	    public String getPmodelid() {
	        return pmodelid;
	    }

	    public void setPmodelid(String pmodelid) {
	        this.pmodelid = pmodelid == null ? null : pmodelid.trim();
	    }
}