package com.linkonworks.vo;

public class DocumentVO {
    private String daid;

    private String lng;

    private String dtid;

    private String title;

    private String titleurl;

    private String titleclass;

    private String subheading;

    private String subheadingclass;

    private String subheadingurl;

    private String content;

    private String contentclass;

    private String contenturl;

    private String transferurl;

    private String imgurl;

    private String imgfilename;

    private String imglink;

    private Short istime;

    private Long starttime;

    private Long endtime;

    private Long addtime;

    private Long sequence;

    public String getDaid() {
        return daid;
    }

    public void setDaid(String daid) {
        this.daid = daid == null ? null : daid.trim();
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng == null ? null : lng.trim();
    }

    public String getDtid() {
        return dtid;
    }

    public void setDtid(String dtid) {
        this.dtid = dtid == null ? null : dtid.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getTitleurl() {
        return titleurl;
    }

    public void setTitleurl(String titleurl) {
        this.titleurl = titleurl == null ? null : titleurl.trim();
    }

    public String getTitleclass() {
        return titleclass;
    }

    public void setTitleclass(String titleclass) {
        this.titleclass = titleclass == null ? null : titleclass.trim();
    }

    public String getSubheading() {
        return subheading;
    }

    public void setSubheading(String subheading) {
        this.subheading = subheading == null ? null : subheading.trim();
    }

    public String getSubheadingclass() {
        return subheadingclass;
    }

    public void setSubheadingclass(String subheadingclass) {
        this.subheadingclass = subheadingclass == null ? null : subheadingclass.trim();
    }

    public String getSubheadingurl() {
        return subheadingurl;
    }

    public void setSubheadingurl(String subheadingurl) {
        this.subheadingurl = subheadingurl == null ? null : subheadingurl.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getContentclass() {
        return contentclass;
    }

    public void setContentclass(String contentclass) {
        this.contentclass = contentclass == null ? null : contentclass.trim();
    }

    public String getContenturl() {
        return contenturl;
    }

    public void setContenturl(String contenturl) {
        this.contenturl = contenturl == null ? null : contenturl.trim();
    }

    public String getTransferurl() {
        return transferurl;
    }

    public void setTransferurl(String transferurl) {
        this.transferurl = transferurl == null ? null : transferurl.trim();
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl == null ? null : imgurl.trim();
    }

    public String getImgfilename() {
        return imgfilename;
    }

    public void setImgfilename(String imgfilename) {
        this.imgfilename = imgfilename == null ? null : imgfilename.trim();
    }

    public String getImglink() {
        return imglink;
    }

    public void setImglink(String imglink) {
        this.imglink = imglink == null ? null : imglink.trim();
    }

    public Short getIstime() {
        return istime;
    }

    public void setIstime(Short istime) {
        this.istime = istime;
    }

    public Long getStarttime() {
        return starttime;
    }

    public void setStarttime(Long starttime) {
        this.starttime = starttime;
    }

    public Long getEndtime() {
        return endtime;
    }

    public void setEndtime(Long endtime) {
        this.endtime = endtime;
    }

    public Long getAddtime() {
        return addtime;
    }

    public void setAddtime(Long addtime) {
        this.addtime = addtime;
    }

    public Long getSequence() {
        return sequence;
    }

    public void setSequence(Long sequence) {
        this.sequence = sequence;
    }
}