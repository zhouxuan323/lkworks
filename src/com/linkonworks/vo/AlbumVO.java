package com.linkonworks.vo;

import java.io.File;
import java.util.List;

public class AlbumVO {
	private String adid;

	private String lng;

	private String dtid;

	private String type;

	private String title;

	private String content;

	private String imgurl;

	private String imgfilename;

	private String imgalt;

	private String click;

	private Short istime;

	private Long starttime;

	private Long endtime;

	private Long addtime;

	public String getAdid() {
		return adid;
	}

	public void setAdid(String adid) {
		this.adid = adid == null ? null : adid.trim();
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng == null ? null : lng.trim();
	}

	public String getDtid() {
		return dtid;
	}

	public void setDtid(String dtid) {
		this.dtid = dtid == null ? null : dtid.trim();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type == null ? null : type.trim();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title == null ? null : title.trim();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content == null ? null : content.trim();
	}

	public String getImgurl() {
		return imgurl;
	}

	public void setImgurl(String imgurl) {
		this.imgurl = imgurl == null ? null : imgurl.trim();
	}

	public String getImgfilename() {
		return imgfilename;
	}

	public void setImgfilename(String imgfilename) {
		this.imgfilename = imgfilename == null ? null : imgfilename.trim();
	}

	public String getImgalt() {
		return imgalt;
	}

	public void setImgalt(String imgalt) {
		this.imgalt = imgalt == null ? null : imgalt.trim();
	}

	public String getClick() {
		return click;
	}

	public void setClick(String click) {
		this.click = click == null ? null : click.trim();
	}

	public Short getIstime() {
		return istime;
	}

	public void setIstime(Short istime) {
		this.istime = istime;
	}

	public Long getStarttime() {
		return starttime;
	}

	public void setStarttime(Long starttime) {
		this.starttime = starttime;
	}

	public Long getEndtime() {
		return endtime;
	}

	public void setEndtime(Long endtime) {
		this.endtime = endtime;
	}

	public Long getAddtime() {
		return addtime;
	}

	public void setAddtime(Long addtime) {
		this.addtime = addtime;
	}

}
