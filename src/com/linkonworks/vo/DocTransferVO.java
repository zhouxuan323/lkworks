package com.linkonworks.vo;

public class DocTransferVO {
	private String id;

    private String lng;

    private String dtid;

    private String title;

    private String subheading;

    private String transferurl;

    private String content;

    private Long sequence;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng == null ? null : lng.trim();
    }

    public String getDtid() {
        return dtid;
    }

    public void setDtid(String dtid) {
        this.dtid = dtid == null ? null : dtid.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getSubheading() {
        return subheading;
    }

    public void setSubheading(String subheading) {
        this.subheading = subheading == null ? null : subheading.trim();
    }

    public String getTransferurl() {
        return transferurl;
    }

    public void setTransferurl(String transferurl) {
        this.transferurl = transferurl == null ? null : transferurl.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Long getSequence() {
        return sequence;
    }

    public void setSequence(Long sequence) {
        this.sequence = sequence;
    }
}