package com.linkonworks.vo;

import java.util.List;

public class TreeVO {
	
	private String id;
	private String text;
	private List<TreeVO> children;
	private String url;
	private  String describe;
	private Short isdel;
	
	 public Short getIsdel() {
		return isdel;
	}
	public void setIsdel(Short isdel) {
		this.isdel = isdel;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	private String params;
	
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	public List<TreeVO> getChildren() {
		return children;
	}
	public void setChildren(List<TreeVO> children) {
		this.children = children;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	
	
	
	
//	  [{
//		    "id":menutree,
//		    "text":"name",
//		    "iconCls":"icon-save",
//		    "children":[{
//				"text":"name",
//				"checked":true,		
//				"children":[{
//					"text":"PhotoShop",
//					"checked":true
//				},{
//					"id": 8,
//					"text":"Sub Bookds",
//					"state":"closed"
//			}]
//		    }]
//	    }];

}
