package com.linkonworks.vo;

import java.io.File;
import java.util.List;

public class FileVO {
	private File fileVO;

	private DocumentVO documentVO;

	public DocumentVO getDocumentVO() {
		return documentVO;
	}

	public void setDocumentVO(DocumentVO documentVO) {
		this.documentVO = documentVO;
	}
	
	public File getFileVO() {
		return fileVO;
	}

	public void setFileVO(File fileVO) {
		this.fileVO = fileVO;
	}

	

}
