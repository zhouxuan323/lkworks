package com.linkonworks.vo;

import java.util.List;

public class MenuVO {
	private String mid;

    private String lng;

    private String pmid;

    private Short isdel;

    private String name;

    private String url;

    private String describe;

    private Short menutype;

    private Short menulevel;
    
    private List<MenuVO> menuList;
    
    private Long sequence;
    
	private List<DocumentVO> documentList;//内容
	private List<AlbumVO> albumList;//图片
    


	public List<MenuVO> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<MenuVO> menuList) {
		this.menuList = menuList;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getPmid() {
		return pmid;
	}

	public void setPmid(String pmid) {
		this.pmid = pmid;
	}

	public Short getIsdel() {
		return isdel;
	}

	public void setIsdel(Short isdel) {
		this.isdel = isdel;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public Short getMenutype() {
		return menutype;
	}

	public void setMenutype(Short menutype) {
		this.menutype = menutype;
	}

	public Short getMenulevel() {
		return menulevel;
	}

	public void setMenulevel(Short menulevel) {
		this.menulevel = menulevel;
	}

	public Long getSequence() {
		return sequence;
	}

	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public List<DocumentVO> getDocumentList() {
		return documentList;
	}

	public void setDocumentList(List<DocumentVO> documentList) {
		this.documentList = documentList;
	}

	public List<AlbumVO> getAlbumList() {
		return albumList;
	}

	public void setAlbumList(List<AlbumVO> albumList) {
		this.albumList = albumList;
	}
    
	
}
