package com.linkonworks.po;

public class MenuPO {
    private String mid;

    private String lng;

    private String pmid;

    private Short isdel;

    private String name;

    private String url;

    private String describe;

    private Short menutype;

    private Short menulevel;

    private Long sequence;

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid == null ? null : mid.trim();
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng == null ? null : lng.trim();
    }

    public String getPmid() {
        return pmid;
    }

    public void setPmid(String pmid) {
        this.pmid = pmid == null ? null : pmid.trim();
    }

    public Short getIsdel() {
        return isdel;
    }

    public void setIsdel(Short isdel) {
        this.isdel = isdel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe == null ? null : describe.trim();
    }

    public Short getMenutype() {
        return menutype;
    }

    public void setMenutype(Short menutype) {
        this.menutype = menutype;
    }

    public Short getMenulevel() {
        return menulevel;
    }

    public void setMenulevel(Short menulevel) {
        this.menulevel = menulevel;
    }

    public Long getSequence() {
        return sequence;
    }

    public void setSequence(Long sequence) {
        this.sequence = sequence;
    }
}