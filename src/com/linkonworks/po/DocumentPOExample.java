package com.linkonworks.po;

import java.util.ArrayList;
import java.util.List;

public class DocumentPOExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DocumentPOExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andDaidIsNull() {
            addCriterion("DAID is null");
            return (Criteria) this;
        }

        public Criteria andDaidIsNotNull() {
            addCriterion("DAID is not null");
            return (Criteria) this;
        }

        public Criteria andDaidEqualTo(String value) {
            addCriterion("DAID =", value, "daid");
            return (Criteria) this;
        }

        public Criteria andDaidNotEqualTo(String value) {
            addCriterion("DAID <>", value, "daid");
            return (Criteria) this;
        }

        public Criteria andDaidGreaterThan(String value) {
            addCriterion("DAID >", value, "daid");
            return (Criteria) this;
        }

        public Criteria andDaidGreaterThanOrEqualTo(String value) {
            addCriterion("DAID >=", value, "daid");
            return (Criteria) this;
        }

        public Criteria andDaidLessThan(String value) {
            addCriterion("DAID <", value, "daid");
            return (Criteria) this;
        }

        public Criteria andDaidLessThanOrEqualTo(String value) {
            addCriterion("DAID <=", value, "daid");
            return (Criteria) this;
        }

        public Criteria andDaidLike(String value) {
            addCriterion("DAID like", value, "daid");
            return (Criteria) this;
        }

        public Criteria andDaidNotLike(String value) {
            addCriterion("DAID not like", value, "daid");
            return (Criteria) this;
        }

        public Criteria andDaidIn(List<String> values) {
            addCriterion("DAID in", values, "daid");
            return (Criteria) this;
        }

        public Criteria andDaidNotIn(List<String> values) {
            addCriterion("DAID not in", values, "daid");
            return (Criteria) this;
        }

        public Criteria andDaidBetween(String value1, String value2) {
            addCriterion("DAID between", value1, value2, "daid");
            return (Criteria) this;
        }

        public Criteria andDaidNotBetween(String value1, String value2) {
            addCriterion("DAID not between", value1, value2, "daid");
            return (Criteria) this;
        }

        public Criteria andLngIsNull() {
            addCriterion("LNG is null");
            return (Criteria) this;
        }

        public Criteria andLngIsNotNull() {
            addCriterion("LNG is not null");
            return (Criteria) this;
        }

        public Criteria andLngEqualTo(String value) {
            addCriterion("LNG =", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotEqualTo(String value) {
            addCriterion("LNG <>", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngGreaterThan(String value) {
            addCriterion("LNG >", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngGreaterThanOrEqualTo(String value) {
            addCriterion("LNG >=", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngLessThan(String value) {
            addCriterion("LNG <", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngLessThanOrEqualTo(String value) {
            addCriterion("LNG <=", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngLike(String value) {
            addCriterion("LNG like", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotLike(String value) {
            addCriterion("LNG not like", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngIn(List<String> values) {
            addCriterion("LNG in", values, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotIn(List<String> values) {
            addCriterion("LNG not in", values, "lng");
            return (Criteria) this;
        }

        public Criteria andLngBetween(String value1, String value2) {
            addCriterion("LNG between", value1, value2, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotBetween(String value1, String value2) {
            addCriterion("LNG not between", value1, value2, "lng");
            return (Criteria) this;
        }

        public Criteria andDtidIsNull() {
            addCriterion("DTID is null");
            return (Criteria) this;
        }

        public Criteria andDtidIsNotNull() {
            addCriterion("DTID is not null");
            return (Criteria) this;
        }

        public Criteria andDtidEqualTo(String value) {
            addCriterion("DTID =", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidNotEqualTo(String value) {
            addCriterion("DTID <>", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidGreaterThan(String value) {
            addCriterion("DTID >", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidGreaterThanOrEqualTo(String value) {
            addCriterion("DTID >=", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidLessThan(String value) {
            addCriterion("DTID <", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidLessThanOrEqualTo(String value) {
            addCriterion("DTID <=", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidLike(String value) {
            addCriterion("DTID like", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidNotLike(String value) {
            addCriterion("DTID not like", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidIn(List<String> values) {
            addCriterion("DTID in", values, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidNotIn(List<String> values) {
            addCriterion("DTID not in", values, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidBetween(String value1, String value2) {
            addCriterion("DTID between", value1, value2, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidNotBetween(String value1, String value2) {
            addCriterion("DTID not between", value1, value2, "dtid");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("TITLE is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("TITLE is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(String value) {
            addCriterion("TITLE =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(String value) {
            addCriterion("TITLE <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(String value) {
            addCriterion("TITLE >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(String value) {
            addCriterion("TITLE >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(String value) {
            addCriterion("TITLE <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(String value) {
            addCriterion("TITLE <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLike(String value) {
            addCriterion("TITLE like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotLike(String value) {
            addCriterion("TITLE not like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<String> values) {
            addCriterion("TITLE in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<String> values) {
            addCriterion("TITLE not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(String value1, String value2) {
            addCriterion("TITLE between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(String value1, String value2) {
            addCriterion("TITLE not between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleurlIsNull() {
            addCriterion("TITLEURL is null");
            return (Criteria) this;
        }

        public Criteria andTitleurlIsNotNull() {
            addCriterion("TITLEURL is not null");
            return (Criteria) this;
        }

        public Criteria andTitleurlEqualTo(String value) {
            addCriterion("TITLEURL =", value, "titleurl");
            return (Criteria) this;
        }

        public Criteria andTitleurlNotEqualTo(String value) {
            addCriterion("TITLEURL <>", value, "titleurl");
            return (Criteria) this;
        }

        public Criteria andTitleurlGreaterThan(String value) {
            addCriterion("TITLEURL >", value, "titleurl");
            return (Criteria) this;
        }

        public Criteria andTitleurlGreaterThanOrEqualTo(String value) {
            addCriterion("TITLEURL >=", value, "titleurl");
            return (Criteria) this;
        }

        public Criteria andTitleurlLessThan(String value) {
            addCriterion("TITLEURL <", value, "titleurl");
            return (Criteria) this;
        }

        public Criteria andTitleurlLessThanOrEqualTo(String value) {
            addCriterion("TITLEURL <=", value, "titleurl");
            return (Criteria) this;
        }

        public Criteria andTitleurlLike(String value) {
            addCriterion("TITLEURL like", value, "titleurl");
            return (Criteria) this;
        }

        public Criteria andTitleurlNotLike(String value) {
            addCriterion("TITLEURL not like", value, "titleurl");
            return (Criteria) this;
        }

        public Criteria andTitleurlIn(List<String> values) {
            addCriterion("TITLEURL in", values, "titleurl");
            return (Criteria) this;
        }

        public Criteria andTitleurlNotIn(List<String> values) {
            addCriterion("TITLEURL not in", values, "titleurl");
            return (Criteria) this;
        }

        public Criteria andTitleurlBetween(String value1, String value2) {
            addCriterion("TITLEURL between", value1, value2, "titleurl");
            return (Criteria) this;
        }

        public Criteria andTitleurlNotBetween(String value1, String value2) {
            addCriterion("TITLEURL not between", value1, value2, "titleurl");
            return (Criteria) this;
        }

        public Criteria andTitleclassIsNull() {
            addCriterion("TITLECLASS is null");
            return (Criteria) this;
        }

        public Criteria andTitleclassIsNotNull() {
            addCriterion("TITLECLASS is not null");
            return (Criteria) this;
        }

        public Criteria andTitleclassEqualTo(String value) {
            addCriterion("TITLECLASS =", value, "titleclass");
            return (Criteria) this;
        }

        public Criteria andTitleclassNotEqualTo(String value) {
            addCriterion("TITLECLASS <>", value, "titleclass");
            return (Criteria) this;
        }

        public Criteria andTitleclassGreaterThan(String value) {
            addCriterion("TITLECLASS >", value, "titleclass");
            return (Criteria) this;
        }

        public Criteria andTitleclassGreaterThanOrEqualTo(String value) {
            addCriterion("TITLECLASS >=", value, "titleclass");
            return (Criteria) this;
        }

        public Criteria andTitleclassLessThan(String value) {
            addCriterion("TITLECLASS <", value, "titleclass");
            return (Criteria) this;
        }

        public Criteria andTitleclassLessThanOrEqualTo(String value) {
            addCriterion("TITLECLASS <=", value, "titleclass");
            return (Criteria) this;
        }

        public Criteria andTitleclassLike(String value) {
            addCriterion("TITLECLASS like", value, "titleclass");
            return (Criteria) this;
        }

        public Criteria andTitleclassNotLike(String value) {
            addCriterion("TITLECLASS not like", value, "titleclass");
            return (Criteria) this;
        }

        public Criteria andTitleclassIn(List<String> values) {
            addCriterion("TITLECLASS in", values, "titleclass");
            return (Criteria) this;
        }

        public Criteria andTitleclassNotIn(List<String> values) {
            addCriterion("TITLECLASS not in", values, "titleclass");
            return (Criteria) this;
        }

        public Criteria andTitleclassBetween(String value1, String value2) {
            addCriterion("TITLECLASS between", value1, value2, "titleclass");
            return (Criteria) this;
        }

        public Criteria andTitleclassNotBetween(String value1, String value2) {
            addCriterion("TITLECLASS not between", value1, value2, "titleclass");
            return (Criteria) this;
        }

        public Criteria andSubheadingIsNull() {
            addCriterion("SUBHEADING is null");
            return (Criteria) this;
        }

        public Criteria andSubheadingIsNotNull() {
            addCriterion("SUBHEADING is not null");
            return (Criteria) this;
        }

        public Criteria andSubheadingEqualTo(String value) {
            addCriterion("SUBHEADING =", value, "subheading");
            return (Criteria) this;
        }

        public Criteria andSubheadingNotEqualTo(String value) {
            addCriterion("SUBHEADING <>", value, "subheading");
            return (Criteria) this;
        }

        public Criteria andSubheadingGreaterThan(String value) {
            addCriterion("SUBHEADING >", value, "subheading");
            return (Criteria) this;
        }

        public Criteria andSubheadingGreaterThanOrEqualTo(String value) {
            addCriterion("SUBHEADING >=", value, "subheading");
            return (Criteria) this;
        }

        public Criteria andSubheadingLessThan(String value) {
            addCriterion("SUBHEADING <", value, "subheading");
            return (Criteria) this;
        }

        public Criteria andSubheadingLessThanOrEqualTo(String value) {
            addCriterion("SUBHEADING <=", value, "subheading");
            return (Criteria) this;
        }

        public Criteria andSubheadingLike(String value) {
            addCriterion("SUBHEADING like", value, "subheading");
            return (Criteria) this;
        }

        public Criteria andSubheadingNotLike(String value) {
            addCriterion("SUBHEADING not like", value, "subheading");
            return (Criteria) this;
        }

        public Criteria andSubheadingIn(List<String> values) {
            addCriterion("SUBHEADING in", values, "subheading");
            return (Criteria) this;
        }

        public Criteria andSubheadingNotIn(List<String> values) {
            addCriterion("SUBHEADING not in", values, "subheading");
            return (Criteria) this;
        }

        public Criteria andSubheadingBetween(String value1, String value2) {
            addCriterion("SUBHEADING between", value1, value2, "subheading");
            return (Criteria) this;
        }

        public Criteria andSubheadingNotBetween(String value1, String value2) {
            addCriterion("SUBHEADING not between", value1, value2, "subheading");
            return (Criteria) this;
        }

        public Criteria andSubheadingclassIsNull() {
            addCriterion("SUBHEADINGCLASS is null");
            return (Criteria) this;
        }

        public Criteria andSubheadingclassIsNotNull() {
            addCriterion("SUBHEADINGCLASS is not null");
            return (Criteria) this;
        }

        public Criteria andSubheadingclassEqualTo(String value) {
            addCriterion("SUBHEADINGCLASS =", value, "subheadingclass");
            return (Criteria) this;
        }

        public Criteria andSubheadingclassNotEqualTo(String value) {
            addCriterion("SUBHEADINGCLASS <>", value, "subheadingclass");
            return (Criteria) this;
        }

        public Criteria andSubheadingclassGreaterThan(String value) {
            addCriterion("SUBHEADINGCLASS >", value, "subheadingclass");
            return (Criteria) this;
        }

        public Criteria andSubheadingclassGreaterThanOrEqualTo(String value) {
            addCriterion("SUBHEADINGCLASS >=", value, "subheadingclass");
            return (Criteria) this;
        }

        public Criteria andSubheadingclassLessThan(String value) {
            addCriterion("SUBHEADINGCLASS <", value, "subheadingclass");
            return (Criteria) this;
        }

        public Criteria andSubheadingclassLessThanOrEqualTo(String value) {
            addCriterion("SUBHEADINGCLASS <=", value, "subheadingclass");
            return (Criteria) this;
        }

        public Criteria andSubheadingclassLike(String value) {
            addCriterion("SUBHEADINGCLASS like", value, "subheadingclass");
            return (Criteria) this;
        }

        public Criteria andSubheadingclassNotLike(String value) {
            addCriterion("SUBHEADINGCLASS not like", value, "subheadingclass");
            return (Criteria) this;
        }

        public Criteria andSubheadingclassIn(List<String> values) {
            addCriterion("SUBHEADINGCLASS in", values, "subheadingclass");
            return (Criteria) this;
        }

        public Criteria andSubheadingclassNotIn(List<String> values) {
            addCriterion("SUBHEADINGCLASS not in", values, "subheadingclass");
            return (Criteria) this;
        }

        public Criteria andSubheadingclassBetween(String value1, String value2) {
            addCriterion("SUBHEADINGCLASS between", value1, value2, "subheadingclass");
            return (Criteria) this;
        }

        public Criteria andSubheadingclassNotBetween(String value1, String value2) {
            addCriterion("SUBHEADINGCLASS not between", value1, value2, "subheadingclass");
            return (Criteria) this;
        }

        public Criteria andSubheadingurlIsNull() {
            addCriterion("SUBHEADINGURL is null");
            return (Criteria) this;
        }

        public Criteria andSubheadingurlIsNotNull() {
            addCriterion("SUBHEADINGURL is not null");
            return (Criteria) this;
        }

        public Criteria andSubheadingurlEqualTo(String value) {
            addCriterion("SUBHEADINGURL =", value, "subheadingurl");
            return (Criteria) this;
        }

        public Criteria andSubheadingurlNotEqualTo(String value) {
            addCriterion("SUBHEADINGURL <>", value, "subheadingurl");
            return (Criteria) this;
        }

        public Criteria andSubheadingurlGreaterThan(String value) {
            addCriterion("SUBHEADINGURL >", value, "subheadingurl");
            return (Criteria) this;
        }

        public Criteria andSubheadingurlGreaterThanOrEqualTo(String value) {
            addCriterion("SUBHEADINGURL >=", value, "subheadingurl");
            return (Criteria) this;
        }

        public Criteria andSubheadingurlLessThan(String value) {
            addCriterion("SUBHEADINGURL <", value, "subheadingurl");
            return (Criteria) this;
        }

        public Criteria andSubheadingurlLessThanOrEqualTo(String value) {
            addCriterion("SUBHEADINGURL <=", value, "subheadingurl");
            return (Criteria) this;
        }

        public Criteria andSubheadingurlLike(String value) {
            addCriterion("SUBHEADINGURL like", value, "subheadingurl");
            return (Criteria) this;
        }

        public Criteria andSubheadingurlNotLike(String value) {
            addCriterion("SUBHEADINGURL not like", value, "subheadingurl");
            return (Criteria) this;
        }

        public Criteria andSubheadingurlIn(List<String> values) {
            addCriterion("SUBHEADINGURL in", values, "subheadingurl");
            return (Criteria) this;
        }

        public Criteria andSubheadingurlNotIn(List<String> values) {
            addCriterion("SUBHEADINGURL not in", values, "subheadingurl");
            return (Criteria) this;
        }

        public Criteria andSubheadingurlBetween(String value1, String value2) {
            addCriterion("SUBHEADINGURL between", value1, value2, "subheadingurl");
            return (Criteria) this;
        }

        public Criteria andSubheadingurlNotBetween(String value1, String value2) {
            addCriterion("SUBHEADINGURL not between", value1, value2, "subheadingurl");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("CONTENT is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("CONTENT is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("CONTENT =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("CONTENT <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("CONTENT >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("CONTENT >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("CONTENT <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("CONTENT <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("CONTENT like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("CONTENT not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("CONTENT in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("CONTENT not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("CONTENT between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("CONTENT not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentclassIsNull() {
            addCriterion("CONTENTCLASS is null");
            return (Criteria) this;
        }

        public Criteria andContentclassIsNotNull() {
            addCriterion("CONTENTCLASS is not null");
            return (Criteria) this;
        }

        public Criteria andContentclassEqualTo(String value) {
            addCriterion("CONTENTCLASS =", value, "contentclass");
            return (Criteria) this;
        }

        public Criteria andContentclassNotEqualTo(String value) {
            addCriterion("CONTENTCLASS <>", value, "contentclass");
            return (Criteria) this;
        }

        public Criteria andContentclassGreaterThan(String value) {
            addCriterion("CONTENTCLASS >", value, "contentclass");
            return (Criteria) this;
        }

        public Criteria andContentclassGreaterThanOrEqualTo(String value) {
            addCriterion("CONTENTCLASS >=", value, "contentclass");
            return (Criteria) this;
        }

        public Criteria andContentclassLessThan(String value) {
            addCriterion("CONTENTCLASS <", value, "contentclass");
            return (Criteria) this;
        }

        public Criteria andContentclassLessThanOrEqualTo(String value) {
            addCriterion("CONTENTCLASS <=", value, "contentclass");
            return (Criteria) this;
        }

        public Criteria andContentclassLike(String value) {
            addCriterion("CONTENTCLASS like", value, "contentclass");
            return (Criteria) this;
        }

        public Criteria andContentclassNotLike(String value) {
            addCriterion("CONTENTCLASS not like", value, "contentclass");
            return (Criteria) this;
        }

        public Criteria andContentclassIn(List<String> values) {
            addCriterion("CONTENTCLASS in", values, "contentclass");
            return (Criteria) this;
        }

        public Criteria andContentclassNotIn(List<String> values) {
            addCriterion("CONTENTCLASS not in", values, "contentclass");
            return (Criteria) this;
        }

        public Criteria andContentclassBetween(String value1, String value2) {
            addCriterion("CONTENTCLASS between", value1, value2, "contentclass");
            return (Criteria) this;
        }

        public Criteria andContentclassNotBetween(String value1, String value2) {
            addCriterion("CONTENTCLASS not between", value1, value2, "contentclass");
            return (Criteria) this;
        }

        public Criteria andContenturlIsNull() {
            addCriterion("CONTENTURL is null");
            return (Criteria) this;
        }

        public Criteria andContenturlIsNotNull() {
            addCriterion("CONTENTURL is not null");
            return (Criteria) this;
        }

        public Criteria andContenturlEqualTo(String value) {
            addCriterion("CONTENTURL =", value, "contenturl");
            return (Criteria) this;
        }

        public Criteria andContenturlNotEqualTo(String value) {
            addCriterion("CONTENTURL <>", value, "contenturl");
            return (Criteria) this;
        }

        public Criteria andContenturlGreaterThan(String value) {
            addCriterion("CONTENTURL >", value, "contenturl");
            return (Criteria) this;
        }

        public Criteria andContenturlGreaterThanOrEqualTo(String value) {
            addCriterion("CONTENTURL >=", value, "contenturl");
            return (Criteria) this;
        }

        public Criteria andContenturlLessThan(String value) {
            addCriterion("CONTENTURL <", value, "contenturl");
            return (Criteria) this;
        }

        public Criteria andContenturlLessThanOrEqualTo(String value) {
            addCriterion("CONTENTURL <=", value, "contenturl");
            return (Criteria) this;
        }

        public Criteria andContenturlLike(String value) {
            addCriterion("CONTENTURL like", value, "contenturl");
            return (Criteria) this;
        }

        public Criteria andContenturlNotLike(String value) {
            addCriterion("CONTENTURL not like", value, "contenturl");
            return (Criteria) this;
        }

        public Criteria andContenturlIn(List<String> values) {
            addCriterion("CONTENTURL in", values, "contenturl");
            return (Criteria) this;
        }

        public Criteria andContenturlNotIn(List<String> values) {
            addCriterion("CONTENTURL not in", values, "contenturl");
            return (Criteria) this;
        }

        public Criteria andContenturlBetween(String value1, String value2) {
            addCriterion("CONTENTURL between", value1, value2, "contenturl");
            return (Criteria) this;
        }

        public Criteria andContenturlNotBetween(String value1, String value2) {
            addCriterion("CONTENTURL not between", value1, value2, "contenturl");
            return (Criteria) this;
        }

        public Criteria andTransferurlIsNull() {
            addCriterion("TRANSFERURL is null");
            return (Criteria) this;
        }

        public Criteria andTransferurlIsNotNull() {
            addCriterion("TRANSFERURL is not null");
            return (Criteria) this;
        }

        public Criteria andTransferurlEqualTo(String value) {
            addCriterion("TRANSFERURL =", value, "transferurl");
            return (Criteria) this;
        }

        public Criteria andTransferurlNotEqualTo(String value) {
            addCriterion("TRANSFERURL <>", value, "transferurl");
            return (Criteria) this;
        }

        public Criteria andTransferurlGreaterThan(String value) {
            addCriterion("TRANSFERURL >", value, "transferurl");
            return (Criteria) this;
        }

        public Criteria andTransferurlGreaterThanOrEqualTo(String value) {
            addCriterion("TRANSFERURL >=", value, "transferurl");
            return (Criteria) this;
        }

        public Criteria andTransferurlLessThan(String value) {
            addCriterion("TRANSFERURL <", value, "transferurl");
            return (Criteria) this;
        }

        public Criteria andTransferurlLessThanOrEqualTo(String value) {
            addCriterion("TRANSFERURL <=", value, "transferurl");
            return (Criteria) this;
        }

        public Criteria andTransferurlLike(String value) {
            addCriterion("TRANSFERURL like", value, "transferurl");
            return (Criteria) this;
        }

        public Criteria andTransferurlNotLike(String value) {
            addCriterion("TRANSFERURL not like", value, "transferurl");
            return (Criteria) this;
        }

        public Criteria andTransferurlIn(List<String> values) {
            addCriterion("TRANSFERURL in", values, "transferurl");
            return (Criteria) this;
        }

        public Criteria andTransferurlNotIn(List<String> values) {
            addCriterion("TRANSFERURL not in", values, "transferurl");
            return (Criteria) this;
        }

        public Criteria andTransferurlBetween(String value1, String value2) {
            addCriterion("TRANSFERURL between", value1, value2, "transferurl");
            return (Criteria) this;
        }

        public Criteria andTransferurlNotBetween(String value1, String value2) {
            addCriterion("TRANSFERURL not between", value1, value2, "transferurl");
            return (Criteria) this;
        }

        public Criteria andImgurlIsNull() {
            addCriterion("IMGURL is null");
            return (Criteria) this;
        }

        public Criteria andImgurlIsNotNull() {
            addCriterion("IMGURL is not null");
            return (Criteria) this;
        }

        public Criteria andImgurlEqualTo(String value) {
            addCriterion("IMGURL =", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlNotEqualTo(String value) {
            addCriterion("IMGURL <>", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlGreaterThan(String value) {
            addCriterion("IMGURL >", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlGreaterThanOrEqualTo(String value) {
            addCriterion("IMGURL >=", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlLessThan(String value) {
            addCriterion("IMGURL <", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlLessThanOrEqualTo(String value) {
            addCriterion("IMGURL <=", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlLike(String value) {
            addCriterion("IMGURL like", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlNotLike(String value) {
            addCriterion("IMGURL not like", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlIn(List<String> values) {
            addCriterion("IMGURL in", values, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlNotIn(List<String> values) {
            addCriterion("IMGURL not in", values, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlBetween(String value1, String value2) {
            addCriterion("IMGURL between", value1, value2, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlNotBetween(String value1, String value2) {
            addCriterion("IMGURL not between", value1, value2, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgfilenameIsNull() {
            addCriterion("IMGFILENAME is null");
            return (Criteria) this;
        }

        public Criteria andImgfilenameIsNotNull() {
            addCriterion("IMGFILENAME is not null");
            return (Criteria) this;
        }

        public Criteria andImgfilenameEqualTo(String value) {
            addCriterion("IMGFILENAME =", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameNotEqualTo(String value) {
            addCriterion("IMGFILENAME <>", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameGreaterThan(String value) {
            addCriterion("IMGFILENAME >", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameGreaterThanOrEqualTo(String value) {
            addCriterion("IMGFILENAME >=", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameLessThan(String value) {
            addCriterion("IMGFILENAME <", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameLessThanOrEqualTo(String value) {
            addCriterion("IMGFILENAME <=", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameLike(String value) {
            addCriterion("IMGFILENAME like", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameNotLike(String value) {
            addCriterion("IMGFILENAME not like", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameIn(List<String> values) {
            addCriterion("IMGFILENAME in", values, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameNotIn(List<String> values) {
            addCriterion("IMGFILENAME not in", values, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameBetween(String value1, String value2) {
            addCriterion("IMGFILENAME between", value1, value2, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameNotBetween(String value1, String value2) {
            addCriterion("IMGFILENAME not between", value1, value2, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImglinkIsNull() {
            addCriterion("IMGLINK is null");
            return (Criteria) this;
        }

        public Criteria andImglinkIsNotNull() {
            addCriterion("IMGLINK is not null");
            return (Criteria) this;
        }

        public Criteria andImglinkEqualTo(String value) {
            addCriterion("IMGLINK =", value, "imglink");
            return (Criteria) this;
        }

        public Criteria andImglinkNotEqualTo(String value) {
            addCriterion("IMGLINK <>", value, "imglink");
            return (Criteria) this;
        }

        public Criteria andImglinkGreaterThan(String value) {
            addCriterion("IMGLINK >", value, "imglink");
            return (Criteria) this;
        }

        public Criteria andImglinkGreaterThanOrEqualTo(String value) {
            addCriterion("IMGLINK >=", value, "imglink");
            return (Criteria) this;
        }

        public Criteria andImglinkLessThan(String value) {
            addCriterion("IMGLINK <", value, "imglink");
            return (Criteria) this;
        }

        public Criteria andImglinkLessThanOrEqualTo(String value) {
            addCriterion("IMGLINK <=", value, "imglink");
            return (Criteria) this;
        }

        public Criteria andImglinkLike(String value) {
            addCriterion("IMGLINK like", value, "imglink");
            return (Criteria) this;
        }

        public Criteria andImglinkNotLike(String value) {
            addCriterion("IMGLINK not like", value, "imglink");
            return (Criteria) this;
        }

        public Criteria andImglinkIn(List<String> values) {
            addCriterion("IMGLINK in", values, "imglink");
            return (Criteria) this;
        }

        public Criteria andImglinkNotIn(List<String> values) {
            addCriterion("IMGLINK not in", values, "imglink");
            return (Criteria) this;
        }

        public Criteria andImglinkBetween(String value1, String value2) {
            addCriterion("IMGLINK between", value1, value2, "imglink");
            return (Criteria) this;
        }

        public Criteria andImglinkNotBetween(String value1, String value2) {
            addCriterion("IMGLINK not between", value1, value2, "imglink");
            return (Criteria) this;
        }

        public Criteria andIstimeIsNull() {
            addCriterion("ISTIME is null");
            return (Criteria) this;
        }

        public Criteria andIstimeIsNotNull() {
            addCriterion("ISTIME is not null");
            return (Criteria) this;
        }

        public Criteria andIstimeEqualTo(Short value) {
            addCriterion("ISTIME =", value, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeNotEqualTo(Short value) {
            addCriterion("ISTIME <>", value, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeGreaterThan(Short value) {
            addCriterion("ISTIME >", value, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeGreaterThanOrEqualTo(Short value) {
            addCriterion("ISTIME >=", value, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeLessThan(Short value) {
            addCriterion("ISTIME <", value, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeLessThanOrEqualTo(Short value) {
            addCriterion("ISTIME <=", value, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeIn(List<Short> values) {
            addCriterion("ISTIME in", values, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeNotIn(List<Short> values) {
            addCriterion("ISTIME not in", values, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeBetween(Short value1, Short value2) {
            addCriterion("ISTIME between", value1, value2, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeNotBetween(Short value1, Short value2) {
            addCriterion("ISTIME not between", value1, value2, "istime");
            return (Criteria) this;
        }

        public Criteria andStarttimeIsNull() {
            addCriterion("STARTTIME is null");
            return (Criteria) this;
        }

        public Criteria andStarttimeIsNotNull() {
            addCriterion("STARTTIME is not null");
            return (Criteria) this;
        }

        public Criteria andStarttimeEqualTo(Long value) {
            addCriterion("STARTTIME =", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotEqualTo(Long value) {
            addCriterion("STARTTIME <>", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeGreaterThan(Long value) {
            addCriterion("STARTTIME >", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeGreaterThanOrEqualTo(Long value) {
            addCriterion("STARTTIME >=", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeLessThan(Long value) {
            addCriterion("STARTTIME <", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeLessThanOrEqualTo(Long value) {
            addCriterion("STARTTIME <=", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeIn(List<Long> values) {
            addCriterion("STARTTIME in", values, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotIn(List<Long> values) {
            addCriterion("STARTTIME not in", values, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeBetween(Long value1, Long value2) {
            addCriterion("STARTTIME between", value1, value2, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotBetween(Long value1, Long value2) {
            addCriterion("STARTTIME not between", value1, value2, "starttime");
            return (Criteria) this;
        }

        public Criteria andEndtimeIsNull() {
            addCriterion("ENDTIME is null");
            return (Criteria) this;
        }

        public Criteria andEndtimeIsNotNull() {
            addCriterion("ENDTIME is not null");
            return (Criteria) this;
        }

        public Criteria andEndtimeEqualTo(Long value) {
            addCriterion("ENDTIME =", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotEqualTo(Long value) {
            addCriterion("ENDTIME <>", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeGreaterThan(Long value) {
            addCriterion("ENDTIME >", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeGreaterThanOrEqualTo(Long value) {
            addCriterion("ENDTIME >=", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeLessThan(Long value) {
            addCriterion("ENDTIME <", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeLessThanOrEqualTo(Long value) {
            addCriterion("ENDTIME <=", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeIn(List<Long> values) {
            addCriterion("ENDTIME in", values, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotIn(List<Long> values) {
            addCriterion("ENDTIME not in", values, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeBetween(Long value1, Long value2) {
            addCriterion("ENDTIME between", value1, value2, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotBetween(Long value1, Long value2) {
            addCriterion("ENDTIME not between", value1, value2, "endtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeIsNull() {
            addCriterion("ADDTIME is null");
            return (Criteria) this;
        }

        public Criteria andAddtimeIsNotNull() {
            addCriterion("ADDTIME is not null");
            return (Criteria) this;
        }

        public Criteria andAddtimeEqualTo(Long value) {
            addCriterion("ADDTIME =", value, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeNotEqualTo(Long value) {
            addCriterion("ADDTIME <>", value, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeGreaterThan(Long value) {
            addCriterion("ADDTIME >", value, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeGreaterThanOrEqualTo(Long value) {
            addCriterion("ADDTIME >=", value, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeLessThan(Long value) {
            addCriterion("ADDTIME <", value, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeLessThanOrEqualTo(Long value) {
            addCriterion("ADDTIME <=", value, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeIn(List<Long> values) {
            addCriterion("ADDTIME in", values, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeNotIn(List<Long> values) {
            addCriterion("ADDTIME not in", values, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeBetween(Long value1, Long value2) {
            addCriterion("ADDTIME between", value1, value2, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeNotBetween(Long value1, Long value2) {
            addCriterion("ADDTIME not between", value1, value2, "addtime");
            return (Criteria) this;
        }

        public Criteria andSequenceIsNull() {
            addCriterion("SEQUENCE is null");
            return (Criteria) this;
        }

        public Criteria andSequenceIsNotNull() {
            addCriterion("SEQUENCE is not null");
            return (Criteria) this;
        }

        public Criteria andSequenceEqualTo(Long value) {
            addCriterion("SEQUENCE =", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceNotEqualTo(Long value) {
            addCriterion("SEQUENCE <>", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceGreaterThan(Long value) {
            addCriterion("SEQUENCE >", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceGreaterThanOrEqualTo(Long value) {
            addCriterion("SEQUENCE >=", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceLessThan(Long value) {
            addCriterion("SEQUENCE <", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceLessThanOrEqualTo(Long value) {
            addCriterion("SEQUENCE <=", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceIn(List<Long> values) {
            addCriterion("SEQUENCE in", values, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceNotIn(List<Long> values) {
            addCriterion("SEQUENCE not in", values, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceBetween(Long value1, Long value2) {
            addCriterion("SEQUENCE between", value1, value2, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceNotBetween(Long value1, Long value2) {
            addCriterion("SEQUENCE not between", value1, value2, "sequence");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}