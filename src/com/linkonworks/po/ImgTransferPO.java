package com.linkonworks.po;

public class ImgTransferPO {
    private String itid;

    private String lng;

    private String dtid;

    private String url;

    private String filename;

    private String transferurl;

    private Long time;

    private String alt;

    private Long sequence;

    public String getItid() {
        return itid;
    }

    public void setItid(String itid) {
        this.itid = itid == null ? null : itid.trim();
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng == null ? null : lng.trim();
    }

    public String getDtid() {
        return dtid;
    }

    public void setDtid(String dtid) {
        this.dtid = dtid == null ? null : dtid.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename == null ? null : filename.trim();
    }

    public String getTransferurl() {
        return transferurl;
    }

    public void setTransferurl(String transferurl) {
        this.transferurl = transferurl == null ? null : transferurl.trim();
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt == null ? null : alt.trim();
    }

    public Long getSequence() {
        return sequence;
    }

    public void setSequence(Long sequence) {
        this.sequence = sequence;
    }
}