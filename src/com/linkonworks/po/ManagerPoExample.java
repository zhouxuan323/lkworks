package com.linkonworks.po;

import java.util.ArrayList;
import java.util.List;

public class ManagerPoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ManagerPoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andManidIsNull() {
            addCriterion("MANID is null");
            return (Criteria) this;
        }

        public Criteria andManidIsNotNull() {
            addCriterion("MANID is not null");
            return (Criteria) this;
        }

        public Criteria andManidEqualTo(String value) {
            addCriterion("MANID =", value, "manid");
            return (Criteria) this;
        }

        public Criteria andManidNotEqualTo(String value) {
            addCriterion("MANID <>", value, "manid");
            return (Criteria) this;
        }

        public Criteria andManidGreaterThan(String value) {
            addCriterion("MANID >", value, "manid");
            return (Criteria) this;
        }

        public Criteria andManidGreaterThanOrEqualTo(String value) {
            addCriterion("MANID >=", value, "manid");
            return (Criteria) this;
        }

        public Criteria andManidLessThan(String value) {
            addCriterion("MANID <", value, "manid");
            return (Criteria) this;
        }

        public Criteria andManidLessThanOrEqualTo(String value) {
            addCriterion("MANID <=", value, "manid");
            return (Criteria) this;
        }

        public Criteria andManidLike(String value) {
            addCriterion("MANID like", value, "manid");
            return (Criteria) this;
        }

        public Criteria andManidNotLike(String value) {
            addCriterion("MANID not like", value, "manid");
            return (Criteria) this;
        }

        public Criteria andManidIn(List<String> values) {
            addCriterion("MANID in", values, "manid");
            return (Criteria) this;
        }

        public Criteria andManidNotIn(List<String> values) {
            addCriterion("MANID not in", values, "manid");
            return (Criteria) this;
        }

        public Criteria andManidBetween(String value1, String value2) {
            addCriterion("MANID between", value1, value2, "manid");
            return (Criteria) this;
        }

        public Criteria andManidNotBetween(String value1, String value2) {
            addCriterion("MANID not between", value1, value2, "manid");
            return (Criteria) this;
        }

        public Criteria andMannameIsNull() {
            addCriterion("MANNAME is null");
            return (Criteria) this;
        }

        public Criteria andMannameIsNotNull() {
            addCriterion("MANNAME is not null");
            return (Criteria) this;
        }

        public Criteria andMannameEqualTo(String value) {
            addCriterion("MANNAME =", value, "manname");
            return (Criteria) this;
        }

        public Criteria andMannameNotEqualTo(String value) {
            addCriterion("MANNAME <>", value, "manname");
            return (Criteria) this;
        }

        public Criteria andMannameGreaterThan(String value) {
            addCriterion("MANNAME >", value, "manname");
            return (Criteria) this;
        }

        public Criteria andMannameGreaterThanOrEqualTo(String value) {
            addCriterion("MANNAME >=", value, "manname");
            return (Criteria) this;
        }

        public Criteria andMannameLessThan(String value) {
            addCriterion("MANNAME <", value, "manname");
            return (Criteria) this;
        }

        public Criteria andMannameLessThanOrEqualTo(String value) {
            addCriterion("MANNAME <=", value, "manname");
            return (Criteria) this;
        }

        public Criteria andMannameLike(String value) {
            addCriterion("MANNAME like", value, "manname");
            return (Criteria) this;
        }

        public Criteria andMannameNotLike(String value) {
            addCriterion("MANNAME not like", value, "manname");
            return (Criteria) this;
        }

        public Criteria andMannameIn(List<String> values) {
            addCriterion("MANNAME in", values, "manname");
            return (Criteria) this;
        }

        public Criteria andMannameNotIn(List<String> values) {
            addCriterion("MANNAME not in", values, "manname");
            return (Criteria) this;
        }

        public Criteria andMannameBetween(String value1, String value2) {
            addCriterion("MANNAME between", value1, value2, "manname");
            return (Criteria) this;
        }

        public Criteria andMannameNotBetween(String value1, String value2) {
            addCriterion("MANNAME not between", value1, value2, "manname");
            return (Criteria) this;
        }

        public Criteria andManpasswordIsNull() {
            addCriterion("MANPASSWORD is null");
            return (Criteria) this;
        }

        public Criteria andManpasswordIsNotNull() {
            addCriterion("MANPASSWORD is not null");
            return (Criteria) this;
        }

        public Criteria andManpasswordEqualTo(String value) {
            addCriterion("MANPASSWORD =", value, "manpassword");
            return (Criteria) this;
        }

        public Criteria andManpasswordNotEqualTo(String value) {
            addCriterion("MANPASSWORD <>", value, "manpassword");
            return (Criteria) this;
        }

        public Criteria andManpasswordGreaterThan(String value) {
            addCriterion("MANPASSWORD >", value, "manpassword");
            return (Criteria) this;
        }

        public Criteria andManpasswordGreaterThanOrEqualTo(String value) {
            addCriterion("MANPASSWORD >=", value, "manpassword");
            return (Criteria) this;
        }

        public Criteria andManpasswordLessThan(String value) {
            addCriterion("MANPASSWORD <", value, "manpassword");
            return (Criteria) this;
        }

        public Criteria andManpasswordLessThanOrEqualTo(String value) {
            addCriterion("MANPASSWORD <=", value, "manpassword");
            return (Criteria) this;
        }

        public Criteria andManpasswordLike(String value) {
            addCriterion("MANPASSWORD like", value, "manpassword");
            return (Criteria) this;
        }

        public Criteria andManpasswordNotLike(String value) {
            addCriterion("MANPASSWORD not like", value, "manpassword");
            return (Criteria) this;
        }

        public Criteria andManpasswordIn(List<String> values) {
            addCriterion("MANPASSWORD in", values, "manpassword");
            return (Criteria) this;
        }

        public Criteria andManpasswordNotIn(List<String> values) {
            addCriterion("MANPASSWORD not in", values, "manpassword");
            return (Criteria) this;
        }

        public Criteria andManpasswordBetween(String value1, String value2) {
            addCriterion("MANPASSWORD between", value1, value2, "manpassword");
            return (Criteria) this;
        }

        public Criteria andManpasswordNotBetween(String value1, String value2) {
            addCriterion("MANPASSWORD not between", value1, value2, "manpassword");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}