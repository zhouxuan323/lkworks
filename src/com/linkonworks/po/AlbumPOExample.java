package com.linkonworks.po;

import java.util.ArrayList;
import java.util.List;

public class AlbumPOExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AlbumPOExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andAdidIsNull() {
            addCriterion("ADID is null");
            return (Criteria) this;
        }

        public Criteria andAdidIsNotNull() {
            addCriterion("ADID is not null");
            return (Criteria) this;
        }

        public Criteria andAdidEqualTo(String value) {
            addCriterion("ADID =", value, "adid");
            return (Criteria) this;
        }

        public Criteria andAdidNotEqualTo(String value) {
            addCriterion("ADID <>", value, "adid");
            return (Criteria) this;
        }

        public Criteria andAdidGreaterThan(String value) {
            addCriterion("ADID >", value, "adid");
            return (Criteria) this;
        }

        public Criteria andAdidGreaterThanOrEqualTo(String value) {
            addCriterion("ADID >=", value, "adid");
            return (Criteria) this;
        }

        public Criteria andAdidLessThan(String value) {
            addCriterion("ADID <", value, "adid");
            return (Criteria) this;
        }

        public Criteria andAdidLessThanOrEqualTo(String value) {
            addCriterion("ADID <=", value, "adid");
            return (Criteria) this;
        }

        public Criteria andAdidLike(String value) {
            addCriterion("ADID like", value, "adid");
            return (Criteria) this;
        }

        public Criteria andAdidNotLike(String value) {
            addCriterion("ADID not like", value, "adid");
            return (Criteria) this;
        }

        public Criteria andAdidIn(List<String> values) {
            addCriterion("ADID in", values, "adid");
            return (Criteria) this;
        }

        public Criteria andAdidNotIn(List<String> values) {
            addCriterion("ADID not in", values, "adid");
            return (Criteria) this;
        }

        public Criteria andAdidBetween(String value1, String value2) {
            addCriterion("ADID between", value1, value2, "adid");
            return (Criteria) this;
        }

        public Criteria andAdidNotBetween(String value1, String value2) {
            addCriterion("ADID not between", value1, value2, "adid");
            return (Criteria) this;
        }

        public Criteria andLngIsNull() {
            addCriterion("LNG is null");
            return (Criteria) this;
        }

        public Criteria andLngIsNotNull() {
            addCriterion("LNG is not null");
            return (Criteria) this;
        }

        public Criteria andLngEqualTo(String value) {
            addCriterion("LNG =", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotEqualTo(String value) {
            addCriterion("LNG <>", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngGreaterThan(String value) {
            addCriterion("LNG >", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngGreaterThanOrEqualTo(String value) {
            addCriterion("LNG >=", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngLessThan(String value) {
            addCriterion("LNG <", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngLessThanOrEqualTo(String value) {
            addCriterion("LNG <=", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngLike(String value) {
            addCriterion("LNG like", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotLike(String value) {
            addCriterion("LNG not like", value, "lng");
            return (Criteria) this;
        }

        public Criteria andLngIn(List<String> values) {
            addCriterion("LNG in", values, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotIn(List<String> values) {
            addCriterion("LNG not in", values, "lng");
            return (Criteria) this;
        }

        public Criteria andLngBetween(String value1, String value2) {
            addCriterion("LNG between", value1, value2, "lng");
            return (Criteria) this;
        }

        public Criteria andLngNotBetween(String value1, String value2) {
            addCriterion("LNG not between", value1, value2, "lng");
            return (Criteria) this;
        }

        public Criteria andDtidIsNull() {
            addCriterion("DTID is null");
            return (Criteria) this;
        }

        public Criteria andDtidIsNotNull() {
            addCriterion("DTID is not null");
            return (Criteria) this;
        }

        public Criteria andDtidEqualTo(String value) {
            addCriterion("DTID =", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidNotEqualTo(String value) {
            addCriterion("DTID <>", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidGreaterThan(String value) {
            addCriterion("DTID >", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidGreaterThanOrEqualTo(String value) {
            addCriterion("DTID >=", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidLessThan(String value) {
            addCriterion("DTID <", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidLessThanOrEqualTo(String value) {
            addCriterion("DTID <=", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidLike(String value) {
            addCriterion("DTID like", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidNotLike(String value) {
            addCriterion("DTID not like", value, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidIn(List<String> values) {
            addCriterion("DTID in", values, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidNotIn(List<String> values) {
            addCriterion("DTID not in", values, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidBetween(String value1, String value2) {
            addCriterion("DTID between", value1, value2, "dtid");
            return (Criteria) this;
        }

        public Criteria andDtidNotBetween(String value1, String value2) {
            addCriterion("DTID not between", value1, value2, "dtid");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("TYPE is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("TYPE =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("TYPE <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("TYPE >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("TYPE >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("TYPE <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("TYPE <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("TYPE like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("TYPE not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("TYPE in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("TYPE not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("TYPE between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("TYPE not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("TITLE is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("TITLE is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(String value) {
            addCriterion("TITLE =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(String value) {
            addCriterion("TITLE <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(String value) {
            addCriterion("TITLE >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(String value) {
            addCriterion("TITLE >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(String value) {
            addCriterion("TITLE <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(String value) {
            addCriterion("TITLE <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLike(String value) {
            addCriterion("TITLE like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotLike(String value) {
            addCriterion("TITLE not like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<String> values) {
            addCriterion("TITLE in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<String> values) {
            addCriterion("TITLE not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(String value1, String value2) {
            addCriterion("TITLE between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(String value1, String value2) {
            addCriterion("TITLE not between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("CONTENT is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("CONTENT is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("CONTENT =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("CONTENT <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("CONTENT >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("CONTENT >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("CONTENT <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("CONTENT <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("CONTENT like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("CONTENT not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("CONTENT in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("CONTENT not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("CONTENT between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("CONTENT not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andImgurlIsNull() {
            addCriterion("IMGURL is null");
            return (Criteria) this;
        }

        public Criteria andImgurlIsNotNull() {
            addCriterion("IMGURL is not null");
            return (Criteria) this;
        }

        public Criteria andImgurlEqualTo(String value) {
            addCriterion("IMGURL =", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlNotEqualTo(String value) {
            addCriterion("IMGURL <>", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlGreaterThan(String value) {
            addCriterion("IMGURL >", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlGreaterThanOrEqualTo(String value) {
            addCriterion("IMGURL >=", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlLessThan(String value) {
            addCriterion("IMGURL <", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlLessThanOrEqualTo(String value) {
            addCriterion("IMGURL <=", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlLike(String value) {
            addCriterion("IMGURL like", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlNotLike(String value) {
            addCriterion("IMGURL not like", value, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlIn(List<String> values) {
            addCriterion("IMGURL in", values, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlNotIn(List<String> values) {
            addCriterion("IMGURL not in", values, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlBetween(String value1, String value2) {
            addCriterion("IMGURL between", value1, value2, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgurlNotBetween(String value1, String value2) {
            addCriterion("IMGURL not between", value1, value2, "imgurl");
            return (Criteria) this;
        }

        public Criteria andImgfilenameIsNull() {
            addCriterion("IMGFILENAME is null");
            return (Criteria) this;
        }

        public Criteria andImgfilenameIsNotNull() {
            addCriterion("IMGFILENAME is not null");
            return (Criteria) this;
        }

        public Criteria andImgfilenameEqualTo(String value) {
            addCriterion("IMGFILENAME =", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameNotEqualTo(String value) {
            addCriterion("IMGFILENAME <>", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameGreaterThan(String value) {
            addCriterion("IMGFILENAME >", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameGreaterThanOrEqualTo(String value) {
            addCriterion("IMGFILENAME >=", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameLessThan(String value) {
            addCriterion("IMGFILENAME <", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameLessThanOrEqualTo(String value) {
            addCriterion("IMGFILENAME <=", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameLike(String value) {
            addCriterion("IMGFILENAME like", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameNotLike(String value) {
            addCriterion("IMGFILENAME not like", value, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameIn(List<String> values) {
            addCriterion("IMGFILENAME in", values, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameNotIn(List<String> values) {
            addCriterion("IMGFILENAME not in", values, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameBetween(String value1, String value2) {
            addCriterion("IMGFILENAME between", value1, value2, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgfilenameNotBetween(String value1, String value2) {
            addCriterion("IMGFILENAME not between", value1, value2, "imgfilename");
            return (Criteria) this;
        }

        public Criteria andImgaltIsNull() {
            addCriterion("IMGALT is null");
            return (Criteria) this;
        }

        public Criteria andImgaltIsNotNull() {
            addCriterion("IMGALT is not null");
            return (Criteria) this;
        }

        public Criteria andImgaltEqualTo(String value) {
            addCriterion("IMGALT =", value, "imgalt");
            return (Criteria) this;
        }

        public Criteria andImgaltNotEqualTo(String value) {
            addCriterion("IMGALT <>", value, "imgalt");
            return (Criteria) this;
        }

        public Criteria andImgaltGreaterThan(String value) {
            addCriterion("IMGALT >", value, "imgalt");
            return (Criteria) this;
        }

        public Criteria andImgaltGreaterThanOrEqualTo(String value) {
            addCriterion("IMGALT >=", value, "imgalt");
            return (Criteria) this;
        }

        public Criteria andImgaltLessThan(String value) {
            addCriterion("IMGALT <", value, "imgalt");
            return (Criteria) this;
        }

        public Criteria andImgaltLessThanOrEqualTo(String value) {
            addCriterion("IMGALT <=", value, "imgalt");
            return (Criteria) this;
        }

        public Criteria andImgaltLike(String value) {
            addCriterion("IMGALT like", value, "imgalt");
            return (Criteria) this;
        }

        public Criteria andImgaltNotLike(String value) {
            addCriterion("IMGALT not like", value, "imgalt");
            return (Criteria) this;
        }

        public Criteria andImgaltIn(List<String> values) {
            addCriterion("IMGALT in", values, "imgalt");
            return (Criteria) this;
        }

        public Criteria andImgaltNotIn(List<String> values) {
            addCriterion("IMGALT not in", values, "imgalt");
            return (Criteria) this;
        }

        public Criteria andImgaltBetween(String value1, String value2) {
            addCriterion("IMGALT between", value1, value2, "imgalt");
            return (Criteria) this;
        }

        public Criteria andImgaltNotBetween(String value1, String value2) {
            addCriterion("IMGALT not between", value1, value2, "imgalt");
            return (Criteria) this;
        }

        public Criteria andClickIsNull() {
            addCriterion("CLICK is null");
            return (Criteria) this;
        }

        public Criteria andClickIsNotNull() {
            addCriterion("CLICK is not null");
            return (Criteria) this;
        }

        public Criteria andClickEqualTo(String value) {
            addCriterion("CLICK =", value, "click");
            return (Criteria) this;
        }

        public Criteria andClickNotEqualTo(String value) {
            addCriterion("CLICK <>", value, "click");
            return (Criteria) this;
        }

        public Criteria andClickGreaterThan(String value) {
            addCriterion("CLICK >", value, "click");
            return (Criteria) this;
        }

        public Criteria andClickGreaterThanOrEqualTo(String value) {
            addCriterion("CLICK >=", value, "click");
            return (Criteria) this;
        }

        public Criteria andClickLessThan(String value) {
            addCriterion("CLICK <", value, "click");
            return (Criteria) this;
        }

        public Criteria andClickLessThanOrEqualTo(String value) {
            addCriterion("CLICK <=", value, "click");
            return (Criteria) this;
        }

        public Criteria andClickLike(String value) {
            addCriterion("CLICK like", value, "click");
            return (Criteria) this;
        }

        public Criteria andClickNotLike(String value) {
            addCriterion("CLICK not like", value, "click");
            return (Criteria) this;
        }

        public Criteria andClickIn(List<String> values) {
            addCriterion("CLICK in", values, "click");
            return (Criteria) this;
        }

        public Criteria andClickNotIn(List<String> values) {
            addCriterion("CLICK not in", values, "click");
            return (Criteria) this;
        }

        public Criteria andClickBetween(String value1, String value2) {
            addCriterion("CLICK between", value1, value2, "click");
            return (Criteria) this;
        }

        public Criteria andClickNotBetween(String value1, String value2) {
            addCriterion("CLICK not between", value1, value2, "click");
            return (Criteria) this;
        }

        public Criteria andIstimeIsNull() {
            addCriterion("ISTIME is null");
            return (Criteria) this;
        }

        public Criteria andIstimeIsNotNull() {
            addCriterion("ISTIME is not null");
            return (Criteria) this;
        }

        public Criteria andIstimeEqualTo(Short value) {
            addCriterion("ISTIME =", value, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeNotEqualTo(Short value) {
            addCriterion("ISTIME <>", value, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeGreaterThan(Short value) {
            addCriterion("ISTIME >", value, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeGreaterThanOrEqualTo(Short value) {
            addCriterion("ISTIME >=", value, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeLessThan(Short value) {
            addCriterion("ISTIME <", value, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeLessThanOrEqualTo(Short value) {
            addCriterion("ISTIME <=", value, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeIn(List<Short> values) {
            addCriterion("ISTIME in", values, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeNotIn(List<Short> values) {
            addCriterion("ISTIME not in", values, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeBetween(Short value1, Short value2) {
            addCriterion("ISTIME between", value1, value2, "istime");
            return (Criteria) this;
        }

        public Criteria andIstimeNotBetween(Short value1, Short value2) {
            addCriterion("ISTIME not between", value1, value2, "istime");
            return (Criteria) this;
        }

        public Criteria andStarttimeIsNull() {
            addCriterion("STARTTIME is null");
            return (Criteria) this;
        }

        public Criteria andStarttimeIsNotNull() {
            addCriterion("STARTTIME is not null");
            return (Criteria) this;
        }

        public Criteria andStarttimeEqualTo(Long value) {
            addCriterion("STARTTIME =", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotEqualTo(Long value) {
            addCriterion("STARTTIME <>", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeGreaterThan(Long value) {
            addCriterion("STARTTIME >", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeGreaterThanOrEqualTo(Long value) {
            addCriterion("STARTTIME >=", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeLessThan(Long value) {
            addCriterion("STARTTIME <", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeLessThanOrEqualTo(Long value) {
            addCriterion("STARTTIME <=", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeIn(List<Long> values) {
            addCriterion("STARTTIME in", values, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotIn(List<Long> values) {
            addCriterion("STARTTIME not in", values, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeBetween(Long value1, Long value2) {
            addCriterion("STARTTIME between", value1, value2, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotBetween(Long value1, Long value2) {
            addCriterion("STARTTIME not between", value1, value2, "starttime");
            return (Criteria) this;
        }

        public Criteria andEndtimeIsNull() {
            addCriterion("ENDTIME is null");
            return (Criteria) this;
        }

        public Criteria andEndtimeIsNotNull() {
            addCriterion("ENDTIME is not null");
            return (Criteria) this;
        }

        public Criteria andEndtimeEqualTo(Long value) {
            addCriterion("ENDTIME =", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotEqualTo(Long value) {
            addCriterion("ENDTIME <>", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeGreaterThan(Long value) {
            addCriterion("ENDTIME >", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeGreaterThanOrEqualTo(Long value) {
            addCriterion("ENDTIME >=", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeLessThan(Long value) {
            addCriterion("ENDTIME <", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeLessThanOrEqualTo(Long value) {
            addCriterion("ENDTIME <=", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeIn(List<Long> values) {
            addCriterion("ENDTIME in", values, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotIn(List<Long> values) {
            addCriterion("ENDTIME not in", values, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeBetween(Long value1, Long value2) {
            addCriterion("ENDTIME between", value1, value2, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotBetween(Long value1, Long value2) {
            addCriterion("ENDTIME not between", value1, value2, "endtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeIsNull() {
            addCriterion("ADDTIME is null");
            return (Criteria) this;
        }

        public Criteria andAddtimeIsNotNull() {
            addCriterion("ADDTIME is not null");
            return (Criteria) this;
        }

        public Criteria andAddtimeEqualTo(Long value) {
            addCriterion("ADDTIME =", value, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeNotEqualTo(Long value) {
            addCriterion("ADDTIME <>", value, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeGreaterThan(Long value) {
            addCriterion("ADDTIME >", value, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeGreaterThanOrEqualTo(Long value) {
            addCriterion("ADDTIME >=", value, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeLessThan(Long value) {
            addCriterion("ADDTIME <", value, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeLessThanOrEqualTo(Long value) {
            addCriterion("ADDTIME <=", value, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeIn(List<Long> values) {
            addCriterion("ADDTIME in", values, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeNotIn(List<Long> values) {
            addCriterion("ADDTIME not in", values, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeBetween(Long value1, Long value2) {
            addCriterion("ADDTIME between", value1, value2, "addtime");
            return (Criteria) this;
        }

        public Criteria andAddtimeNotBetween(Long value1, Long value2) {
            addCriterion("ADDTIME not between", value1, value2, "addtime");
            return (Criteria) this;
        }

        public Criteria andSequenceIsNull() {
            addCriterion("SEQUENCE is null");
            return (Criteria) this;
        }

        public Criteria andSequenceIsNotNull() {
            addCriterion("SEQUENCE is not null");
            return (Criteria) this;
        }

        public Criteria andSequenceEqualTo(Long value) {
            addCriterion("SEQUENCE =", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceNotEqualTo(Long value) {
            addCriterion("SEQUENCE <>", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceGreaterThan(Long value) {
            addCriterion("SEQUENCE >", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceGreaterThanOrEqualTo(Long value) {
            addCriterion("SEQUENCE >=", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceLessThan(Long value) {
            addCriterion("SEQUENCE <", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceLessThanOrEqualTo(Long value) {
            addCriterion("SEQUENCE <=", value, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceIn(List<Long> values) {
            addCriterion("SEQUENCE in", values, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceNotIn(List<Long> values) {
            addCriterion("SEQUENCE not in", values, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceBetween(Long value1, Long value2) {
            addCriterion("SEQUENCE between", value1, value2, "sequence");
            return (Criteria) this;
        }

        public Criteria andSequenceNotBetween(Long value1, Long value2) {
            addCriterion("SEQUENCE not between", value1, value2, "sequence");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}