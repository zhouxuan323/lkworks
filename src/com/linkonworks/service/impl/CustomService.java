package com.linkonworks.service.impl;

import com.linkonworks.dao.inf.CustomPOMapper;
import com.linkonworks.po.CustomPO;
import com.linkonworks.po.CustomPOExample;
import com.linkonworks.service.inf.CustomServiceInf;
import com.linkonworks.vo.CustomVO;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class CustomService implements CustomServiceInf{

	@Resource
	private CustomPOMapper customDao;
	
	@Override
	public List<CustomVO> selectByExample(CustomVO customVO) {
		List<CustomVO> rlist = new ArrayList<CustomVO>();

		List<CustomPO> list = new ArrayList<CustomPO>();

		if (customVO == null) {
			list = customDao.selectByExample(null);
		} else {
			CustomPOExample example = new CustomPOExample();
			CustomPOExample.Criteria criteria = example.createCriteria();
			example.setOrderByClause("SEQUENCE");

			String lng = customVO.getLng();
			if (!StringUtils.isBlank(lng)) {
				criteria.andLngEqualTo(lng);
			}
			String dtid = customVO.getDtid();
			if (!StringUtils.isBlank(lng)) {
				criteria.andDtidEqualTo(dtid);
			}
			
			list = customDao.selectByExample(example);
		}
		for (CustomPO po : list) {
			CustomVO vo = new CustomVO();
			BeanUtils.copyProperties(po, vo);
			rlist.add(vo);
		}

		// TODO Auto-generated method stub
		return rlist;
	}
 
}