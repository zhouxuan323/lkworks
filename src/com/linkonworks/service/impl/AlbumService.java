package com.linkonworks.service.impl;

import javax.annotation.Resource;



import java.util.ArrayList;
import java.util.List;







import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.linkonworks.dao.inf.AlbumPOMapper;
import com.linkonworks.po.AlbumPOExample;
import com.linkonworks.po.AlbumPO;
import com.linkonworks.service.inf.AlbumServiceInf;
import com.linkonworks.utils.ApplicationConstant;
import com.linkonworks.utils.IDGenUtils;
import com.linkonworks.vo.AlbumVO;


@Service
public class AlbumService implements AlbumServiceInf{

	
	@Resource
	private AlbumPOMapper albumDao;
	
	
	@Override
	public List<AlbumVO> selectByExample(AlbumVO albumVO) {
		List<AlbumVO> rlist = new ArrayList<AlbumVO>();

		List<AlbumPO> list = new ArrayList<AlbumPO>();

		if (albumVO == null) {
			list = albumDao.selectByExample(null);
		} else {
			AlbumPOExample example = new AlbumPOExample();
			AlbumPOExample.Criteria criteria = example.createCriteria();
			example.setOrderByClause("SEQUENCE");

			String lng = albumVO.getLng();
			if (!StringUtils.isBlank(lng)) {
				criteria.andLngEqualTo(lng);
			}
			String dtid = albumVO.getDtid();
			if (!StringUtils.isBlank(lng)) {
				criteria.andDtidEqualTo(dtid);
			}
			
			list = albumDao.selectByExample(example);
		}
		for (AlbumPO po : list) {
			AlbumVO vo = new AlbumVO();
			BeanUtils.copyProperties(po, vo);
			rlist.add(vo);
		}

		// TODO Auto-generated method stub
		return rlist;
	}

@Override
public int delteAlbum(String dtid) {
	//调用内置判断条件
	AlbumPOExample example = new AlbumPOExample();
	AlbumPOExample.Criteria criteria = example.createCriteria();
	
	//以dtid作为删除条件
	if (!StringUtils.isBlank(dtid)) {
		criteria.andDtidEqualTo(dtid);
	}
	//调用dao层进行删除
	int num=albumDao.deleteByExample(example);

	return num;
}

@Override
public int insertAlbumVOList(List<String> fileNameList) {
	int num=0;
	String imgurl = ApplicationConstant.IMGURL;
	for(String filename:fileNameList)
	{
		//自动生成图片的主键
		AlbumPO record=new AlbumPO();
		String adid=IDGenUtils.gen();
		record.setAdid(adid);
		record.setImgfilename(filename);
		record.setImgurl(imgurl);
		
		//调用dao层进行图片插入
		albumDao.insertSelective(record);	
	}
	num=num+1;
	
	return num;
}

@Override
public int insert(AlbumVO albumVO) {
	// TODO Auto-generated method stub
	return 0;
}

@Override
public int delteAlbumstr(String adidstr) {
	String[] adids=adidstr.split("\\|");
	for(String adid:adids){
		albumDao.deleteByPrimaryKey(adid);
		
	}
	return 0;
}
}