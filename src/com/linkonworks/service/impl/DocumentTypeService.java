package com.linkonworks.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.linkonworks.dao.inf.AlbumPOMapper;
import com.linkonworks.dao.inf.DocumentPOMapper;
import com.linkonworks.dao.inf.DocumentTypePOMapper;
import com.linkonworks.po.AlbumPO;
import com.linkonworks.po.DocumentTypePO;
import com.linkonworks.po.DocumentTypePOExample;
import com.linkonworks.service.inf.AlbumServiceInf;
import com.linkonworks.service.inf.DocumentServiceInf;
import com.linkonworks.service.inf.DocumentTypeServiceInf;
import com.linkonworks.utils.ApplicationConstant;
import com.linkonworks.utils.IDGenUtils;
import com.linkonworks.utils.UploadUtil;
import com.linkonworks.vo.DocumentTypeVO;
import com.linkonworks.vo.DocumentVO;
import com.linkonworks.vo.FileVO;
import com.linkonworks.vo.MenuVO;
import com.linkonworks.vo.ModelVO;

@Service
public class DocumentTypeService implements DocumentTypeServiceInf {

	@Resource
	private DocumentTypePOMapper documentTypeDao;

	@Resource
	public DocumentServiceInf documentService;

	@Resource
	public AlbumServiceInf albumService;

	@Resource
	private AlbumPOMapper albumDao;
	
	@Resource
	public DocumentPOMapper documentDao;
	
	long documentsequence=0;
	
	long albumsequence=0;

	@Override
	public List<DocumentTypeVO> selectByExample(DocumentTypeVO documentTypeVO) {

		List<DocumentTypeVO> rlist = new ArrayList<DocumentTypeVO>();
		List<DocumentTypePO> list = new ArrayList<DocumentTypePO>();

		if (documentTypeVO == null) {
			list = documentTypeDao.selectByExample(null);
		} else {
			DocumentTypePOExample example = new DocumentTypePOExample();
			DocumentTypePOExample.Criteria criteria = example.createCriteria();

			String mid = documentTypeVO.getMid();
			if (!StringUtils.isBlank(mid)) {
				criteria.andMidEqualTo(mid);
			}

			list = documentTypeDao.selectByExample(example);
		}
		for (DocumentTypePO po : list) {
			DocumentTypeVO vo = new DocumentTypeVO();
			BeanUtils.copyProperties(po, vo);
			rlist.add(vo);
		}
		return rlist;

	}

	public int insert(DocumentTypeVO dtvo) {
		DocumentTypePO dtpo = new DocumentTypePO();
		dtpo.setDtid(dtvo.getDtid());
		dtpo.setLng(dtvo.getLng());
		dtpo.setMid(dtvo.getMid());
		dtpo.setModelid(dtvo.getModelid());
		dtpo.setLng(ApplicationConstant.LNG_CN);
		int num = documentTypeDao.insert(dtpo);
		return num;
	}

	// 保存内容和文档的方法
	@Transactional
	@Override
	public void save(MenuVO menuVO,String adidstr,List<DocumentVO> documentList,List<File> file) {
		if(!StringUtils.isBlank(adidstr)){
			albumService.delteAlbumstr(adidstr);
			// 调用上传方法，得到文件的名字
			List<String> fileNameList = UploadUtil.upload(file);

			// 声明对象、获取mid
			DocumentTypeVO dttv = new DocumentTypeVO();
			dttv.setMid(menuVO.getMid());
			String lng=menuVO.getLng();
			// 通过mid查询documenttype
			List<DocumentTypeVO> documenttypeList = selectByExample(dttv);
			String dtid = documenttypeList.get(0).getDtid();

			// 插入类型数据
			if (!StringUtils.isBlank(dtid)) {
				// 删除原有内容
				documentService.deleteDocument(dtid);
			}
			
			if(documentList==null){
				documentService.deleteDocument(dtid);
			}else{
				// 循环插入内容
				for (DocumentVO documentVO : documentList) {
					if(documentVO!=null){
						
						documentsequence=documentsequence+1;
						documentVO.setDaid(IDGenUtils.gen());
						documentVO.setDtid(dtid);
						documentVO.setSequence(documentsequence);
						documentVO.setLng(lng);
						documentService.insert(documentVO);
					}
					continue;
				}
			}

			// 循环插入图片
			String imgurl = ApplicationConstant.IMGURL;
			for (String filename : fileNameList) {
				albumsequence=albumsequence+1;
				// 自动生成图片的主键
				AlbumPO record = new AlbumPO();
				String adid = IDGenUtils.gen();
				record.setAdid(adid);
				record.setDtid(dtid);
				record.setImgfilename(filename);
				record.setImgurl(imgurl);
				record.setSequence(albumsequence);
				record.setLng(lng);
				// 调用dao层进行图片插入
				albumDao.insertSelective(record);
			}
		}
		
		// 调用上传方法，得到文件的名字
		List<String> fileNameList = UploadUtil.upload(file);

		// 声明对象、获取mid
		DocumentTypeVO dttv = new DocumentTypeVO();
		dttv.setMid(menuVO.getMid());
		String lng=menuVO.getLng();
		// 通过mid查询documenttype
		List<DocumentTypeVO> documenttypeList = selectByExample(dttv);
		String dtid = documenttypeList.get(0).getDtid();

		// 插入类型数据
		if (!StringUtils.isBlank(dtid)) {
			// 删除原有内容
			documentService.deleteDocument(dtid);
		}

		if(documentList==null){
			documentService.deleteDocument(dtid);
		}else{
			
			// 循环插入内容
			for (DocumentVO documentVO : documentList) {
				if(documentVO!=null){
					
					documentsequence=documentsequence+1;
					documentVO.setDaid(IDGenUtils.gen());
					documentVO.setDtid(dtid);
					documentVO.setSequence(documentsequence);
					documentVO.setLng(lng);
					documentService.insert(documentVO);
				}
				continue;
			}
		}

		// 循环插入图片
		String imgurl = ApplicationConstant.IMGURL;
		for (String filename : fileNameList) {
			albumsequence=albumsequence+1;
			// 自动生成图片的主键
			AlbumPO record = new AlbumPO();
			String adid = IDGenUtils.gen();
			record.setAdid(adid);
			record.setDtid(dtid);
			record.setImgfilename(filename);
			record.setImgurl(imgurl);
			record.setSequence(albumsequence);
			record.setLng(lng);
			// 调用dao层进行图片插入
			albumDao.insertSelective(record);
		}
	}

	//保存只有内容的方法
	@Transactional
	@Override
	public void saveDocument(MenuVO menuVO,String adidstr,List<DocumentVO> documentList) {
		if(!StringUtils.isBlank(adidstr)){
			albumService.delteAlbumstr(adidstr);
			// 声明对象、获取mid
			DocumentTypeVO dttv = new DocumentTypeVO();
			dttv.setMid(menuVO.getMid());
			String lng=menuVO.getLng();
			// 通过mid查询documenttype
			List<DocumentTypeVO> documenttypeList = selectByExample(dttv);
			String dtid = documenttypeList.get(0).getDtid();

			if (!StringUtils.isBlank(dtid)) {
				// 删除原有内容
				documentService.deleteDocument(dtid);

			}
			if(documentList==null){
				documentService.deleteDocument(dtid);
			}else{
				
				// 循环插入内容
				for (DocumentVO documentVO : documentList) {
						if(documentVO!=null){	
							documentsequence=documentsequence+1;
							documentVO.setDaid(IDGenUtils.gen());
							documentVO.setDtid(dtid);
							documentVO.setSequence(documentsequence);
							documentVO.setLng(lng);
							documentService.insert(documentVO);
						}
						continue;
					
				}
			}
		}
		// 声明对象、获取mid
		DocumentTypeVO dttv = new DocumentTypeVO();
		dttv.setMid(menuVO.getMid());
		String lng=menuVO.getLng();
		// 通过mid查询documenttype
		List<DocumentTypeVO> documenttypeList = selectByExample(dttv);
		String dtid = documenttypeList.get(0).getDtid();

		if (!StringUtils.isBlank(dtid)) {
			// 删除原有内容
			documentService.deleteDocument(dtid);

		}
		if(documentList==null){
			documentService.deleteDocument(dtid);
		}else{
			
			// 循环插入内容
			for (DocumentVO documentVO : documentList) {
				if(documentVO!=null){
					
					documentsequence=documentsequence+1;
					documentVO.setDaid(IDGenUtils.gen());
					documentVO.setDtid(dtid);
					documentVO.setSequence(documentsequence);
					documentVO.setLng(lng);
					documentService.insert(documentVO);
				}
				continue;
			}
		}

	}
	
	//保存只有图片文件的方法
	@Transactional
	@Override
	public void saveFile(MenuVO menuVO,String adidstr,List<File> file) {
		if(!StringUtils.isBlank(adidstr)){
			albumService.delteAlbumstr(adidstr);
			// 调用上传方法，得到文件的名字
			List<String> fileNameList = UploadUtil.upload(file);

			// 声明对象、获取mid
			DocumentTypeVO dttv = new DocumentTypeVO();
			dttv.setMid(menuVO.getMid());
			String lng=menuVO.getLng();
			// 通过mid查询documenttype
			List<DocumentTypeVO> documenttypeList = selectByExample(dttv);
			String dtid = documenttypeList.get(0).getDtid();

			if (!StringUtils.isBlank(dtid)) {
				// 删除原有图片
				albumService.delteAlbum(dtid);

			}

			// 循环插入图片
			String imgurl = ApplicationConstant.IMGURL;
			for (String filename : fileNameList) {
				albumsequence=albumsequence+1;
				// 自动生成图片的主键
				AlbumPO record = new AlbumPO();
				String adid = IDGenUtils.gen();
				record.setAdid(adid);
				record.setDtid(dtid);
				record.setImgfilename(filename);
				record.setImgurl(imgurl);
				record.setSequence(albumsequence);
				record.setLng(lng);

				// 调用dao层进行图片插入
				albumDao.insertSelective(record);
			}
		}
		// 调用上传方法，得到文件的名字
		List<String> fileNameList = UploadUtil.upload(file);

		// 声明对象、获取mid
		DocumentTypeVO dttv = new DocumentTypeVO();
		dttv.setMid(menuVO.getMid());
		String lng=menuVO.getLng();
		// 通过mid查询documenttype
		List<DocumentTypeVO> documenttypeList = selectByExample(dttv);
		String dtid = documenttypeList.get(0).getDtid();

		if (!StringUtils.isBlank(dtid)) {
			// 删除原有图片
			albumService.delteAlbum(dtid);

		}

		// 循环插入图片
		String imgurl = ApplicationConstant.IMGURL;
		for (String filename : fileNameList) {
			albumsequence=albumsequence+1;
			// 自动生成图片的主键
			AlbumPO record = new AlbumPO();
			String adid = IDGenUtils.gen();
			record.setAdid(adid);
			record.setDtid(dtid);
			record.setImgfilename(filename);
			record.setImgurl(imgurl);
			record.setSequence(albumsequence);
			record.setLng(lng);

			// 调用dao层进行图片插入
			albumDao.insertSelective(record);
		}
	}
	
	// 通过模板插类型
		@Override
		public void insert(MenuVO menuVO, ModelVO modelVO) {
			String dtidt = IDGenUtils.gen();
			DocumentTypePO dtv = new DocumentTypePO();
			dtv.setDtid(dtidt);
			dtv.setMid(menuVO.getMid());
			dtv.setModelid(modelVO.getModelid());
			documentTypeDao.insertSelective(dtv);
		}

		@Override
		public int deleteDocumentDaidstr(String daidstr) {
			int num=0;
			String[] strings=daidstr.split("\\|");
			for(String daid:strings){
				if(!StringUtils.isBlank(daid)){
					documentDao.deleteByPrimaryKey(daid);
				}
				num+=1;
			}
			return num;
		}

		@Transactional
		@Override
		public int saveFileVOList(MenuVO menuVo,String daidstr,List<FileVO> fileVOList) {
			if(!StringUtils.isBlank(daidstr)){
				documentService.deleteDocumentDaidstr(daidstr);
				DocumentTypeVO dttv = new DocumentTypeVO();
				dttv.setMid(menuVo.getMid());
				String lng=menuVo.getLng();
				// 通过mid查询documenttype
				List<DocumentTypeVO> documenttypeList = selectByExample(dttv);
				String dtid = documenttypeList.get(0).getDtid();
				int num=0;
				for(FileVO fileVO:fileVOList){
					documentsequence=documentsequence+1;
					List<File> files = new ArrayList<File>();
					File file = fileVO.getFileVO();
					files.add(file);
					DocumentVO documentVO = fileVO.getDocumentVO();
					List<String> filenames=UploadUtil.upload(files);
					String filename=filenames.get(0);
					documentVO.setImgfilename(filename);
					documentVO.setSequence(documentsequence);
					documentVO.setDaid(IDGenUtils.gen());
					documentVO.setDtid(dtid);
					documentVO.setLng(lng);
					documentVO.setImgurl(ApplicationConstant.IMGURL);
					documentService.insert(documentVO);
					
					num+=1;
				}
				return num;	
			}
			DocumentTypeVO dttv = new DocumentTypeVO();
			dttv.setMid(menuVo.getMid());
			String lng=menuVo.getLng();
			// 通过mid查询documenttype
			List<DocumentTypeVO> documenttypeList = selectByExample(dttv);
			String dtid = documenttypeList.get(0).getDtid();
			int num=0;
			for(FileVO fileVO:fileVOList){
				documentsequence=documentsequence+1;
				List<File> files = new ArrayList<File>();
				File file = fileVO.getFileVO();
				files.add(file);
				DocumentVO documentVO = fileVO.getDocumentVO();
				List<String> filenames=UploadUtil.upload(files);
				String filename=filenames.get(0);
				documentVO.setImgfilename(filename);
				documentVO.setSequence(documentsequence);
				documentVO.setDaid(IDGenUtils.gen());
				documentVO.setDtid(dtid);
				documentVO.setLng(lng);
				documentVO.setImgurl(ApplicationConstant.IMGURL);
				documentService.insert(documentVO);
				
				num+=1;
			}
			return num;
		}

		@Override
		public int saveLxwmList(MenuVO menuVo, List<DocumentVO> documentList) {
			String lng=menuVo.getLng();
			DocumentTypeVO dttv = new DocumentTypeVO();
			dttv.setMid(menuVo.getMid());
			// 通过mid查询documenttype
			List<DocumentTypeVO> documenttypeList = selectByExample(dttv);
			String dtid = documenttypeList.get(0).getDtid();
			if(documentList==null){
				
				documentService.deleteDocument(dtid);	
			}else{
				documentService.deleteDocument(dtid);	
				for (DocumentVO documentVO : documentList) {
					documentsequence=documentsequence+1;
					documentVO.setDaid(IDGenUtils.gen());
					documentVO.setDtid(dtid);
					documentVO.setSequence(documentsequence);
					documentVO.setLng(lng);
					documentService.insert(documentVO);
				}
				
			}
			return 0;
		}
}
