package com.linkonworks.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.linkonworks.dao.inf.AdvertPOMapper;
import com.linkonworks.dao.inf.DocTransferPOMapper;
import com.linkonworks.dao.inf.DocumentTypePOMapper;
import com.linkonworks.dao.inf.ImgTransferPOMapper;
import com.linkonworks.dao.inf.MenuPOMapper;
import com.linkonworks.po.AdvertPO;
import com.linkonworks.po.AdvertPOExample;
import com.linkonworks.po.DocTransferPO;
import com.linkonworks.po.DocTransferPOExample;
import com.linkonworks.po.DocumentPOExample;
import com.linkonworks.po.DocumentTypePO;
import com.linkonworks.po.DocumentTypePOExample;
import com.linkonworks.po.ImgTransferPO;
import com.linkonworks.po.ImgTransferPOExample;
import com.linkonworks.po.MenuPO;
import com.linkonworks.po.MenuPOExample;
import com.linkonworks.service.inf.HomeServiceInf;
import com.linkonworks.utils.ApplicationConstant;
import com.linkonworks.utils.IDGenUtils;
import com.linkonworks.utils.UploadUtil;
import com.linkonworks.vo.AdvertVO;
import com.linkonworks.vo.DocTransferVO;
import com.linkonworks.vo.DocumentTypeVO;
import com.linkonworks.vo.ImgTransferVO;
import com.linkonworks.vo.MenuVO;

@Service
public class HomeService implements HomeServiceInf {
	@Resource
	private MenuPOMapper menuDao;
	@Resource
	private AdvertPOMapper advertDao;
	@Resource
	private ImgTransferPOMapper imgTransferDao;
	@Resource
	private DocTransferPOMapper docTransferDao;
	@Resource
	private DocumentTypePOMapper documentTypeDao;
	@Resource
	private HomeServiceInf homeService;

	@Override
	public List<AdvertVO> selectadvert(AdvertVO advertVO) {
		List<AdvertVO> list = new ArrayList<AdvertVO>();
		List<AdvertPO> advertpo = new ArrayList<AdvertPO>();
		AdvertPOExample example = new AdvertPOExample();
		AdvertPOExample.Criteria criteria = example.createCriteria();
		// 判断语言
		String lng = advertVO.getLng();
		// 非空判断
		if (!StringUtils.isBlank(lng)) {
			criteria.andLngEqualTo(lng);
		}
		advertpo = advertDao.selectByExample(example);
		for (AdvertPO po : advertpo) {
			AdvertVO vo = new AdvertVO();
			BeanUtils.copyProperties(po, vo);
			list.add(vo);
		}
		return list;
	}

	@Override
	public List<DocTransferVO> selectdoctransfer(DocTransferVO docTransferVO) {
		List<DocTransferVO> list = new ArrayList<DocTransferVO>();
		List<DocTransferPO> listpo = new ArrayList<DocTransferPO>();
		DocTransferPOExample example = new DocTransferPOExample();
		DocTransferPOExample.Criteria criteria = example.createCriteria();
		// 判断语言
		String lng = docTransferVO.getLng();
		// 非空判断
		if (!StringUtils.isBlank(lng)) {
			criteria.andLngEqualTo(lng);
		}
		listpo = docTransferDao.selectByExample(example);
		for (DocTransferPO po : listpo) {
			DocTransferVO vo = new DocTransferVO();
			BeanUtils.copyProperties(po, vo);
			list.add(vo);
		}
		return list;
	}

	@Override
	public List<ImgTransferVO> selectimgtransfer(ImgTransferVO imgTransferVO) {
		List<ImgTransferVO> list = new ArrayList<ImgTransferVO>();
		List<ImgTransferPO> listpo = new ArrayList<ImgTransferPO>();
		ImgTransferPOExample example = new ImgTransferPOExample();
		ImgTransferPOExample.Criteria criteria = example.createCriteria();
		// 判断语言
		String lng = imgTransferVO.getLng();
		// 非空判断
		if (!StringUtils.isBlank(lng)) {
			criteria.andLngEqualTo(lng);
		}
		listpo = imgTransferDao.selectByExample(example);
		for (ImgTransferPO po : listpo) {
			ImgTransferVO vo = new ImgTransferVO();
			BeanUtils.copyProperties(po, vo);
			list.add(vo);
		}
		return list;
	}

	@Override
	public void saveadvert(MenuVO menuVO, List<MenuVO> newmenuList,
			List<MenuVO> menuList, List<AdvertVO> advertList, List<File> file) {
		// 通过menuList的值来更改页面上图片的链接地址
		if (menuList != null) {
			int i = 0;
			for (MenuVO menulist : menuList) {
				List<MenuPO> menulistpo = new ArrayList<MenuPO>();
				MenuPOExample example = new MenuPOExample();
				MenuPOExample.Criteria criteria = example.createCriteria();
				String name = menulist.getName();
				if (!StringUtils.isBlank(name)) {
					criteria.andNameEqualTo(name);
				}
				AdvertVO advertVO = advertList.get(i);
				menulistpo = menuDao.selectByExample(example);
				String itid = advertVO.getItid();
				AdvertPO advertpo = advertDao.selectByPrimaryKey(itid);
				if (menulistpo.size() > 0) {
					String url = menulistpo.get(0).getUrl();
					advertpo.setTransferurl(url);
				} else {
					String url = "";
					advertpo.setTransferurl(url);
				}
				// 更改url的值
				advertDao.updateByPrimaryKey(advertpo);
				// advertVOs.add(advertVO);
				i++;
			}
		}
		if (newmenuList != null) {
			int j = 0;
			// 调用上传方法，得到文件的名字
			List<String> fileNameList = UploadUtil.upload(file);
			// 声明对象、获取mid
			DocumentTypeVO documentTypeVO = new DocumentTypeVO();
			documentTypeVO.setMid(menuVO.getMid());
			// 通过mid查询document type
			List<DocumentTypeVO> documenttypeList = selectByExample(documentTypeVO);
			String dtid = documenttypeList.get(0).getDtid();
			// 循环插入图片
			String imgurl = ApplicationConstant.IMGURL;
			Long sequence = 1L;
			for (String filename : fileNameList) {
				MenuVO newmenuVO = newmenuList.get(j);
				List<MenuPO> menulistpo = new ArrayList<MenuPO>();
				MenuPOExample example = new MenuPOExample();
				MenuPOExample.Criteria criteria = example.createCriteria();
				String name = newmenuVO.getName();
				if (!StringUtils.isBlank(name)) {
					criteria.andNameEqualTo(name);
				}
				menulistpo = menuDao.selectByExample(example);
				// 自动生成图片的主键
				AdvertPO advertPO = new AdvertPO();
				String itid = IDGenUtils.gen();
				advertPO.setItid(itid);
				advertPO.setDtid(dtid);
				advertPO.setLng(menuVO.getLng());
				advertPO.setFilename(filename);
				advertPO.setUrl(imgurl);
				if (menulistpo.size() > 0) {
					String url = menulistpo.get(0).getUrl();
					advertPO.setTransferurl(url);
				} else {
					String url = null;
					advertPO.setTransferurl(url);
				}
				advertPO.setSequence(sequence);
				sequence += 1;
				// 调用dao层进行图片插入
				advertDao.insertSelective(advertPO);
				j++;
			}
		}
	}

	@Override
	public List<DocumentTypeVO> selectByExample(DocumentTypeVO documentTypeVO) {

		List<DocumentTypeVO> rlist = new ArrayList<DocumentTypeVO>();
		List<DocumentTypePO> list = new ArrayList<DocumentTypePO>();

		if (documentTypeVO == null) {
			list = documentTypeDao.selectByExample(null);
		} else {
			DocumentTypePOExample example = new DocumentTypePOExample();
			DocumentTypePOExample.Criteria criteria = example.createCriteria();

			String mid = documentTypeVO.getMid();
			if (!StringUtils.isBlank(mid)) {
				criteria.andMidEqualTo(mid);
			}

			list = documentTypeDao.selectByExample(example);
		}
		for (DocumentTypePO po : list) {
			DocumentTypeVO vo = new DocumentTypeVO();
			BeanUtils.copyProperties(po, vo);
			rlist.add(vo);
		}
		return rlist;

	}

	@Override
	public void savedoctransfer(DocumentTypeVO documentTypeVO, MenuVO menuVO,
			List<DocTransferVO> docTransferList) {
		// 通过mid 查询url

		List<MenuPO> menupo = new ArrayList<MenuPO>();
		MenuPOExample example = new MenuPOExample();
		MenuPOExample.Criteria criteria = example.createCriteria();
		String name = menuVO.getName();
		if (!StringUtils.isBlank(name)) {
			criteria.andNameEqualTo(name);
		}
		// 菜单表中通过name查询器对应的url
		menupo = menuDao.selectByExample(example);
		// 设置url的值
		String url = menupo.get(0).getUrl();
		// 设置mid的值
		String mid = documentTypeVO.getMid();
		// 类型表中通过mid 查询到一条数据
		DocumentTypePO documentTypePO = documentTypeDao.selectByPrimaryKey(mid);
		DocumentTypeVO documentTypevo = new DocumentTypeVO();
		BeanUtils.copyProperties(documentTypePO, documentTypevo);
		DocumentTypePO po = new DocumentTypePO();
		po.setIstime(documentTypevo.getIstime());
		po.setTime(documentTypevo.getTime());
		po.setDtid(documentTypevo.getDtid());
		po.setModelid(documentTypevo.getModelid());
		po.setTransferurl(url);
		po.setTitle(documentTypeVO.getTitle());
		po.setLng(documentTypeVO.getLng());
		po.setMid(mid);
		// 更新类型表中的数据
		documentTypeDao.updateByPrimaryKeySelective(po);
		String dtid = documentTypevo.getDtid();
		// 根据dtid作为删除条件
		if (!StringUtils.isBlank(dtid)) {
			homeService.deletedoc(dtid);
		}
		// 循环插入内容(保存页面上的content)
		Long sequence = 1L;

		for (DocTransferVO documentVO : docTransferList) {
			if (!documentVO.getContent().equals("")) {
				documentVO.setLng(documentTypeVO.getLng());
				documentVO.setDtid(dtid);
				documentVO.setTitle(documentTypeVO.getTitle());
				documentVO.setSequence(sequence);
				documentVO.setTransferurl(url);
				homeService.insert(documentVO);
				sequence += 1;
			}
		}
	}

	@Override
	public void insert(DocTransferVO documentVO) {
		DocTransferPO dpo = new DocTransferPO();
		dpo.setId(IDGenUtils.gen());
		dpo.setLng(documentVO.getLng());
		dpo.setDtid(documentVO.getDtid());
		dpo.setTitle(documentVO.getTitle());
		dpo.setContent(documentVO.getContent());
		dpo.setSequence(documentVO.getSequence());
		dpo.setTransferurl(documentVO.getTransferurl());
		docTransferDao.insertSelective(dpo);
		return;
	}

	@Override
	public void saveimgtransfer(MenuVO menuVO, List<MenuVO> newmenuList,
			List<MenuVO> menuList, List<ImgTransferVO> imgTransferList,
			List<File> file) {
		// 通过menuList的值来更改页面上图片的链接地址
		if (menuList != null) {
			int i = 0;
			for (MenuVO menulist : menuList) {
				List<MenuPO> menulistpo = new ArrayList<MenuPO>();
				MenuPOExample example = new MenuPOExample();
				MenuPOExample.Criteria criteria = example.createCriteria();
				String name = menulist.getName();
				if (!StringUtils.isBlank(name)) {
					criteria.andNameEqualTo(name);
				}
				ImgTransferVO imgTransferVO = imgTransferList.get(i);
				// 通过名称来查询菜单表的数据
				menulistpo = menuDao.selectByExample(example);
				String itid = imgTransferVO.getItid();
				// 通过质检itid来查询图片链接表的数据
				ImgTransferPO imgTransferpo = imgTransferDao
						.selectByPrimaryKey(itid);
				if (menulistpo.size() > 0) {
					String url = menulistpo.get(0).getUrl();
					imgTransferpo.setTransferurl(url);
				} else {
					String url = null;
					imgTransferpo.setTransferurl(url);
				}
				// 更改url的值
				imgTransferDao.updateByPrimaryKey(imgTransferpo);
				i++;
			}
		}
		if (newmenuList != null) {
			int j = 0;
			// 调用上传方法，得到文件的名字
			List<String> fileNameList = UploadUtil.upload(file);
			// 声明对象、获取mid
			DocumentTypeVO documentTypeVO = new DocumentTypeVO();
			documentTypeVO.setMid(menuVO.getMid());
			// 通过mid查询document type
			List<DocumentTypeVO> documenttypeList = selectByExample(documentTypeVO);
			String dtid = documenttypeList.get(0).getDtid();
			// 根据dtid作为删除条件
			if (!StringUtils.isBlank(dtid)) {
				homeService.deleteimg(dtid);
			}
			// 循环插入图片
			String imgurl = ApplicationConstant.IMGURL;
			Long sequence = 1L;
			for (String filename : fileNameList) {
				MenuVO newmenuVO = newmenuList.get(j);
				List<MenuPO> menulistpo = new ArrayList<MenuPO>();
				MenuPOExample example = new MenuPOExample();
				MenuPOExample.Criteria criteria = example.createCriteria();
				String name = newmenuVO.getName();
				if (!StringUtils.isBlank(name)) {
					criteria.andNameEqualTo(name);
				}
				menulistpo = menuDao.selectByExample(example);
				// 自动生成图片的主键
				ImgTransferPO imgTransferPO = new ImgTransferPO();
				String itid = IDGenUtils.gen();
				imgTransferPO.setItid(itid);
				imgTransferPO.setDtid(dtid);
				imgTransferPO.setLng(menuVO.getLng());
				imgTransferPO.setFilename(filename);
				imgTransferPO.setUrl(imgurl);
				if (menulistpo.size() > 0) {
					String url = menulistpo.get(0).getUrl();
					imgTransferPO.setTransferurl(url);
				} else {
					String url = null;
					imgTransferPO.setTransferurl(url);
				}
				imgTransferPO.setSequence(sequence);
				sequence += 1;
				// 调用dao层进行图片插入
				imgTransferDao.insertSelective(imgTransferPO);
				j++;
			}
		}

	}

	@Override
	public void deletebyid(String adidstr) {
		String[] adids = adidstr.split("\\|");
		for (String adid : adids) {
			advertDao.deleteByPrimaryKey(adid);
		}
	}

	@Override
	public void deleteimg(String adidstr) {
		String[] adids = adidstr.split("\\|");
		for (String adid : adids) {
			imgTransferDao.deleteByPrimaryKey(adid);

		}
	}

	@Override
	public void deletedoc(String dtid) {
		// 调用内置的查询判断条件
		DocTransferPOExample example = new DocTransferPOExample();
		DocTransferPOExample.Criteria criteria = example.createCriteria();
		// 根据dtid作为删除条件
		if (!StringUtils.isBlank(dtid)) {
			criteria.andDtidEqualTo(dtid);
		}
		// 调用dao层进行删除
		int num = docTransferDao.deleteByExample(example);
		return;
	}

	@Override
	public int savetime(DocumentTypeVO documentTypeVO) {
		String mid = documentTypeVO.getMid();
		DocumentTypePO documentTypePO = documentTypeDao.selectByPrimaryKey(mid);
		DocumentTypeVO documentTypevo = new DocumentTypeVO();
		BeanUtils.copyProperties(documentTypePO, documentTypevo);
		DocumentTypePO po = new DocumentTypePO();
		po.setIstime(documentTypeVO.getIstime());// 设置是否滚动
		po.setTime(documentTypeVO.getTime());// 设置滚动时间
		po.setDtid(documentTypevo.getDtid());
		po.setModelid(documentTypevo.getModelid());
		po.setTitle(documentTypevo.getTitle());
		po.setTransferurl(documentTypevo.getTransferurl());
		po.setLng(documentTypevo.getLng());
		po.setDescribe(documentTypevo.getDescribe());
		po.setHeight(documentTypevo.getHeight());
		po.setWidth(documentTypevo.getWidth());
		po.setMid(mid);
		return documentTypeDao.updateByPrimaryKey(po);

	}

}
