package com.linkonworks.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.linkonworks.dao.inf.ImgTransferPOMapper;
import com.linkonworks.po.ImgTransferPO;
import com.linkonworks.po.ImgTransferPOExample;
import com.linkonworks.service.inf.ImgTransferServiceInf;
import com.linkonworks.vo.ImgTransferVO;

@Service
public class ImgTransferService implements ImgTransferServiceInf {

	@Resource
	private ImgTransferPOMapper imgTransferDao;

	@Override
	public List<ImgTransferVO> selectByExample(ImgTransferVO imgTransferVO) {

		List<ImgTransferVO> rlist = new ArrayList<ImgTransferVO>();

		List<ImgTransferPO> list = new ArrayList<ImgTransferPO>();

		if (imgTransferVO == null) {
			list = imgTransferDao.selectByExample(null);
		} else {
			ImgTransferPOExample example = new ImgTransferPOExample();
			ImgTransferPOExample.Criteria criteria = example.createCriteria();
			example.setOrderByClause("SEQUENCE");

			String lng = imgTransferVO.getLng();
			if (!StringUtils.isBlank(lng)) {
				criteria.andLngEqualTo(lng);
			}
			
			String dtid = imgTransferVO.getDtid();
			if (!StringUtils.isBlank(lng)) {
				criteria.andDtidEqualTo(dtid);
			}
			list = imgTransferDao.selectByExample(example);
		}
		for (ImgTransferPO po : list) {
			ImgTransferVO vo = new ImgTransferVO();
			BeanUtils.copyProperties(po, vo);
			rlist.add(vo);
		}

		return rlist;
	}

}