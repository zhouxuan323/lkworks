package com.linkonworks.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Service;

import com.linkonworks.dao.inf.ManagerPoMapper;
import com.linkonworks.po.ManagerPo;
import com.linkonworks.po.ManagerPoExample;
import com.linkonworks.po.ManagerPoExample.Criteria;
import com.linkonworks.service.inf.LoginServiceInf;
import com.linkonworks.vo.ManagerVo;

@Service
public class LoginService implements LoginServiceInf{

	@Resource
	private ManagerPoMapper managerDao;

	@Override
	public ManagerVo logins(ManagerVo managerVo) {
		ManagerPoExample example = new ManagerPoExample();
		Criteria criteria = example.createCriteria();
		criteria.andMannameEqualTo(managerVo.getManname());
		criteria.andManpasswordEqualTo(managerVo.getManpassword());
		List<ManagerPo>  list = managerDao.selectByExample(example);
		ManagerPo managerPo = list.get(0);
		ManagerVo managerVo2 = new ManagerVo();
		try {
			BeanUtils.copyProperties(managerPo,managerVo2);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return managerVo2;
	}



	@Override
	public boolean login(String manname, String manpassword) {
		// TODO Auto-generated method stub
		return false;
	} 

}
