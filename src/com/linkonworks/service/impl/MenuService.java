package com.linkonworks.service.impl;

import java.awt.Menu;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;




import com.linkonworks.dao.inf.MenuPOMapper;
import com.linkonworks.po.MenuPO;
import com.linkonworks.po.MenuPOExample;
import com.linkonworks.service.inf.MenuServiceInf;
import com.linkonworks.utils.IDGenUtils;
import com.linkonworks.vo.MenuVO;
import com.linkonworks.vo.TreeVO;

@Service
public class MenuService implements MenuServiceInf {

	@Resource
	private MenuPOMapper menuDao;

	@Override
	public List<MenuVO> menuList(MenuVO menuVO) {
		// 新建一个对象
		List<MenuVO> rlist = new ArrayList<MenuVO>();
		// 查看一级菜单
		List<MenuVO> firList = selectByExample(menuVO);
		for (MenuVO fvo : firList) {
			MenuVO fList = new MenuVO();
			fList.setPmid(fvo.getMid());
			//fList.setIsdel(fvo.getIsdel());
			// 查看二级菜单
			List<MenuVO> secList = new ArrayList<MenuVO>();
			secList = selectByExample(fList);
			   for (MenuVO svo : secList) {
				   MenuVO sList = new MenuVO();//每一个二级菜单循环
				   sList.setPmid(svo.getMid()); //设置pmid
				   //sList.setIsdel(svo.getIsdel());
				   List<MenuVO> thiList = new ArrayList<MenuVO>();
				   thiList = selectByExample(sList);			   
				   svo.setMenuList(thiList);
//			       thiList.add(svo);
			      }
			fvo.setMenuList(secList);
			rlist.add(fvo);
		}
		
		return rlist;
	};

	@Override
	public List<MenuVO> selectByExample(MenuVO menuVO) {
		List<MenuVO> rlist = new ArrayList<MenuVO>();
		List<MenuPO> list = new ArrayList<MenuPO>();

		if (menuVO != null) {
			MenuPOExample example = new MenuPOExample();
			MenuPOExample.Criteria criteria = example.createCriteria();
			//判断是否启用菜单
           // Short isdel =menuVO.getIsdel();            
			//criteria.andIsdelEqualTo(isdel);
			
			String mid = menuVO.getMid();
			if (!StringUtils.isBlank(mid)) {
				criteria.andMidEqualTo(mid);
			}
			//判断pmid的值，为空时查询一级菜单，不为空时查询二三级菜单
			String pmid = menuVO.getPmid();
			if (pmid == null) {
				criteria.andPmidIsNull();
			} else if ("".equals(pmid)) {

			} else {
				criteria.andPmidEqualTo(pmid);
			}
           //判断语言
			String lng = menuVO.getLng();
			// 非空判断
			if (!StringUtils.isBlank(lng)) {
				criteria.andLngEqualTo(lng);
			}

			example.setOrderByClause("SEQUENCE");

			list = menuDao.selectByExample(example);

		} else {
			list = menuDao.selectByExample(null);
		}
    //循环输出每一级菜单的值
		for (MenuPO po : list) {
			MenuVO vo = new MenuVO();
			BeanUtils.copyProperties(po, vo);
			rlist.add(vo);
		}
		return rlist;
	}

	@Override
	public List<MenuVO> containedMenu(MenuVO menuVO) {
		List<MenuVO> rlist = new ArrayList<MenuVO>();

		List<MenuVO> list = selectByExample(menuVO);
		for (MenuVO vo : list) {
			MenuVO record = new MenuVO();
			record.setPmid(vo.getMid());
			record.setLng(menuVO.getLng());
			record.setIsdel(menuVO.getIsdel());
			vo.setMenuList(selectByExample(record));
			rlist.add(vo);

		}
		return rlist;
	}

	@Override
	public int deletemenu(TreeVO treeVO) {
		//String mid = menuVO.getMid();
		   //TreeVO treeVO = new TreeVO();
			//所有的id的值的集合
			String params = treeVO.getParams();
			String[] s = params.split("\\|");
			for (int i = s.length-1 ; i >= 0; i--) {				
				//获取mid的值
				String mid = s[i];
				//menuVO.setMid(s[i]);
				//System.out.println(i+"    "+s[i]);				
				menuDao.deleteByPrimaryKey(mid);
			}
		return 0;	
	}

	@Override
	public int updatemenu(TreeVO treeVO) {
		//MenuPO po = new MenuPO();
		String mid =treeVO.getId();		
		MenuPO menuPO = menuDao.selectByPrimaryKey(mid);
		  MenuVO menuVO = new MenuVO();
		  BeanUtils.copyProperties(menuPO, menuVO);
		  MenuPO po = new MenuPO();
		  po.setLng(menuVO.getLng());
		  po.setIsdel(treeVO.getIsdel());
		  po.setMenulevel(menuVO.getMenulevel());
		  po.setDescribe(treeVO.getDescribe());
		  po.setMenutype(menuVO.getMenutype());
		  po.setPmid(menuVO.getPmid());
		  po.setSequence(menuVO.getSequence());
		  po.setUrl(treeVO.getUrl());
		  po.setMid(menuVO.getMid());
		  po.setName(treeVO.getText());	
		return menuDao.updateByPrimaryKey(po);
	}

	@Override
	public int insertmenu(MenuVO menuVO) {
		//MenuPO menupo = new MenuPO();
		//通过tmid查询到数据
//		String pmid= menuVO.getPmid();
//		if(pmid != null){
//	
//		}
//		List<MenuPO> list= menuDao.selectByPmid(pmid);
		MenuVO param = new MenuVO();
		param.setLng(menuVO.getLng());
		param.setPmid(menuVO.getPmid());
		List<MenuVO> list= selectByExample(param);
		MenuPO menuPO = new MenuPO();
		if(list.size()>0){
		MenuVO vo =  list.get(list.size()-1);
		menuPO.setSequence(vo.getSequence()+1);
		}
		else{
			menuPO.setSequence(1L);
		}
		//遍历将vo的值传给po		
		menuPO.setMid(IDGenUtils.gen());
		menuPO.setLng(menuVO.getLng());
		menuPO.setDescribe(menuVO.getDescribe());
		menuPO.setIsdel(menuVO.getIsdel());
		menuPO.setMenulevel(menuVO.getMenulevel());
		menuPO.setMenutype(menuVO.getMenutype());
		menuPO.setName(menuVO.getName());
		//判断sequence是否为空，不为空时加1，为空时附一个值1
//		if(vo.getSequence()!=null){
//			menuPO.setSequence(vo.getSequence()+1);
//		}else{
//			menuPO.setSequence(1L);
//			}
		//menuPO.setUrl(menuVO.getUrl());
		menuPO.setUrl("/client.jsp?mid="+menuPO.getMid()+"&pmid="+menuVO.getPmid());
		menuPO.setPmid(menuVO.getPmid());
		   //menuDao.insert(menuPO);
		return menuDao.insert(menuPO);
	}
	@Override
	public List<MenuVO> selecturlList(MenuVO menuVO) {
		List<MenuVO> rlist = new ArrayList<MenuVO>();
		List<MenuPO> list = new ArrayList<MenuPO>();
		if (menuVO != null) {
			MenuPOExample example = new MenuPOExample();
			MenuPOExample.Criteria criteria = example.createCriteria();			
           //判断语言
			String lng = menuVO.getLng();
			// 非空判断
			if (!StringUtils.isBlank(lng)) {
				criteria.andLngEqualTo(lng);
			}
			list = menuDao.selectByExample(example);
    //循环输出每一级菜单的值
		for (MenuPO po : list) {
			MenuVO vo = new MenuVO();
			BeanUtils.copyProperties(po, vo);
			rlist.add(vo);
		}
	}
		return rlist;
}
	}