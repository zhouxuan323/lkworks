package com.linkonworks.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.linkonworks.dao.inf.DocTransferPOMapper;
import com.linkonworks.po.DocTransferPO;
import com.linkonworks.po.DocTransferPOExample;
import com.linkonworks.service.inf.DocTransferServiceInf;
import com.linkonworks.vo.DocTransferVO;

@Service
public class DocTransferService implements DocTransferServiceInf {

	@Resource
	private DocTransferPOMapper docTransferDao;

	@Override
	public List<DocTransferVO> selectByExample(DocTransferVO docTransferVO) {

		List<DocTransferVO> rlist = new ArrayList<DocTransferVO>();

		List<DocTransferPO> list = new ArrayList<DocTransferPO>();

		if (docTransferVO == null) {
			list = docTransferDao.selectByExample(null);
		} else {
			DocTransferPOExample example = new DocTransferPOExample();
			DocTransferPOExample.Criteria criteria = example.createCriteria();
			example.setOrderByClause("SEQUENCE");

			String lng = docTransferVO.getLng();
			if (!StringUtils.isBlank(lng)) {
				criteria.andLngEqualTo(lng);
			}
			String dtid = docTransferVO.getDtid();
			if (!StringUtils.isBlank(lng)) {
				criteria.andDtidEqualTo(dtid);
			}
			
			list = docTransferDao.selectByExample(example);
		}
		for (DocTransferPO po : list) {
			DocTransferVO vo = new DocTransferVO();
			BeanUtils.copyProperties(po, vo);
			rlist.add(vo);
		}

		// TODO Auto-generated method stub
		return rlist;
	}

}