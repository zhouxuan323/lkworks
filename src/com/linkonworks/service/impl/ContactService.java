package com.linkonworks.service.impl;

import com.linkonworks.dao.inf.ContactPOMapper;
import com.linkonworks.po.ContactPO;
import com.linkonworks.po.ContactPOExample;
import com.linkonworks.service.inf.ContactServiceInf;
import com.linkonworks.vo.ContactVO;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class ContactService implements ContactServiceInf{

	@Resource
	private ContactPOMapper contactDao;
	
	
	@Override
	public List<ContactVO> selectByExample(ContactVO contactVO) {
		List<ContactVO> rlist = new ArrayList<ContactVO>();

		List<ContactPO> list = new ArrayList<ContactPO>();

		if (contactVO == null) {
			list = contactDao.selectByExample(null);
		} else {
			ContactPOExample example = new ContactPOExample();
			ContactPOExample.Criteria criteria = example.createCriteria();
			example.setOrderByClause("type,SEQUENCE");

			String lng = contactVO.getLng();
			if (!StringUtils.isBlank(lng)) {
				criteria.andLngEqualTo(lng);
			}
			String dtid = contactVO.getDtid();
			if (!StringUtils.isBlank(lng)) {
				criteria.andDtidEqualTo(dtid);
			}
			
			list = contactDao.selectByExample(example);
		}
		for (ContactPO po : list) {
			ContactVO vo = new ContactVO();
			BeanUtils.copyProperties(po, vo);
			rlist.add(vo);
		}

		// TODO Auto-generated method stub
		return rlist;
	}
   
}