package com.linkonworks.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.linkonworks.dao.inf.AdvertPOMapper;
import com.linkonworks.po.AdvertPO;
import com.linkonworks.po.AdvertPOExample;
import com.linkonworks.service.inf.AdvertServiceInf;
import com.linkonworks.vo.AdvertVO;


@Service
public class AdvertService implements AdvertServiceInf{

	@Resource
	private AdvertPOMapper advertDao;
	
	@Override
	public List<AdvertVO> selectByExample(AdvertVO advertVO) {
		List<AdvertVO> rlist = new ArrayList<AdvertVO>();

		List<AdvertPO> list = new ArrayList<AdvertPO>();

		if (advertVO == null) {
			list = advertDao.selectByExample(null);
		} else {
			AdvertPOExample example = new AdvertPOExample();
			AdvertPOExample.Criteria criteria = example.createCriteria();
			example.setOrderByClause("SEQUENCE");

			String lng = advertVO.getLng();
			if (!StringUtils.isBlank(lng)) {
				criteria.andLngEqualTo(lng);
			}
			
			String dtid = advertVO.getDtid();
			if (!StringUtils.isBlank(lng)) {
				criteria.andDtidEqualTo(dtid);
			}
			list = advertDao.selectByExample(example);
		}
		for (AdvertPO po : list) {
			AdvertVO vo = new AdvertVO();
			BeanUtils.copyProperties(po, vo);
			rlist.add(vo);
		}

		return rlist;
	}
  
}