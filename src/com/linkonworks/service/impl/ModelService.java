package com.linkonworks.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.linkonworks.dao.inf.ModelPOMapper;
import com.linkonworks.po.ModelPO;
import com.linkonworks.service.inf.ModelServiceInf;
import com.linkonworks.vo.ModelVO;
/**
 * 模板的Service
 * @author Administrator
 *
 */
@Service
public class ModelService  implements ModelServiceInf{

	@Resource
	private ModelPOMapper modelDao;
	//查询模板
	@Override
	public List<ModelVO> selectByExample(ModelVO modelVO){
		//创建一个ModelVO的List集接收数据
		List<ModelVO> mdvList = new ArrayList<ModelVO>();
		//创建ModelPO的List集
		List<ModelPO> mdpList = new ArrayList<ModelPO>();
		if(modelVO == null){
			mdpList = modelDao.selectByExample(null);
		}
		//将po查询到的值传到vo
		for (ModelPO po : mdpList) {
			ModelVO vo = new ModelVO();
			BeanUtils.copyProperties(po, vo);
			mdvList.add(vo);
		}

		// TODO Auto-generated method stub
		return mdvList;
	}

	@Override
	public ModelVO selectByPrimaryKey(String modelid) {
		ModelPO po = modelDao.selectByPrimaryKey(modelid);
		ModelVO vo = new ModelVO();
		BeanUtils.copyProperties(po, vo);
		return vo;
	}

}
