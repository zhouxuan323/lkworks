package com.linkonworks.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.linkonworks.dao.inf.AlbumPOMapper;
import com.linkonworks.dao.inf.DocumentPOMapper;
import com.linkonworks.dao.inf.DocumentTypePOMapper;
import com.linkonworks.po.AlbumPO;
import com.linkonworks.po.AlbumPOExample;
import com.linkonworks.po.DocumentPO;
import com.linkonworks.po.DocumentPOExample;
import com.linkonworks.po.DocumentTypePO;
import com.linkonworks.po.DocumentTypePOExample;
import com.linkonworks.service.inf.DocumentServiceInf;
import com.linkonworks.vo.AlbumVO;
import com.linkonworks.vo.DocumentTypeVO;
import com.linkonworks.vo.DocumentVO;
import com.linkonworks.vo.MenuVO;

@Service
public class DocumentService implements DocumentServiceInf {
	@Resource
	public DocumentPOMapper documentDao;

	@Resource
	public DocumentTypePOMapper documentTypeDao;

	@Resource
	public AlbumPOMapper albumDao;

	@Override
	public int insert(DocumentVO documentVO) {
		DocumentPO dpo = new DocumentPO();
		BeanUtils.copyProperties(documentVO, dpo);
		documentDao.insertSelective(dpo);
		return 0;
	}

	@Override
	public int deleteDocument(String dtid) {
		// 调用内置的查询判断条件
		DocumentPOExample example = new DocumentPOExample();
		DocumentPOExample.Criteria criteria = example.createCriteria();
		// 根据dtid作为删除条件
		if (!StringUtils.isBlank(dtid)) {
			criteria.andDtidEqualTo(dtid);
		}
		// 调用dao层进行删除
		int num = documentDao.deleteByExample(example);

		return num;
	}

	@Override
	public List<DocumentVO> selectByExample(DocumentVO documentVo) {
		List<DocumentVO> rlist = new ArrayList<DocumentVO>();

		List<DocumentPO> list = new ArrayList<DocumentPO>();

		if (documentVo == null) {
			list = documentDao.selectByExample(null);
		} else {
			DocumentPOExample example = new DocumentPOExample();
			DocumentPOExample.Criteria criteria = example.createCriteria();
			example.setOrderByClause("SEQUENCE");

			String lng = documentVo.getLng();
			if (!StringUtils.isBlank(lng)) {
				criteria.andLngEqualTo(lng);
			}
			String dtid = documentVo.getDtid();
			if (!StringUtils.isBlank(lng)) {
				criteria.andDtidEqualTo(dtid);
			}
			
			list = documentDao.selectByExample(example);
		}
		for (DocumentPO po : list) {
			DocumentVO vo = new DocumentVO();
			BeanUtils.copyProperties(po, vo);
			rlist.add(vo);
		}

		// TODO Auto-generated method stub
		return rlist;
	}

	@Override
	public DocumentTypeVO findDocumentTypeByMid(MenuVO menuVO) {
		// 查询文档的类型
		List<DocumentTypePO> listpo = new ArrayList<DocumentTypePO>();
		List<DocumentTypeVO> listvo = new ArrayList<DocumentTypeVO>();
		if (menuVO != null) {
			DocumentTypePOExample example = new DocumentTypePOExample();
			DocumentTypePOExample.Criteria criteria = example.createCriteria();
			String mid = menuVO.getMid();
			if (!StringUtils.isBlank(mid)) {
				criteria.andMidEqualTo(mid);
			}

			listpo = documentTypeDao.selectByExample(example);
			for (DocumentTypePO po : listpo) {
				DocumentTypeVO vo = new DocumentTypeVO();
				vo.setDtid(po.getDtid());
				listvo.add(vo);
			}

		}

		DocumentTypeVO vo = listvo.get(0);

		return vo;
	}

	@Override
	public List<DocumentVO> selectDocumentByExample(DocumentVO documentVO) {
		// 查询文档的文字信息
		List<DocumentPO> listpo = new ArrayList<DocumentPO>();
		List<DocumentVO> listvo = new ArrayList<DocumentVO>();
		if (documentVO != null) {
			DocumentPOExample example = new DocumentPOExample();
			DocumentPOExample.Criteria criteria = example.createCriteria();

			String dtid = documentVO.getDtid();
			if (!StringUtils.isBlank(dtid)) {
				criteria.andDtidEqualTo(dtid);
			}

			listpo = documentDao.selectByExample(example);
			for (DocumentPO po : listpo) {
				DocumentVO vo = new DocumentVO();
				BeanUtils.copyProperties(po, vo);
				listvo.add(vo);
			}

		}
		return listvo;
	}

	@Override
	public List<AlbumVO> selectDocumentAlbumByExample(AlbumVO albumVO) {
		// 查询文档的图片信息
		List<AlbumPO> listpo = new ArrayList<AlbumPO>();
		List<AlbumVO> listvo = new ArrayList<AlbumVO>();
		if (albumVO != null) {
			AlbumPOExample example = new AlbumPOExample();
			AlbumPOExample.Criteria criteria = example.createCriteria();

			String dtid = albumVO.getDtid();
			if (!StringUtils.isBlank(dtid)) {
				criteria.andDtidEqualTo(dtid);
			}
			listpo = albumDao.selectByExample(example);
			for (AlbumPO po : listpo) {
				AlbumVO vo = new AlbumVO();
				BeanUtils.copyProperties(po, vo);
				listvo.add(vo);
			}
		}
		return listvo;
	}

	@Override
	public void deleteDocumentDaidstr(String daidstr) {
		String[] strings=daidstr.split("\\|");
		for(String daid:strings){
			if(!StringUtils.isBlank(daid)){
				documentDao.deleteByPrimaryKey(daid);	
			}
		}
		
		
	}

}