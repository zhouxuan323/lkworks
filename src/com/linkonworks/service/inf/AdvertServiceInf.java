package com.linkonworks.service.inf;

import java.util.List;

import com.linkonworks.vo.AdvertVO;



public interface AdvertServiceInf {
	/**
	 * 按条件查询
	 * @param advertVO
	 * @return
	 */
	List<AdvertVO> selectByExample(AdvertVO advertVO); 
    
}