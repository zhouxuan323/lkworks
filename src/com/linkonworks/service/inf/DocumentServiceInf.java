package com.linkonworks.service.inf;

import java.util.List;

import com.linkonworks.vo.AlbumVO;
import com.linkonworks.vo.DocumentTypeVO;
import com.linkonworks.vo.DocumentVO;
import com.linkonworks.vo.FileVO;
import com.linkonworks.vo.MenuVO;



public interface DocumentServiceInf{
    /**
    * 插入内容
    */
	int insert(DocumentVO documentVo);
	/**
	 * 查询文章
	 */
	List<DocumentVO> selectByExample(DocumentVO documentVo);
	
	/**
	 * 删除内容
	*/
	int deleteDocument(String dtid);
	
	
	
	/**
	 * 查询文档类型
	 */
	DocumentTypeVO findDocumentTypeByMid(MenuVO menuVO);
	/**
	 * 查询内容信息
	 */
	List<DocumentVO> selectDocumentByExample(DocumentVO VO);
	
	/**
	 * 查询图片信息
	 */
	List<AlbumVO> selectDocumentAlbumByExample(AlbumVO albumVO);
	
	
	/**
	 * 删除内容
	*/
	void deleteDocumentDaidstr(String daidstr);
	
}