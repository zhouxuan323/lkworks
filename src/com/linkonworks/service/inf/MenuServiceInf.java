package com.linkonworks.service.inf;

import java.util.List;

import com.linkonworks.vo.MenuVO;
import com.linkonworks.vo.TreeVO;

public interface MenuServiceInf {
	/**
	 * 三级菜单的查询操作
	 * 
	 * @param menuVO
	 * @return
	 */
	List<MenuVO> menuList(MenuVO menuVO);

	/**
	 * 根据条件查询
	 * 
	 * @param menuVO
	 * @return
	 */
	List<MenuVO> selectByExample(MenuVO menuVO);

	/**
	 * 返回包含二级菜单的菜单集合
	 * @param menuvo
	 * @return
	 */
	List<MenuVO> containedMenu(MenuVO menuvo);
/**
 * 删除菜单
 * @param menuVO
 * @return
 */
	int deletemenu(TreeVO treeVO);
	
	
	/**
	 * 更改菜单
	 * @param treeVO
	 * @return
	 */
   int updatemenu(TreeVO treeVO);
   
/**
 * 新增菜单
 * @param menuVO
 * @return
 */
  int insertmenu(MenuVO menuVO);
/**
 * 下拉列表查询菜单时用
 * @param menuVO
 * @return
 */
List<MenuVO> selecturlList(MenuVO menuVO);
	
}
