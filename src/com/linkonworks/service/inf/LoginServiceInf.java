package com.linkonworks.service.inf;

import com.linkonworks.vo.ManagerVo;

public interface LoginServiceInf {

	/**
	 * 登录查询方法
	 * @param manname
	 * @param manpassword
	 * @return
	 */
	boolean login(String manname,String manpassword);
	
	ManagerVo logins(ManagerVo managerVo);
}
