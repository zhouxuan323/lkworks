package com.linkonworks.service.inf;

import java.util.List;

import com.linkonworks.vo.ModelVO;

public interface ModelServiceInf {
	/**
	 * 查询模板图片
	 * 
	 * @param modelVO
	 * @return
	 */
	List<ModelVO> selectByExample(ModelVO modelVO);

	/**
	 * 通过主键查询
	 * @param modelid
	 * @return
	 */
	ModelVO selectByPrimaryKey(String modelid);

	// int insert(ModelVO modelVO);
}
