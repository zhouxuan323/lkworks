package com.linkonworks.service.inf;

import java.io.File;
import java.util.List;

import com.linkonworks.vo.DocumentTypeVO;
import com.linkonworks.vo.DocumentVO;
import com.linkonworks.vo.FileVO;
import com.linkonworks.vo.MenuVO;
import com.linkonworks.vo.ModelVO;



public interface DocumentTypeServiceInf {
	/**
	 * 按条件查询
	 * @param documentTypeVO
	 * @return
	 */
	List<DocumentTypeVO> selectByExample(DocumentTypeVO documentTypeVO); 
   
	/**
    * 插入类型
    * @param documrntTypevo
    * @return
    */
	int insert(DocumentTypeVO documrntTypevo);
	
	
	/**
	 * 保存文档和图片
	 * @param menuVO
	 */
	void save(MenuVO menuVO,String adidstr,List<DocumentVO> documentList,List<File> file);
	
	/**
	 * 通过模型插类型
	 * @param menuVO
	 * @param modelVO
	 */
	void insert(MenuVO menuVO,ModelVO modelVO);
	
	/**
	 * 保存文档
	 * @param menuVO
	 */
	void saveDocument(MenuVO menuVO,String adidstr,List<DocumentVO> documentList);
	
	/**
	 * 保存图片
	 * @param menuVO
	 */
	void saveFile(MenuVO menuVO,String adidstr,List<File> file);
	
	/**
	 * 删除内容
	*/
	int deleteDocumentDaidstr(String daidstr);
	
	 /**
	  * 插入内容
	 */
	int saveFileVOList(MenuVO menuVo,String daidstr,List<FileVO> fileVOList);
	
	 /**
	  * 插入联系我们内容
	 */
	int saveLxwmList(MenuVO menuVo,List<DocumentVO> documentList);
	
}