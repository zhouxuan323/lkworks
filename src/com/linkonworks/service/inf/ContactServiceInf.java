package com.linkonworks.service.inf;

import com.linkonworks.vo.ContactVO;

import java.util.List;


public interface ContactServiceInf {

    List<ContactVO> selectByExample(ContactVO contactVO);
}

