package com.linkonworks.service.inf;

import java.util.List;

import com.linkonworks.vo.ImgTransferVO;



public interface ImgTransferServiceInf {
	/**
	 * 
	 * @param imgTransferVO
	 * @return
	 */
	List<ImgTransferVO> selectByExample(ImgTransferVO imgTransferVO); 
}