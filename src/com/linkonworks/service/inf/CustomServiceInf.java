package com.linkonworks.service.inf;

import com.linkonworks.vo.CustomVO;

import java.util.List;


public interface CustomServiceInf {


	/**
	 * 按条件查询
	 * @param customVO
	 * @return
	 */
    List<CustomVO> selectByExample(CustomVO customVO);



}