package com.linkonworks.service.inf;

import java.io.File;
import java.util.List;

import com.linkonworks.po.MenuPO;
import com.linkonworks.vo.AdvertVO;
import com.linkonworks.vo.DocTransferVO;
import com.linkonworks.vo.DocumentTypeVO;
import com.linkonworks.vo.ImgTransferVO;
import com.linkonworks.vo.MenuVO;
import com.linkonworks.vo.TreeVO;

public interface HomeServiceInf {
/**
 * 图片轮换表的查询操作
 * @param advertVO
 * @return
 */
	List<AdvertVO> selectadvert(AdvertVO advertVO);
/**
 * 文字快捷链接的查询操作
 * @param docTransferVO
 * @return
 */
List<DocTransferVO> selectdoctransfer(DocTransferVO docTransferVO);

/**
 * 图片快捷链接的查询操作
 * @param imgTransferVO
 * @return
 */
List<ImgTransferVO> selectimgtransfer(ImgTransferVO imgTransferVO);

/**
 * 图片轮换保存图片
 * @param menuVO 
 * @param newmenuList
 * @param menuList 
 * @param advertList 
 * @param advertVO 
 * @param file
 */
void saveadvert(MenuVO menuVO, List<MenuVO> newmenuList,List<MenuVO> menuList, List<AdvertVO> advertList, List<File> file);
/**
 * 查询类型表得到dtid
 */
List<DocumentTypeVO> selectByExample(DocumentTypeVO documentTypeVO);
/**
 * 文字连接保存文字
 * @param documentTypeVO
 * @param menuVO 
 * @param docTransferList
 */
void savedoctransfer(DocumentTypeVO documentTypeVO, MenuVO menuVO, List<DocTransferVO> docTransferList);
/**
 * doc 插入图片
 * @param documentVO
 */
void insert(DocTransferVO documentVO);
/**
 * 图片链接保存图片
 * @param menuVO
 * @param imgTransferList 
 * @param menuList 
 * @param menuList 
 * @param file
 */
void saveimgtransfer(MenuVO menuVO, List<MenuVO> newmenuList, List<MenuVO> menuList, List<ImgTransferVO> imgTransferList, List<File> file);
/**
 * 通过页面上的删除按钮获取id  删除图片
 * @param adidstr
 */
void deletebyid(String adidstr);
/**
 * 图片链接界面删除图片获取itid的值
 * @param adidstr
 */
void deleteimg(String adidstr);
/**
 * 文字链接界面删除文字获取id的值
 * @param dtid
 */
void deletedoc(String dtid);
/**
 * 保存滚动设置的信息
 * @param documentTypeVO
 * @return
 */
int savetime(DocumentTypeVO documentTypeVO);




}
