package com.linkonworks.service.inf;

import java.util.List;

import com.linkonworks.vo.DocTransferVO;



public interface DocTransferServiceInf {
	/**
	 * 按条件查询
	 * @param docTransferVO
	 * @return
	 */
	List<DocTransferVO> selectByExample(DocTransferVO docTransferVO); 
}