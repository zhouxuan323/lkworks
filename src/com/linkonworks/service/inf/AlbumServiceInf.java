package com.linkonworks.service.inf;

import com.linkonworks.vo.AlbumVO;
import java.util.List;

public interface AlbumServiceInf {

	/**
	 * 查询图片
	 * @param albumVO
	 * @return
	 */
	List<AlbumVO> selectByExample(AlbumVO albumVO);
   
	/**
	* 插入图片
	* @param albumVO
	* @return
	*/
	int insert(AlbumVO albumVO);
	
	/**
    * 插入多张图片
    * @param albumVO
    * @return
    */
	int insertAlbumVOList(List<String> fileNameList);
	
	/**
	 * 删除图片
	 */
	int delteAlbum(String dtid);
	
	/**
	 * 删除图片
	 */
	int delteAlbumstr(String adidstr);
}