package com.linkonworks.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.linkonworks.service.inf.ModelServiceInf;


import com.linkonworks.vo.ModelVO;

@Namespace("\\*")
@ParentPackage("json-default")
public class SelectmodelAction {

	private static final long serialVersionUID = 1L;
	//模板
	@Resource
	public ModelServiceInf modelService;

	//返回模板参数
	private List<ModelVO> modelist;
	
	//接收模板参数
	private ModelVO modelvo;
	
	@Action(value = "/selectmodelAction",results = {@Result(name = "success", type = "json")})
	public String selectmodelAction(){
		
		try{
			//ModelVO modelvo = new ModelVO();
			modelist = modelService.selectByExample(modelvo);
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return "success";
	}
	//模板的set和get方法

	public ModelVO getModelvo() {
		return modelvo;
	}

	public void setModelvo(ModelVO modelvo) {
		this.modelvo = modelvo;
	}

	public List<ModelVO> getModelist() {
		return modelist;
	}
	
}
