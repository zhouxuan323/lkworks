package com.linkonworks.action;

import java.io.File;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.linkonworks.service.inf.AlbumServiceInf;
import com.linkonworks.utils.ApplicationConstant;
import com.linkonworks.utils.UploadUtil;
import com.opensymphony.xwork2.ActionSupport;

@Namespace("\\*")
@ParentPackage("json-default")
public class UploadAction extends ActionSupport {
	@Resource
	private AlbumServiceInf albumService;
	private List<File> file;

	private List<String> fileFileName;

	private List<String> fileContentType;

	private List<String> dataUrl;
	
	@Action(value = "/fileUploadAction", results = { @Result(name = "success", type = "json") })
	public String shangChuan(){
		try {
			//调用上传方法，得到文件的名字
			List<String> fileNameList=UploadUtil.upload(file);
			
			albumService.insertAlbumVOList(fileNameList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return "success";
	}

	
	public List<File> getFile() {
		return file;
	}

	public void setFile(List<File> file) {
		this.file = file;
	}

	public List<String> getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(List<String> fileFileName) {
		this.fileFileName = fileFileName;
	}

	public List<String> getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(List<String> fileContentType) {
		this.fileContentType = fileContentType;
	}

	public List<String> getDataUrl() {
		return dataUrl;
	}

	public void setDataUrl(List<String> dataUrl) {
		this.dataUrl = dataUrl;
	}

}
