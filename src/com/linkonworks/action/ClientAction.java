package com.linkonworks.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.linkonworks.service.inf.AdvertServiceInf;
import com.linkonworks.service.inf.AlbumServiceInf;
import com.linkonworks.service.inf.ContactServiceInf;
import com.linkonworks.service.inf.CustomServiceInf;
import com.linkonworks.service.inf.DocTransferServiceInf;
import com.linkonworks.service.inf.DocumentServiceInf;
import com.linkonworks.service.inf.DocumentTypeServiceInf;
import com.linkonworks.service.inf.ImgTransferServiceInf;
import com.linkonworks.service.inf.MenuServiceInf;
import com.linkonworks.service.inf.ModelServiceInf;
import com.linkonworks.utils.ApplicationConstant;
import com.linkonworks.vo.AdvertVO;
import com.linkonworks.vo.AlbumVO;
import com.linkonworks.vo.ContactVO;
import com.linkonworks.vo.CustomVO;
import com.linkonworks.vo.DocTransferVO;
import com.linkonworks.vo.DocumentTypeVO;
import com.linkonworks.vo.DocumentVO;
import com.linkonworks.vo.ImgTransferVO;
import com.linkonworks.vo.MenuVO;
import com.linkonworks.vo.ModelVO;
import com.opensymphony.xwork2.ActionSupport;

@Namespace("\\*")
@ParentPackage("json-default")
public class ClientAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	@Resource
	private MenuServiceInf menuService;
	@Resource
	private DocumentTypeServiceInf documentTypeService;
	@Resource
	private ImgTransferServiceInf imgTransferService;
	@Resource
	private DocTransferServiceInf docTransferService;
	@Resource
	private AdvertServiceInf advertService;
	@Resource
	private AlbumServiceInf albumService;
	@Resource
	private DocumentServiceInf documentService;
	@Resource
	private ContactServiceInf contactService;
	@Resource
	private CustomServiceInf customService;
	@Resource
	private ModelServiceInf modelService;
	// ==返回值
	private List<MenuVO> menuList;// 菜单项
	private List<ImgTransferVO> imgTransferList;// 图片快捷链接
	private List<DocTransferVO> docTransferList;// 文字快捷链接
	private List<AdvertVO> advertList;// 首页轮换图片
	private List<DocumentVO> documentList;// 文档
	private List<AlbumVO> albumList;// 相册
	private List<ContactVO> contactList;// 联系方式
	private List<CustomVO> customList;// 自定义

	// ==接收参数（当前同用于返回）
	private MenuVO menuVO;
	private DocumentTypeVO documentTypeVO;
	@Action(value = "/loadModelAction", results = { @Result(name = "success", type = "json") })
	public String loadModelAction(){
		try {
			DocumentTypeVO param = new DocumentTypeVO();
			param.setMid(menuVO.getMid());
			param.setLng(menuVO.getLng());
			List<DocumentTypeVO> list = documentTypeService.selectByExample(param);
			if (list != null && list.size() == 1) {
				DocumentTypeVO documentTypeVO = list.get(0);
				this.documentTypeVO = documentTypeVO;
				ModelVO modelVO = modelService.selectByPrimaryKey(list.get(0).getModelid());
				this.documentTypeVO.setPmodelid(modelVO.getPmodelid());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	@Action(value = "/loadModelHomeAction", results = { @Result(name = "success", type = "json") })
	public String loadModelHomeAction(){
		try {
			DocumentTypeVO param = new DocumentTypeVO();
			param.setMid(menuVO.getMid());
			param.setLng(menuVO.getLng());
			List<DocumentTypeVO> list = documentTypeService.selectByExample(param);
			if (list != null && list.size() == 1) {
				DocumentTypeVO documentTypeVO = list.get(0);
				this.documentTypeVO = documentTypeVO;
				ImgTransferVO imgTransferVO = new ImgTransferVO();
				imgTransferVO.setLng(param.getLng());
				imgTransferVO.setDtid(documentTypeVO.getDtid());
				imgTransferList = imgTransferService.selectByExample(imgTransferVO);
	
				DocTransferVO docTransferVO = new DocTransferVO();
				docTransferVO.setLng(param.getLng());
				docTransferVO.setDtid(documentTypeVO.getDtid());
				docTransferList = docTransferService.selectByExample(docTransferVO);
	
				AdvertVO advertVO = new AdvertVO();
				advertVO.setLng(param.getLng());
				advertVO.setDtid(documentTypeVO.getDtid());
				advertList = advertService.selectByExample(advertVO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	@Action(value = "/loadModelContentAction", results = { @Result(name = "success", type = "json") })
	public String loadModelContentAction(){
		try {
				DocumentTypeVO param = new DocumentTypeVO();
				param.setMid(menuVO.getMid());
				param.setLng(menuVO.getLng());
				List<DocumentTypeVO> list = documentTypeService.selectByExample(param);
				if (list != null && list.size() == 1) {
					DocumentTypeVO documentTypeVO = list.get(0);
					this.documentTypeVO = documentTypeVO;
					ModelVO modelVO = modelService.selectByPrimaryKey(list.get(0).getModelid());
					this.documentTypeVO.setPmodelid(modelVO.getPmodelid());
					if (ApplicationConstant.MODEL_01.equals(documentTypeVO.getModelid())) {
						
					} else {
						MenuVO menuVO = new MenuVO();
						menuVO.setLng(this.menuVO.getLng());
						menuVO.setIsdel(this.menuVO.getIsdel());
						if (StringUtils.isBlank(this.menuVO.getPmid()) || "null".equals(this.menuVO.getPmid())) {
							menuVO.setPmid(this.menuVO.getMid());
						} else {
							menuVO.setPmid(this.menuVO.getPmid());
						}
						menuList = menuService.containedMenu(menuVO);
						

						AlbumVO albumVO = new AlbumVO();
						albumVO.setLng(param.getLng());
						albumVO.setDtid(documentTypeVO.getDtid());
						albumList = albumService.selectByExample(albumVO);
						
						DocumentVO documentVO = new DocumentVO();
						documentVO.setLng(param.getLng());
						documentVO.setDtid(documentTypeVO.getDtid());
						documentList = documentService.selectByExample(documentVO);
						
						ContactVO contactVO = new ContactVO();
						contactVO.setLng(param.getLng());
						contactVO.setDtid(documentTypeVO.getDtid());
						contactList = contactService.selectByExample(contactVO);
						
						CustomVO customVO = new CustomVO();
						customVO.setLng(param.getLng());
						customVO.setDtid(documentTypeVO.getDtid());
						customList = customService.selectByExample(customVO);
					}
				}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	
	
	

	@Action(value = "/clientDocTypeAction", results = { @Result(name = "success", type = "json") })
	public String clientDocTypeAction() {
		try {
			DocumentTypeVO param = new DocumentTypeVO();
			param.setMid(menuVO.getMid());
			param.setLng(menuVO.getLng());
			List<DocumentTypeVO> list = documentTypeService.selectByExample(param);
			if (list != null && list.size() == 1) {
				DocumentTypeVO documentTypeVO = list.get(0);
				this.documentTypeVO = documentTypeVO;
				ModelVO modelVO = modelService.selectByPrimaryKey(list.get(0).getModelid());
				this.documentTypeVO.setPmodelid(modelVO.getPmodelid());
				if (ApplicationConstant.MODEL_01.equals(documentTypeVO.getModelid())) {
					ImgTransferVO imgTransferVO = new ImgTransferVO();
					imgTransferVO.setLng(param.getLng());
					imgTransferVO.setDtid(documentTypeVO.getDtid());
					imgTransferList = imgTransferService.selectByExample(imgTransferVO);

					DocTransferVO docTransferVO = new DocTransferVO();
					docTransferVO.setLng(param.getLng());
					docTransferVO.setDtid(documentTypeVO.getDtid());
					docTransferList = docTransferService.selectByExample(docTransferVO);

					AdvertVO advertVO = new AdvertVO();
					advertVO.setLng(param.getLng());
					advertVO.setDtid(documentTypeVO.getDtid());
					advertList = advertService.selectByExample(advertVO);
				} else {
					MenuVO menuVO = new MenuVO();
					menuVO.setLng(this.menuVO.getLng());
					menuVO.setIsdel(this.menuVO.getIsdel());
					if (StringUtils.isBlank(this.menuVO.getPmid()) || "null".equals(this.menuVO.getPmid())) {
						menuVO.setPmid(this.menuVO.getMid());
					} else {
						menuVO.setPmid(this.menuVO.getPmid());
					}
					menuList = menuService.containedMenu(menuVO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(value = "/clientContentTypeAction", results = { @Result(name = "success", type = "json") })
	public String clientContentTypeAction() {
		try {
			DocumentTypeVO param = new DocumentTypeVO();
			param.setMid(menuVO.getMid());
			param.setLng(menuVO.getLng());
			List<DocumentTypeVO> list = documentTypeService.selectByExample(param);
			if (list != null && list.size() == 1) {
				DocumentTypeVO documentTypeVO = list.get(0);
				this.documentTypeVO = documentTypeVO;
				
				AlbumVO albumVO = new AlbumVO();
				albumVO.setLng(param.getLng());
				albumVO.setDtid(documentTypeVO.getDtid());
				albumList = albumService.selectByExample(albumVO);
				
				DocumentVO documentVO = new DocumentVO();
				documentVO.setLng(param.getLng());
				documentVO.setDtid(documentTypeVO.getDtid());
				documentList = documentService.selectByExample(documentVO);
				
				ContactVO contactVO = new ContactVO();
				contactVO.setLng(param.getLng());
				contactVO.setDtid(documentTypeVO.getDtid());
				contactList = contactService.selectByExample(contactVO);
				
				CustomVO customVO = new CustomVO();
				customVO.setLng(param.getLng());
				customVO.setDtid(documentTypeVO.getDtid());
				customList = customService.selectByExample(customVO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	
	//
	/**
	 * 用于擦查询包含二级子菜单的菜单集合
	 */
	@Action(value = "/containedMenuAction", results = { @Result(name = "success", type = "json") })
	public String containedMenuAction() {
		try {
			// 获取参数
			MenuVO menuVO = new MenuVO();
			menuVO.setLng(this.menuVO.getLng());
			menuVO.setIsdel(this.menuVO.getIsdel());
			menuVO.setPmid(this.menuVO.getPmid());
			// 获取查询结果
			menuList = menuService.containedMenu(menuVO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	

	public List<MenuVO> getMenuList() {
		return menuList;
	}

	public void setMenuVO(MenuVO menuVO) {
		this.menuVO = menuVO;
	}

	public MenuVO getMenuVO() {
		return menuVO;
	}

	public List<ImgTransferVO> getImgTransferList() {
		return imgTransferList;
	}

	public List<DocTransferVO> getDocTransferList() {
		return docTransferList;
	}

	public List<AdvertVO> getAdvertList() {
		return advertList;
	}

	public DocumentTypeVO getDocumentTypeVO() {
		return documentTypeVO;
	}

	public void setDocumentTypeVO(DocumentTypeVO documentTypeVO) {
		this.documentTypeVO = documentTypeVO;
	}

	public List<DocumentVO> getDocumentList() {
		return documentList;
	}

	public List<AlbumVO> getAlbumList() {
		return albumList;
	}

	public List<ContactVO> getContactList() {
		return contactList;
	}

	public List<CustomVO> getCustomList() {
		return customList;
	}

}
