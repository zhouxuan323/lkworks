package com.linkonworks.action;

import java.io.File;
import java.util.List;

import javax.annotation.Resource;

import org.apache.catalina.connector.Request;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;





import com.linkonworks.service.inf.HomeServiceInf;
import com.linkonworks.vo.AdvertVO;
import com.linkonworks.vo.DocTransferVO;
import com.linkonworks.vo.DocumentTypeVO;
import com.linkonworks.vo.ImgTransferVO;
import com.linkonworks.vo.MenuVO;
import com.opensymphony.xwork2.ActionSupport;

@Namespace("\\*")
@ParentPackage("json-default")
public class HomeAction extends ActionSupport{
	private static final long serialVersionUID = 1L;
	@Resource
	private HomeServiceInf homeService;
	private List<AdvertVO> advertList;
	private List<DocTransferVO> docTransferList;
	private List<ImgTransferVO> imgTransferList;
	private DocumentTypeVO   documentTypeVO;
	private MenuVO menuVO;
	//用于接收下拉列表中的name值
	private List<MenuVO> menuList;
	//用于接收新增的图片的name值
	private List<MenuVO> newmenuList;
	private AdvertVO advertVO;
	private DocTransferVO docTransferVO;
	private ImgTransferVO imgTransferVO;
	//接收 返回值
	private int result;
	//图片接收参数(接收上传文件)
	private List<File> file;
	private List<String> fileFileName;
	private List<String> fileType;
	private List<String> datatUrl;
	
	//接收删除图片的id
	private String adidstr;
	@Action(value = "/selectAdvertAction", results = { @Result(name = "success", type = "json") })
	public String selectAdvertAction() {
		try {
			advertList = homeService.selectadvert(advertVO);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";		
	}
	
	@Action(value = "/saveAdvert", results = { @Result(name = "success", type = "redirectAction") })
	public String saveAdvertAction() {
		try {
			//如果存在页面删除图片
			if(!StringUtils.isBlank(adidstr)){
				homeService.deletebyid(adidstr);
				 homeService.saveadvert(menuVO,newmenuList,menuList,advertList,file);				
		}else{
			  homeService.saveadvert(menuVO,newmenuList,menuList,advertList,file);
		}
		 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";		
	}
	@Action(value = "/savetime", results = { @Result(name = "success", type = "json") })
	public String savetimeAction() {
		try {			
			result = homeService.savetime(documentTypeVO);		 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";		
	}
	
	
	@Action(value = "/selectDoctransfer", results = { @Result(name = "success", type = "json") })
	public String selectDoctransferAction() {
		try {
			docTransferList = homeService.selectdoctransfer(docTransferVO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";		
	}
	@Action(value = "/saveDoctransfer", results = { @Result(name = "success", type = "redirectAction") })
	public String insertDoctransferAction() {
		try {
    //通过先删除数据库中所有的，再保存页面上显示的			
			homeService.savedoctransfer(documentTypeVO, menuVO,docTransferList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";		
	}

	@Action(value = "/selectImgTransfer", results = { @Result(name = "success", type = "json") })
	public String selectImgtransferAction() {
		try {
			imgTransferList = homeService.selectimgtransfer(imgTransferVO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";		
	}
	@Action(value = "/saveImgtransfer", results = { @Result(name = "success", type = "redirectAction") })
	public String insertImgtransferAction() {
		try {
			//如果存在页面删除图片
			if(!StringUtils.isBlank(adidstr)){
				 homeService.deleteimg(adidstr);
				 homeService.saveimgtransfer(menuVO,newmenuList,menuList,imgTransferList,file);				
		}else{
			  homeService.saveimgtransfer(menuVO,newmenuList,menuList,imgTransferList,file);
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "success";		
	}
	
	
	
	public List<AdvertVO> getAdvertList() {
		return advertList;
	}

	public void setAdvertList(List<AdvertVO> advertList) {
		this.advertList = advertList;
	}
	public AdvertVO getAdvertVO() {
		return advertVO;
	}
	public void setAdvertVO(AdvertVO advertVO) {
		this.advertVO = advertVO;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public List<DocTransferVO> getDocTransferList() {
		return docTransferList;
	}
	public void setDocTransferList(List<DocTransferVO> docTransferList) {
		this.docTransferList = docTransferList;
	}
	public List<ImgTransferVO> getImgTransferList() {
		return imgTransferList;
	}
	public void setImgTransferList(List<ImgTransferVO> imgTransferList) {
		this.imgTransferList = imgTransferList;
	}
	
	
	public DocumentTypeVO getDocumentTypeVO() {
		return documentTypeVO;
	}

	public void setDocumentTypeVO(DocumentTypeVO documentTypeVO) {
		this.documentTypeVO = documentTypeVO;
	}

	public MenuVO getMenuVO() {
		return menuVO;
	}
	public void setMenuVO(MenuVO menuVO) {
		this.menuVO = menuVO;
	}
	
	
	
	public List<MenuVO> getNewmenuList() {
		return newmenuList;
	}

	public void setNewmenuList(List<MenuVO> newmenuList) {
		this.newmenuList = newmenuList;
	}

	public List<MenuVO> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<MenuVO> menuList) {
		this.menuList = menuList;
	}

	public DocTransferVO getDocTransferVO() {
		return docTransferVO;
	}
	public void setDocTransferVO(DocTransferVO docTransferVO) {
		this.docTransferVO = docTransferVO;
	}
	public ImgTransferVO getImgTransferVO() {
		return imgTransferVO;
	}
	public void setImgTransferVO(ImgTransferVO imgTransferVO) {
		this.imgTransferVO = imgTransferVO;
	}
	public List<File> getFile() {
		return file;
	}
	public void setFile(List<File> file) {
		this.file = file;
	}
	public List<String> getFileFileName() {
		return fileFileName;
	}
	public void setFileFileName(List<String> fileFileName) {
		this.fileFileName = fileFileName;
	}
	public List<String> getFileType() {
		return fileType;
	}
	public void setFileType(List<String> fileType) {
		this.fileType = fileType;
	}
	public List<String> getDatatUrl() {
		return datatUrl;
	}
	public void setDatatUrl(List<String> datatUrl) {
		this.datatUrl = datatUrl;
	}

	public String getAdidstr() {
		return adidstr;
	}

	public void setAdidstr(String adidstr) {
		this.adidstr = adidstr;
	}

	
	

	
	
}
