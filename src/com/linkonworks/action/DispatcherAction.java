package com.linkonworks.action;

import java.util.List;

import javax.annotation.Resource;

import com.linkonworks.service.inf.DocumentTypeServiceInf;
import com.linkonworks.service.inf.ModelServiceInf;
import com.linkonworks.utils.IDGenUtils;
import com.linkonworks.vo.DocumentTypeVO;
import com.linkonworks.vo.MenuVO;
import com.linkonworks.vo.ModelVO;
import com.opensymphony.xwork2.ActionSupport;

public class DispatcherAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	@Resource
	private DocumentTypeServiceInf documentTypeService;
	@Resource
	private ModelServiceInf modelService;
	// ==接收参数
	private MenuVO menuVO;

	private DocumentTypeVO documentTypeVO;

	private String result;

	private String source;

	private String dispatcherURL;

	@Override
	public String execute() throws Exception {
		System.out.println(source);
		System.err.println(menuVO.getMid());
		if (menuVO == null) {
			result = "没有获取到编辑的菜单信息！";
			return SUCCESS;
		}
		if("0".equals(source)){
			
		}else{
			documentTypeVO.setDtid(IDGenUtils.gen());
			documentTypeService.insert(documentTypeVO);
		}
		DocumentTypeVO param = new DocumentTypeVO();
		param.setMid(menuVO.getMid());
		List<DocumentTypeVO> list = documentTypeService.selectByExample(param);
		if (list != null && list.size() > 0) {
			ModelVO modelVO = modelService.selectByPrimaryKey(list.get(0).getModelid());
			dispatcherURL = modelVO.getDispatcher();
			if(dispatcherURL == null){
				
			}else{
				dispatcherURL = dispatcherURL.trim();
			}
			
		} else {
			dispatcherURL = "management/model.jsp";
		}

		//dispatcherURL = "management/muban.jsp";

		return SUCCESS;

	}
	public DocumentTypeVO getDocumentTypeVO() {
		return documentTypeVO;
	}

	public void setDocumentTypeVO(DocumentTypeVO documentTypeVO) {
		this.documentTypeVO = documentTypeVO;
	}

	public void setMenuVO(MenuVO menuVO) {
		this.menuVO = menuVO;
	}

	public MenuVO getMenuVO() {
		return menuVO;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getResult() {
		return result;
	}

	public String getSource() {
		return source;
	}

	public String getDispatcherURL() {
		return dispatcherURL;
	}

	public void setDispatcherURL(String dispatcherURL) {
		this.dispatcherURL = dispatcherURL;
	}

}