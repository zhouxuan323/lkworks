package com.linkonworks.action;


import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.linkonworks.service.inf.LoginServiceInf;
import com.linkonworks.vo.ManagerVo;
import com.opensymphony.xwork2.ActionSupport;

@Namespace("\\*")
@ParentPackage("json-default")
public class LoginAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Resource
	private LoginServiceInf loginService;
	
	public ManagerVo managerVo;

	@Action(value = "/loginAction", results = {
			@Result(name = "success", type = "dispatcher", location = "/WEB-INF/management/management.jsp"),
			@Result(name = "error", type = "dispatcher", location = "/index.jsp") })
	public String loginAction() {
		ManagerVo logins = loginService.logins(managerVo);
		if(null==logins){
			return "error";
		}
		return "success";
	}
	public ManagerVo getManagerVo() {
		return managerVo;
	}

	public void setManagerVo(ManagerVo managerVo) {
		this.managerVo = managerVo;
	}
}
