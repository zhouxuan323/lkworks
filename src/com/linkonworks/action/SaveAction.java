package com.linkonworks.action;

import java.io.File;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.linkonworks.service.inf.AlbumServiceInf;
import com.linkonworks.service.inf.DocumentServiceInf;
import com.linkonworks.service.inf.DocumentTypeServiceInf;
import com.linkonworks.vo.AlbumVO;
import com.linkonworks.vo.DocumentTypeVO;
import com.linkonworks.vo.DocumentVO;
import com.linkonworks.vo.FileVO;
import com.linkonworks.vo.MenuVO;
import com.linkonworks.vo.ModelVO;
import com.opensymphony.xwork2.ActionSupport;


@Namespace("\\*")
@ParentPackage("json-default")
public class SaveAction extends ActionSupport{

	private static final long serialVersionUID = 1L;
	//类型接口
	@Resource
	public DocumentTypeServiceInf documenttypeService;
	
	@Resource
	public AlbumServiceInf albumService;
	
	//类型接收参数
	public DocumentTypeVO documenttypeVo;
	//类型返回值
	public List<DocumentTypeVO> documenttypeList;
	//文章接口
	@Resource
	public DocumentServiceInf documentService;
	//文章接收参数
	public DocumentVO documentVo;
	
	//文章返回值
	public List<DocumentVO> documentList;
	
	//图片接收参数
	public AlbumVO albumVo;
	
	//图片返回值
	public List<AlbumVO> albumList;
	
	//菜单接收参数
	private MenuVO menuVo;
	
	//接收返回参数的条数
	public int result;
	
	//模型接收参数
	public ModelVO modelVo;
	
	//接收上传文件
	private List<File> file;
	
	//接收删除图片的字符串id
	private String adidstr;
	
	//接收删除document的字符串id
	private String daidstr;
	
	//接收批量的图片
	private List<FileVO> fileVOList;
	
	//创建保存产品方案Action方法
	@Action(value = "/savecpfaAction",results = {@Result(name = "success", type = "redirectAction")})
	public String savecpfaAction(){		
		try {
			//如果存在页面删除图片
			if(!StringUtils.isBlank(adidstr)){
				if(file==null&& documentList!=null){
					//保存只有文档内容
					documenttypeService.saveDocument(menuVo,adidstr,documentList);
				}else if(documentList==null && file!=null){
					//保存只有图片
					documenttypeService.saveFile(menuVo,adidstr,file);
				}else{
					//保存文档和图片
					documenttypeService.save(menuVo,adidstr,documentList, file);
				}
				
			}else{//如果不存在页面删除图片
			if(file==null&& documentList!=null){
				//保存只有文档内容
				documenttypeService.saveDocument(menuVo,adidstr,documentList);
			}else if(documentList==null && file!=null){
				//保存只有图片
				documenttypeService.saveFile(menuVo,adidstr,file);
			}else{
				//保存文档和图片
				documenttypeService.save(menuVo,adidstr,documentList, file);
			}
			} 
		}catch (Exception e) {
			e.printStackTrace();
		}	
		return "success";

	}
	
	//创建保存核心团队Action方法
	@Action(value = "/savehxtdAction",results = {@Result(name = "success", type = "redirectAction")})
	public String savehxtdAction(){
		try {	
			//如果页面存在要删除的内容
			if (!StringUtils.isBlank(daidstr)) {
				if (fileVOList != null) {
					documenttypeService.saveFileVOList(menuVo,daidstr,fileVOList);
				}else{
					documenttypeService.deleteDocumentDaidstr(daidstr);
				}
			}else{
				//如果页面不存在要删除的内容
				if (fileVOList != null) {
					documenttypeService.saveFileVOList(menuVo,daidstr,fileVOList);
				}
			} 	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";	
	}
	
	//创建保存联系我们Action方法
	@Action(value = "/savelxwmAction",results = {@Result(name = "success", type = "redirectAction")})
	public String savelxwmAction(){
		try {
			documenttypeService.saveLxwmList(menuVo, documentList);
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";	
	}
	
	//创建新增类型Action
	@Action(value = "/insertAction",results = {@Result(name = "success", type = "json")})
	public String insertAction(){
		try{
			
			documenttypeService.insert(menuVo, modelVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
	//类型get和set
	public DocumentTypeVO getDocumenttypeVo() {
		return documenttypeVo;
	}
	
	public void setDocumenttypeVo(DocumentTypeVO documenttypeVo) {
		this.documenttypeVo = documenttypeVo;
	}
	
	//文章set和get
	public DocumentVO getDocumentVo() {
		return documentVo;
	}
	
	public void setDocumentVo(DocumentVO documentVo) {
		this.documentVo = documentVo;
	}
	
	//图片set和get
	public AlbumVO getAlbumVo() {
		return albumVo;
	}
	
	public void setAlbumVo(AlbumVO albumVo) {
		this.albumVo = albumVo;
	}
	
	public List<DocumentTypeVO> getDocumenttypeList() {
		return documenttypeList;
	}
	
	public List<DocumentVO> getDocumentList() {
		return documentList;
	}
	
	public List<AlbumVO> getAlbumList() {
		return albumList;
	}
	
	public MenuVO getMenuVo() {
		return menuVo;
	}

	public void setMenuVo(MenuVO menuVo) {
		this.menuVo = menuVo;
	}
	
	public void setFile(List<File> file) {
		this.file = file;
	}
	
	public List<File> getFile() {
		return file;
	}
	
	public String getAdidstr() {
		return adidstr;
	}
	
	public void setAdidstr(String adidstr) {
		this.adidstr = adidstr;
	}
	
	public List<FileVO> getFileVOList() {
		return fileVOList;
	}
	
	public void setFileVOList(List<FileVO> fileVOList) {
		this.fileVOList = fileVOList;
	}
	
	public String getDaidstr() {
		return daidstr;
	}
	
	public void setDaidstr(String daidstr) {
		this.daidstr = daidstr;
	}

}
