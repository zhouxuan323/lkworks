package com.linkonworks.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.linkonworks.service.inf.MenuServiceInf;
import com.linkonworks.vo.MenuVO;
import com.linkonworks.vo.TreeVO;
import com.opensymphony.xwork2.ActionSupport;

@Namespace("\\*")
@ParentPackage("json-default")
public class MenuAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	@Resource
	private MenuServiceInf menuService;
	// ==返回值
	private List<MenuVO> menuList;
	// 接收返回值
	private int result;

	private List<TreeVO> treeList;
	// ==接收参数
	private MenuVO menuVO;
	private TreeVO treeVO;

	public int getResult() {
		return result;
	}

	/**
	 * 查询菜单的action
	 * 
	 * @return
	 */
	@Action(value = "/menuAction", results = { @Result(name = "success", type = "json") })
	public String menuAction() {
		treeList = new ArrayList<TreeVO>();
		try {
			//MenuVO menuVO = new MenuVO();
			// 获取查询结果
			menuList = menuService.menuList(menuVO);			
			// 设置菜单节点
			List<TreeVO> parent = new ArrayList<TreeVO>();
			TreeVO root = new TreeVO();			
			    root.setId("-1");
			    root.setText("菜单");	
			    root.setUrl("shouye");
			    root.setDescribe("caidan");
			    root.setIsdel(new Short("0"));
			// 循环遍历一级菜单
			for (MenuVO vo : menuList) {
				TreeVO treevo = new TreeVO();
				// 获取id的值
				treevo.setId(vo.getMid());
				// 获取text的值
				treevo.setText(vo.getName());
				treevo.setUrl(vo.getUrl());
				treevo.setDescribe(vo.getDescribe());
				treevo.setIsdel(vo.getIsdel());
				List<TreeVO> children = new ArrayList<TreeVO>();
				// 循环遍历二级菜单
				for (MenuVO svo : vo.getMenuList()) {
					TreeVO secVo = new TreeVO();
					secVo.setId(svo.getMid());
					secVo.setText(svo.getName());
					secVo.setUrl(svo.getUrl());
					secVo.setDescribe(svo.getDescribe());
					secVo.setIsdel(svo.getIsdel());
					List<TreeVO> threechild = new ArrayList<TreeVO>();
					// 循环遍历三级菜单
					for (MenuVO tvo : svo.getMenuList()) {
						TreeVO thiVo = new TreeVO();
						thiVo.setId(tvo.getMid());
						thiVo.setText(tvo.getName());
						thiVo.setUrl(tvo.getUrl());
						thiVo.setDescribe(tvo.getDescribe());
						thiVo.setIsdel(tvo.getIsdel());
						threechild.add(thiVo);
					}
					// 二级菜单的子节点
					secVo.setChildren(threechild);
					children.add(secVo);
				}
				// 一级菜单的子节点
				treevo.setChildren(children);
				// treeList.add(root);
				parent.add(treevo);
			}
			// 菜单节点的子节点
			root.setChildren(parent);
			treeList.add(root);
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
/**
 * 仅仅查询菜单的值 ，用于加载下拉列表，获取url
 */
	@Action(value = "/selectUrlAction", results = { @Result(name = "success", type = "json") })
	public String selectUrlAction() {
		try {
			menuList = menuService.selecturlList(menuVO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
		
	}
	
	
	/**
	 * 删除菜单的action
	 * 
	 * @return
	 */
	@Action(value = "/deletemenu", results = { @Result(name = "success", type = "json") })
	public String deleteAction() {
		try {
			// treeVO接收js中传入的值，再传到service层去
			result = menuService.deletemenu(treeVO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
	/**
	 * 保存（修改时用的）菜单的action
	 * @param result
	 */
	@Action(value = "/updatemenu",results = { @Result(name = "success", type = "json") })
	public String updateAction(){
		try {
			result = menuService.updatemenu(treeVO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
	/**
	 * 保存（新增时使用）菜单的action
	 * @param result
	 */
	@Action(value = "/save1menu",results = { @Result(name = "success", type = "json") })
	public String insertAction(){
		try {
			result = menuService.insertmenu(menuVO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
	

	public void setResult(int result) {
		this.result = result;
	}

	public List<MenuVO> getMenuList() {
		return menuList;
	}

	public List<TreeVO> getTreeList() {
		return treeList;
	}

	public MenuVO getMenuVO() {
		return menuVO;
	}

	public void setMenuVO(MenuVO menuVO) {
		this.menuVO = menuVO;
	}

	public TreeVO getTreeVO() {
		return treeVO;
	}

	public void setTreeVO(TreeVO treeVO) {
		this.treeVO = treeVO;
	}
}