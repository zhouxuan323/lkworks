package com.linkonworks.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;

@Namespace("\\*")
@ParentPackage("json-default")
public class ShangchuanAction extends ActionSupport {
	private List<File> file;

	private List<String> fileFileName;

	private List<String> fileContentType;

	private List<String> dataUrl;
	
	@Action(value = "/fileUploadAction", results = { @Result(name = "success", type = "json") })
	public String shangChuan(){
		try {
			dataUrl = new ArrayList<String>();
			String imgpath = "img/";
			
			//循环上传图片
			for (int i = 0; i < file.size(); ++i) {
				InputStream is = new FileInputStream(file.get(i));

				//得到工程保存图片的路径
				String path = ServletActionContext.getServletContext().getRealPath("/");
				System.out.println(path);
				// String root = "D:\\";

				dataUrl.add(imgpath + this.getFileFileName().get(i));
				
				//得到图片保存的位置(根据root来得到图片保存的路径在tomcat下的该工程里)
				File destFile = new File(path + imgpath, this.getFileFileName().get(i));

				//把图片写入到上面设置的路径里
				OutputStream os = new FileOutputStream(destFile);

				byte[] buffer = new byte[400];

				int length = 0;

				while ((length = is.read(buffer)) > 0) {
					os.write(buffer, 0, length);
				}

				is.close();

				os.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return "success";
	}

	
	public List<File> getFile() {
		return file;
	}

	public void setFile(List<File> file) {
		this.file = file;
	}

	public List<String> getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(List<String> fileFileName) {
		this.fileFileName = fileFileName;
	}

	public List<String> getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(List<String> fileContentType) {
		this.fileContentType = fileContentType;
	}

	public List<String> getDataUrl() {
		return dataUrl;
	}

	public void setDataUrl(List<String> dataUrl) {
		this.dataUrl = dataUrl;
	}

}
