package com.linkonworks.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.linkonworks.service.inf.DocumentServiceInf;
import com.linkonworks.service.inf.MenuServiceInf;
import com.linkonworks.vo.AlbumVO;
import com.linkonworks.vo.DocumentTypeVO;
import com.linkonworks.vo.DocumentVO;
import com.linkonworks.vo.MenuVO;
import com.opensymphony.xwork2.ActionSupport;

@Namespace("\\*")
@ParentPackage("json-default")
public class NeirongAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	@Resource
	private DocumentServiceInf doucumentServiceInf;
	
	@Resource
	private MenuServiceInf menuServiceInf;
	
	private DocumentTypeVO documentTypeVO;
	
	private DocumentVO documentVO;	
	
	private MenuVO menuVO;

	public MenuVO getMenuVO() {
		return menuVO;
	}

	public void setMenuVO(MenuVO menuVO) {
		this.menuVO = menuVO;
	}

	//定义文档类型集合
	private List<DocumentTypeVO> documentTypeVOList;

	//定义文档集合
	private List<DocumentVO> documentVOList;

	//定义图片集合
	private List<AlbumVO> albumVOList;
	
	public List<AlbumVO> getAlbumVOList() {
		return albumVOList;
	}

	public void setAlbumVOList(List<AlbumVO> albumVOList) {
		this.albumVOList = albumVOList;
	}

	public DocumentVO getDocumentVO() {
		return documentVO;
	}

	public void setDocumentVO(DocumentVO documentVO) {
		this.documentVO = documentVO;
	}
	
	public DocumentTypeVO getDocumentTypeVO() {
		return documentTypeVO;
	}

	public void setDocumentTypeVO(DocumentTypeVO documentTypeVO) {
		this.documentTypeVO = documentTypeVO;
	}

	public List<DocumentTypeVO> getDocumentTypeVOList() {
		return documentTypeVOList;
	}

	public void setDocumentTypeVOList(List<DocumentTypeVO> documentTypeVOList) {
		this.documentTypeVOList = documentTypeVOList;
	}
	
	public List<DocumentVO> getDocumentVOList() {
		return documentVOList;
	}

	public void setDocumentVOList(List<DocumentVO> documentVOList) {
		this.documentVOList = documentVOList;
	}

	@Action(value = "/neirongAction", results = { @Result(name = "success", type = "json") })
	public String sanjiCaidan() {

		try {	
			
			//根据menuVO的MID查询dtid
			DocumentTypeVO documentType=doucumentServiceInf.findDocumentTypeByMid(menuVO);
			
			//根类型dtid查询文档文字信息
			DocumentVO documentVO =new DocumentVO();
			documentVO.setDtid(documentType.getDtid());
			documentVOList=doucumentServiceInf.selectDocumentByExample(documentVO);
			
			//查询图片信息
			AlbumVO documentAlbumVO=new AlbumVO();
			documentAlbumVO.setDtid(documentType.getDtid());
			albumVOList=doucumentServiceInf.selectDocumentAlbumByExample(documentAlbumVO);
		
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "success";

	}


	

}
