package com.linkonworks.dao.inf;

import com.linkonworks.po.DocumentTypePO;
import com.linkonworks.po.DocumentTypePOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DocumentTypePOMapper {
    int countByExample(DocumentTypePOExample example);

    int deleteByExample(DocumentTypePOExample example);

    int deleteByPrimaryKey(String dtid);

    int insert(DocumentTypePO record);

    int insertSelective(DocumentTypePO record);

    List<DocumentTypePO> selectByExample(DocumentTypePOExample example);

    DocumentTypePO selectByPrimaryKey(String dtid);

    int updateByExampleSelective(@Param("record") DocumentTypePO record, @Param("example") DocumentTypePOExample example);

    int updateByExample(@Param("record") DocumentTypePO record, @Param("example") DocumentTypePOExample example);

    int updateByPrimaryKeySelective(DocumentTypePO record);

    int updateByPrimaryKey(DocumentTypePO record);
}