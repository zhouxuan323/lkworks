package com.linkonworks.dao.inf;

import com.linkonworks.po.ManagerPo;
import com.linkonworks.po.ManagerPoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ManagerPoMapper {
    int countByExample(ManagerPoExample example);

    int deleteByExample(ManagerPoExample example);

    int insert(ManagerPo record);

    int insertSelective(ManagerPo record);

    List<ManagerPo> selectByExample(ManagerPoExample example);

    int updateByExampleSelective(@Param("record") ManagerPo record, @Param("example") ManagerPoExample example);

    int updateByExample(@Param("record") ManagerPo record, @Param("example") ManagerPoExample example);
}