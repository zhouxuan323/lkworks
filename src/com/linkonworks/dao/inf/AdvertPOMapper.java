package com.linkonworks.dao.inf;

import com.linkonworks.po.AdvertPO;
import com.linkonworks.po.AdvertPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AdvertPOMapper {
    int countByExample(AdvertPOExample example);

    int deleteByExample(AdvertPOExample example);

    int deleteByPrimaryKey(String itid);

    int insert(AdvertPO record);

    int insertSelective(AdvertPO record);

    List<AdvertPO> selectByExample(AdvertPOExample example);

    AdvertPO selectByPrimaryKey(String itid);

    int updateByExampleSelective(@Param("record") AdvertPO record, @Param("example") AdvertPOExample example);

    int updateByExample(@Param("record") AdvertPO record, @Param("example") AdvertPOExample example);

    int updateByPrimaryKeySelective(AdvertPO record);

    int updateByPrimaryKey(AdvertPO record);
}