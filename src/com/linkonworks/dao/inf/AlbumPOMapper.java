package com.linkonworks.dao.inf;

import com.linkonworks.po.AlbumPO;
import com.linkonworks.po.AlbumPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AlbumPOMapper {
    int countByExample(AlbumPOExample example);

    int deleteByExample(AlbumPOExample example);

    int deleteByPrimaryKey(String adid);

    int insert(AlbumPO record);

    int insertSelective(AlbumPO record);

    List<AlbumPO> selectByExample(AlbumPOExample example);

    AlbumPO selectByPrimaryKey(String adid);

    int updateByExampleSelective(@Param("record") AlbumPO record, @Param("example") AlbumPOExample example);

    int updateByExample(@Param("record") AlbumPO record, @Param("example") AlbumPOExample example);

    int updateByPrimaryKeySelective(AlbumPO record);

    int updateByPrimaryKey(AlbumPO record);
}