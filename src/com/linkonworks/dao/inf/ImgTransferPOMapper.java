package com.linkonworks.dao.inf;

import com.linkonworks.po.ImgTransferPO;
import com.linkonworks.po.ImgTransferPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ImgTransferPOMapper {
    int countByExample(ImgTransferPOExample example);

    int deleteByExample(ImgTransferPOExample example);

    int deleteByPrimaryKey(String itid);

    int insert(ImgTransferPO record);

    int insertSelective(ImgTransferPO record);

    List<ImgTransferPO> selectByExample(ImgTransferPOExample example);

    ImgTransferPO selectByPrimaryKey(String itid);

    int updateByExampleSelective(@Param("record") ImgTransferPO record, @Param("example") ImgTransferPOExample example);

    int updateByExample(@Param("record") ImgTransferPO record, @Param("example") ImgTransferPOExample example);

    int updateByPrimaryKeySelective(ImgTransferPO record);

    int updateByPrimaryKey(ImgTransferPO record);
}