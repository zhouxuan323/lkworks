package com.linkonworks.dao.inf;

import com.linkonworks.po.MenuPO;
import com.linkonworks.po.MenuPOExample;
import com.linkonworks.vo.MenuVO;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface MenuPOMapper {
    int countByExample(MenuPOExample example);

    int deleteByExample(MenuPOExample example);

    int deleteByPrimaryKey(String mid);

    int insert(MenuPO record);

    int insertSelective(MenuPO record);

    List<MenuPO> selectByExample(MenuPOExample example);

    MenuPO selectByPrimaryKey(String mid);

    int updateByExampleSelective(@Param("record") MenuPO record, @Param("example") MenuPOExample example);

    int updateByExample(@Param("record") MenuPO record, @Param("example") MenuPOExample example);

    int updateByPrimaryKeySelective(MenuPO record);

    int updateByPrimaryKey(MenuPO record);

	List<MenuPO> selectByPmid(String pmid);
}