package com.linkonworks.dao.inf;

import com.linkonworks.po.CustomPO;
import com.linkonworks.po.CustomPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CustomPOMapper {
    int countByExample(CustomPOExample example);

    int deleteByExample(CustomPOExample example);

    int deleteByPrimaryKey(String cid);

    int insert(CustomPO record);

    int insertSelective(CustomPO record);

    List<CustomPO> selectByExample(CustomPOExample example);

    CustomPO selectByPrimaryKey(String cid);

    int updateByExampleSelective(@Param("record") CustomPO record, @Param("example") CustomPOExample example);

    int updateByExample(@Param("record") CustomPO record, @Param("example") CustomPOExample example);

    int updateByPrimaryKeySelective(CustomPO record);

    int updateByPrimaryKey(CustomPO record);
}