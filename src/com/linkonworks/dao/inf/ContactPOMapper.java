package com.linkonworks.dao.inf;

import com.linkonworks.po.ContactPO;
import com.linkonworks.po.ContactPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ContactPOMapper {
    int countByExample(ContactPOExample example);

    int deleteByExample(ContactPOExample example);

    int deleteByPrimaryKey(String cid);

    int insert(ContactPO record);

    int insertSelective(ContactPO record);

    List<ContactPO> selectByExample(ContactPOExample example);

    ContactPO selectByPrimaryKey(String cid);

    int updateByExampleSelective(@Param("record") ContactPO record, @Param("example") ContactPOExample example);

    int updateByExample(@Param("record") ContactPO record, @Param("example") ContactPOExample example);

    int updateByPrimaryKeySelective(ContactPO record);

    int updateByPrimaryKey(ContactPO record);
}