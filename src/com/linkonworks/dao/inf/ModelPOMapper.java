package com.linkonworks.dao.inf;

import com.linkonworks.po.ModelPO;
import com.linkonworks.po.ModelPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ModelPOMapper {
    int countByExample(ModelPOExample example);

    int deleteByExample(ModelPOExample example);

    int deleteByPrimaryKey(String modelid);

    int insert(ModelPO record);

    int insertSelective(ModelPO record);

    List<ModelPO> selectByExample(ModelPOExample example);

    ModelPO selectByPrimaryKey(String modelid);

    int updateByExampleSelective(@Param("record") ModelPO record, @Param("example") ModelPOExample example);

    int updateByExample(@Param("record") ModelPO record, @Param("example") ModelPOExample example);

    int updateByPrimaryKeySelective(ModelPO record);

    int updateByPrimaryKey(ModelPO record);
}