package com.linkonworks.dao.inf;

import com.linkonworks.po.DocumentPO;
import com.linkonworks.po.DocumentPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DocumentPOMapper {
    int countByExample(DocumentPOExample example);

    int deleteByExample(DocumentPOExample example);

    int deleteByPrimaryKey(String daid);

    int insert(DocumentPO record);

    int insertSelective(DocumentPO record);

    List<DocumentPO> selectByExample(DocumentPOExample example);

    DocumentPO selectByPrimaryKey(String daid);

    int updateByExampleSelective(@Param("record") DocumentPO record, @Param("example") DocumentPOExample example);

    int updateByExample(@Param("record") DocumentPO record, @Param("example") DocumentPOExample example);

    int updateByPrimaryKeySelective(DocumentPO record);

    int updateByPrimaryKey(DocumentPO record);
}