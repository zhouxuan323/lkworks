package com.linkonworks.dao.inf;

import com.linkonworks.po.DocTransferPO;
import com.linkonworks.po.DocTransferPOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DocTransferPOMapper {
    int countByExample(DocTransferPOExample example);

    int deleteByExample(DocTransferPOExample example);

    int deleteByPrimaryKey(String id);

    int insert(DocTransferPO record);

    int insertSelective(DocTransferPO record);

    List<DocTransferPO> selectByExample(DocTransferPOExample example);

    DocTransferPO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") DocTransferPO record, @Param("example") DocTransferPOExample example);

    int updateByExample(@Param("record") DocTransferPO record, @Param("example") DocTransferPOExample example);

    int updateByPrimaryKeySelective(DocTransferPO record);

    int updateByPrimaryKey(DocTransferPO record);
}