package com.linkonworks.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;


import org.apache.struts2.ServletActionContext;

public class UploadUtil {

	public static List<String> upload(List<File> file) {

		List<String> fileFileName = new ArrayList<String>();
		try {
			// 循环上传图片
			for (int i = 0; i < file.size(); ++i) {
				String filename = IDGenUtils.gen() + ".jpg";
				InputStream is = new FileInputStream(file.get(i));

				// 得到工程保存图片的路径
				String path =ServletActionContext.getServletContext().getRealPath(ApplicationConstant.IMGURL);

				System.out.println(path);

				File destFile = new File(path,filename);

				// 把图片写入到上面设置的路径里
				OutputStream os = new FileOutputStream(destFile);

				byte[] buffer = new byte[400];

				int length = 0;

				while ((length = is.read(buffer)) > 0) {
					os.write(buffer, 0, length);
				}

				is.close();

				os.close();
				
				fileFileName.add(filename);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return fileFileName;
	}

}
