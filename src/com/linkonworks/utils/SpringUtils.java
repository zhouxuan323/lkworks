package com.linkonworks.utils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class SpringUtils {
	private static ApplicationContext context;

	  public static ApplicationContext getApplicationContext()
	  {
	    if (context == null)
	    {
	      synchronized (SpringUtils.class)
	      {
	        if (context == null)
	        {
	          try
	          {
	            context =
	                WebApplicationContextUtils.getRequiredWebApplicationContext(HTTPUtils.getServletContext());
	          }
	          catch (Throwable e)
	          {
	            e.printStackTrace();
	            context =
	                new ClassPathXmlApplicationContext(
	                		new String[]{"com/linkonworks/conf/applicationContext.xml", "com/linkonworks/conf/applicationContext-Dao.xml"});
	          }
	        }
	      }
	    }
	    return context;
	  }

	  @SuppressWarnings("unchecked")
	  public static <T> T getBean(String name)
	  {
	    return (T) getApplicationContext().getBean(name);
	  }
}
