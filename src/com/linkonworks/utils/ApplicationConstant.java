package com.linkonworks.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Properties;


/**
 * 系统常量类
 * 
 * @author Administrator
 * 
 */
public class ApplicationConstant {
	/* ====是否删除 */
	public final static short NO = 0;// 否
	public final static short YES = 1;// 是

	/* ===中英文标识符 */
	public final static String LNG_CN = "cn";// 中文
	public final static String LNG_EN = "en";// 英文

	/* ===中英文标识符 */
	public final static String MODEL_00 = "0";// 通用
	public final static String MODEL_01 = "1";// 首页
	public final static String MODEL_02 = "2";// 联系方式
	public final static String MODEL_03 = "3";// 自定义

	public final static String IMGURL = initImgurl();

	/**
	 * 获取图片上传路径
	 * @return
	 */
	private static String initImgurl() {
		String IMGURL = "";
		try {
			String path = UploadUtil.class.getClassLoader().getResource("").toURI().getPath();
			Properties props = new Properties();
			InputStream inputStream = new FileInputStream(new File(path+ "com/linkonworks/conf/config.properties"));
			props.load(inputStream);
			IMGURL = props.getProperty("IMGURL");
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return IMGURL;
	}

}
