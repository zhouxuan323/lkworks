package com.linkonworks.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public  class  DateUtil  {  

    /**  
      *  格式化输出日期  
      *    
      *  @param  date  
      *                        日期  
      *  @param  format  
      *                        格式  
      *  @return  返回字符型日期  
      */  
    public  static  String  format(Date  date,  String  format)  {  
                String  result  =  "";  
                try  {  
                            if  (date  !=  null)  {  
                                        DateFormat  df  =  new  SimpleDateFormat(format);  
                                        result  =  df.format(date);  
                            }  
                }  catch  (Exception  e)  {  
                }  
                return  result;  
    }  

    public  static  String  format(Date  date)  {  
                return  format(date,  "yyyy/MM/dd");  
    }  

    /**  
      *  返回年份  
      *    
      *  @param  date  
      *                        日期  
      *  @return  返回年份  
      */  
    public  static  int  getYear(Date  date)  {  
                Calendar  c  =  Calendar.getInstance();  
                c.setTime(date);  
                return  c.get(Calendar.YEAR);  
    }  

    /**  
      *  返回月份  
      *    
      *  @param  date  
      *                        日期  
      *  @return  返回月份  
      */  
    public  static  int  getMonth(Date  date)  {  
                Calendar  c  =  Calendar.getInstance();  
                c.setTime(date);  
                return  c.get(Calendar.MONTH)  +  1;  
    }  

    /**  
      *  返回日份  
      *    
      *  @param  date  
      *                        日期  
      *  @return  返回日份  
      */  
    public  static  int  getDay(Date  date)  {  
                Calendar  c  =  Calendar.getInstance();  
                c.setTime(date);  
                return  c.get(Calendar.DAY_OF_MONTH);  
    }  

    /**  
      *  返回小时  
      *    
      *  @param  date  
      *                        日期  
      *  @return  返回小时  
      */  
    public  static  int  getHour(Date  date)  {  
                Calendar  c  =  Calendar.getInstance();  
                c.setTime(date);  
                return  c.get(Calendar.HOUR_OF_DAY);  
    }  

    /**  
      *  返回分钟  
      *    
      *  @param  date  
      *                        日期  
      *  @return  返回分钟  
      */  
    public  static  int  getMinute(Date  date)  {  
                Calendar  c  =  Calendar.getInstance();  
                c.setTime(date);  
                return  c.get(Calendar.MINUTE);  
    }  

    /**  
      *  返回秒钟  
      *    
      *  @param  date  
      *                        日期  
      *  @return  返回秒钟  
      */  
    public  static  int  getSecond(Date  date)  {  
                Calendar  c  =  Calendar.getInstance();  
                c.setTime(date);  
                return  c.get(Calendar.SECOND);  
    }  

    /**  
      *  返回毫秒  
      *    
      *  @param  date  
      *                        日期  
      *  @return  返回毫秒  
      */  
    public  static  long  getMillis(Date  date)  {  
                Calendar  c  =  Calendar.getInstance();  
                c.setTime(date);  
                return  c.getTimeInMillis();  
    }  

    /**  
      *  返回字符型日期  
      *    
      *  @param  date  
      *                        日期  
      *  @return  返回字符型日期  
      */  
    public  static  String  getDate(Date  date)  {  
                return  format(date,  "yyyy/MM/dd");  
    }  

    /**  
      *  返回字符型时间  
      *    
      *  @param  date  
      *                        日期  
      *  @return  返回字符型时间  
      */  
    public  static  String  getTime(Date  date)  {  
                return  format(date,  "HH:mm:ss");  
    }  

    /**  
      *  返回字符型日期时间  
      *    
      *  @param  date  
      *                        日期  
      *  @return  返回字符型日期时间  
      */  
    public  static  String  getDateTime(Date  date)  {  
                return  format(date,  "yyyy/MM/dd  HH:mm:ss");  
    }  

    /**  
      *  日期相加  
      *    
      *  @param  date  
      *                        日期  
      *  @param  day  
      *                        天数  
      *  @return  返回相加后的日期  
      */  
    public  static  Date  addDate(Date  date,  int  day)  {  
                Calendar  c  =  Calendar.getInstance();  
                c.setTimeInMillis(getMillis(date)  +  ((long)  day)  *  24  *  3600  *  1000);  
                return  c.getTime();  
    }  

    /**  
      *  日期相减  
      *    
      *  @param  date  
      *                        日期  
      *  @param  date1  
      *                        日期  
      *  @return  返回相减后的日期  
      */  
    public  static  int  diffDate(Date  date,  Date  date1)  {  
                return  (int)  ((getMillis(date)  -  getMillis(date1))  /  (24  *  3600  *  1000));  
    }   
    
    /**  
     *  取得指定月份的第一天  
     *  
     *  @param Date  date  
     *  @return  String  
     */  
   public static String  getMonthBegin(Date  date)  
   {  
           return  formatDateByFormat(date,"yyyy-MM")  +  "-01";  
   }  

   /**  
     *  取得指定日期月份的最后一天  
     *  
     *  @param  Date  date  
     *  @return  String  
     */  
   @SuppressWarnings("deprecation")
public static Date  getMonthEnd(Date  date)  
   {  
	   Calendar   cDay1   =   Calendar.getInstance();  
       cDay1.setTime(date);  
       final   int   lastDay   =   cDay1.getActualMaximum(Calendar.DAY_OF_MONTH);  
       Date   lastDate   =   cDay1.getTime();  
       lastDate.setDate(lastDay);  
       return   lastDate; 
   } 
   
   /**
    * 获取日期下个月的这一天
    */
   public static String  getNextMonth(Date  date)  
   {  
           Calendar  calendar  =  Calendar.getInstance();  
           calendar.setTime(date);  
           calendar.add(Calendar.MONTH,1);  
           calendar.add(Calendar.DAY_OF_YEAR,  -1);  
           return  formatDate(calendar.getTime());  
   }  
   /**  
     *  常用的格式化日期  
     *  
     *  @param  date  Date  
     *  @return  String  
     */  
   public static  String  formatDate(Date  date)  
   {  
           return  formatDateByFormat(date,"yyyy-MM-dd");  
   }  

   /**  
     *  以指定的格式来格式化日期  
     *  
     *  @param  date  Date  
     *  @param  format  String  
     *  @return  String  
     */  
   public static String  formatDateByFormat(Date  date,String  format)  
   {  
           String  result  =  "";  
           if(date  !=  null)  
           {  
                   try  
                   {  
                           SimpleDateFormat  sdf  =  new  SimpleDateFormat(format);  
                           result  =  sdf.format(date);  
                   }  
                   catch(Exception  ex)  
                   {  
                           ex.printStackTrace();  
                   }  
           }  
           return  result;  
   }  
   /**  
    *Date,String 间相互转换
    */
   public static Date stringToDate(String strDate){
	   DateFormat fmt =new SimpleDateFormat("yyyy-MM-dd");
	   Date date = null;	
	   try {
			 date = fmt.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
   /**  
    *计算年龄
    */
   public static int getBirthday(String strDate){
	   Date date = stringToDate(strDate);
	   int birth_year = getYear(date);
	   int local_year = getYear(new Date());
	   int age = local_year-birth_year;
	   return age;
   }
} 
