package com.linkonworks.utils;

import java.util.Date;
import java.util.Random;
import java.text.SimpleDateFormat;
/**
 * ID 生成器 日期+时间（毫秒） + 两随机数数
 * @author gaoxin'guo
 *
 */
public class IDGenUtils {
	private final static String DATA_FORMAT = "yyyyMMddHHmmssSSS";
	
	public static synchronized String gen(){
		Random random = new Random();
		
		SimpleDateFormat sf = new SimpleDateFormat(DATA_FORMAT);
		Date nowDate = new Date();
		return sf.format(nowDate) + random.nextInt(10) 
				+ random.nextInt(10) + random.nextInt(10)+ random.nextInt(10) + random.nextInt(10)
				+ random.nextInt(10)+ random.nextInt(10) + random.nextInt(10);
	}
	
	public static void main(String[] args) {
		for(int i = 0; i < 10000; i++){
			Runnable t = new Runnable() {
				public void run() {
					System.out.println(IDGenUtils.gen());
				}
			};
			new Thread(t).start();
		}
		
	}
}
