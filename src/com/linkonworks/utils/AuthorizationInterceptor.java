package com.linkonworks.utils;

import java.util.Map;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class AuthorizationInterceptor extends AbstractInterceptor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5695426215702037692L;

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		Map<String, Object> session = invocation.getInvocationContext()
				.getSession();

		String userName = (String) session.get("user");

		if (null != userName) {
			return invocation.invoke();
		} else {
			invocation.getInvocationContext().getValueStack().set("str", "请先登录系统！");
			return Action.LOGIN;

		}
	}

}
