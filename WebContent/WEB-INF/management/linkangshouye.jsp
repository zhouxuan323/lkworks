<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String mid=request.getParameter("mid");
	String lng=request.getParameter("lng");
%>
<script type="text/javascript" src="<%=path %>/resources/jquery/jquery.form.js"></script>
<script type="text/javascript" src="<%=path %>/resources/jquery/ajaxfileupload.js"></script>
<script type="text/javascript" src="<%=path%>/js/home.js"></script>
<script type="text/javascript">
var path = '<%=path%>';
var mid = '<%=mid%>';
var lng = '<%=lng%>';
	$(function() {		
		selectAdvert();	
		docTransferselect();
		imgTransferselect();

	});
	
	
	
</script>

<div id="index" class="easyui-layout" fit="true" style="width: 1000px; height: 635px;">
	<div data-options="region:'north',title:'首页图片轮换',split:true"style="height: 300px;">
		<div>
		<a href="javascript:void(0)" id="add" class="easyui-linkbutton" iconCls="icon-add" plain="true">新增</a> 
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="advertsave()">保存</a> 
		<input type="button"  value="是否滚动" onclick="isroll()"/> 
		</div>
		<form action="saveAdvert.do" method="post" enctype="multipart/form-data" id="form1">
	
		  <input type="text" name="menuVO.mid" value=<%=mid%> style="visibility:hidden">
		  <input type="text" name="menuVO.lng" value=<%=lng%> style="visibility:hidden">
		<!-- t图片轮转界面 -->
		<div class="main_image">
		</div>
        </form>
	</div>	
	<div data-options="region:'west',title:'首页文字链接',split:true"style="width: 360px;">
		<div>
		<a href="javascript:void(0)" id="add2" class="easyui-linkbutton" iconCls="icon-add" plain="true">新增</a> 
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="docTransfersave()">保存</a>
		</div>
		<form action="saveDoctransfer.do" method="post" enctype="multipart/form-data" id="form2">
		  <input type="text" name="documentTypeVO.mid" value=<%=mid%> style="visibility:hidden">
		  <input type="text" name="documentTypeVO.lng" value=<%=lng%> style="visibility:hidden">	
		<!-- 文字快捷链接界面 -->
		<div class="Introduction">
		</div>
		<input type="submit"  style="visibility:hidden;">
		</form>
	</div>
	<div data-options="region:'center',title:'首页图片链接'">
		<div>
		<a href="javascript:void(0)" id="add1" class="easyui-linkbutton" iconCls="icon-add" plain="true">新增</a> 
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="imgTransfersave()">保存</a>
		</div>
		<form action="saveImgtransfer.do" method="post" enctype="multipart/form-data" id="form3">
		  <input type="text" name="menuVO.mid" value=<%=mid%> style="visibility:hidden">
		  <input type="text" name="menuVO.lng" value=<%=lng%> style="visibility:hidden">
		<!-- 图片快捷链接界面 -->
		<div class="box"></div>
		 <!-- <input type="submit"  style="visibility:hidden;"> --> 
		</form>
	</div>
</div>
<div id="rollDialog" class="easyui-dialog" title="滚动设置" modal="true" closed="true"  border="false">
			<div id="index_top" style="width:450px;height:30px;" >
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="savetime()">保存</a>
				<span style="float:right;padding-top:10px;"><input  type="checkbox" id="istime" checked="checked"/>是否启用滚动  </span>        
		    </div>
		     <input id="id" type="hidden"/> 	    
					    <div style="width:420px;height:25px;border:1px solid #CCC;float:left;padding-left:0;margin-left:15px;margin-top:10px; overflow:hidden;padding-top:15px;">
							<label  style="	display: inline-block;width: 110px;text-align:right;">滚动时间设置：</label>
							<input id="time" class="easyui-validatebox textbox"   style="width:250px;" />
					    </div>
					    
  </div>
