﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<title>林康--后台管理系统</title>
<meta name="keywords" content="林康" />
<meta name="description" content="" />
<link rel="stylesheet" type="text/css"href="<%=path%>/resources/easyui/themes/default/easyui.css" />
<link rel="stylesheet" type="text/css"href="<%=path%>/resources/easyui/themes/icon.css" />
<script type="text/javascript"src="<%=path%>/resources/jquery/jquery-1.7.2.min.js">var path='<%=path%>';</script>
<script type="text/javascript"src="<%=path%>/resources/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"src="<%=path%>/resources/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="<%=path%>/js/menu.js"></script>

<script type="text/javascript">
	$(function() {
		//给菜单添加点击事件 --获取当前选中的菜单的mid
		$("#l_menu").children("#menutree").click(
				function() {
					var node = $(this).tree('getSelected');
					if (!$(this).tree('isLeaf', node.target)) {
						//alert("a")
						//alert("")
						return;
					}
					var tabTitle = node.text;

					if ($('#r_content').tabs('exists', tabTitle)) {
						$('#r_content').tabs('select', tabTitle);
					} else {
						var link = 'dispatcherAction.do?menuVO.mid=' + node.id
								+ '&source=0&menuVO.lng='
								+ $.trim($('#language').combobox('getValue'));

						$('#r_content').tabs(
								"close",
								$('#r_content').tabs('getSelected').panel(
										'options').title);
						$('#r_content').tabs(
								'add',
								{
									id : node.id,
									title : node.text,
									href : link,
									closable : true,
									tools : [ {
										iconCls : 'icon-mini-refresh',
										handler : function() {
											//刷新tab
											var currentTab = $('#r_content')
													.tabs('getTab', tabTitle);
											$('#r_content').tabs('update', {
												tab : currentTab,
												options : {
													id : node.id,
													title : node.text,
													href : link,
												}
											});
											currentTab.panel('refresh', link);
										}
									} ]
								});
					}

				});
	});
</script>

</head>
<body class="easyui-layout">
	<div id="l_menu" data-options="region:'west',title:'菜单',split:true"
		style="width: 350px;">
		<div id="toolbar_menu">
			<a href="javascript:void(0)" class="easyui-linkbutton"
				iconCls="icon-add" plain="true" onclick="insert()">新增</a> <a
				href="javascript:void(0)" class="easyui-linkbutton"
				iconCls="icon-edit" plain="true" onclick="update()">修改</a> <a
				href="javascript:void(0)" class="easyui-linkbutton"
				iconCls="icon-remove" plain="true" onclick="deletemenu()">删除</a> <label>选择语言：</label>
			<select id="language" class="easyui-combobox" name="lng"
				style="width: 80px">
				<option value="cn">中文</option>
				<option value="en">ENGLISH</option>
			</select>
			<!--  <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="save()">保存</a> -->
		</div>
		<ul id="menutree" class="easyui-tree" checkbox="true" lines="true">
		</ul>
	</div>
	<div id="r_content" data-options="region:'center'" class="easyui-tabs">
		<div title="Tab1" style="padding: 20px; display: none;">tab1</div>
	</div>
</body>

<div id="menuDialog" class="easyui-dialog" title="修改" modal="true"
	closed="true" border="false">
	<form id="menu1Form" method="post">
		<div id="index_top" data-options="region:'north',border:'false'"
			split="false" style="width: 280px; height: 30px;">
			<a href="javascript:void(0)" class="easyui-linkbutton"
				iconCls="icon-save" plain="true" onclick="save()">保存</a> <span
				style="float: right; padding-top: 10px;"> <input
				type="checkbox" id="isdel" checked="checked" />是否启用该菜单
			</span>
		</div>
		<input id="id" type="hidden" />
		<div
			style="width: 420px; height: 25px; border: 1px solid #CCC; float: left; padding-left: 0; margin-left: 15px; margin-top: 10px; overflow: hidden; padding-top: 15px;">
			<label
				style="display: inline-block; width: 110px; text-align: right;">名称：</label>
			<input id="text" class="easyui-validatebox textbox"
				style="width: 250px;" />
		</div>
		<div
			style="width: 420px; height: 25px; border: 1px solid #CCC; float: left; padding-left: 0; margin-left: 15px; margin-top: 10px; overflow: hidden; padding-top: 15px;">
			<label
				style="display: inline-block; width: 110px; text-align: right;">路径：</label>
			<input id="url" class="easyui-validatebox textbox"
				disabled="disabled" style="width: 250px;" />
		</div>
		<div
			style="width: 420px; height: 25px; border: 1px solid #CCC; float: left; padding-left: 0; margin-left: 15px; margin-top: 10px; overflow: hidden; padding-top: 15px;">
			<label
				style="display: inline-block; width: 110px; text-align: right;">文本描述：</label>
			<input id="describe" class="easyui-validatebox textbox"
				style="width: 250px;" />
		</div>
	</form>
</div>


<div id="insertDialog" class="easyui-dialog" title="新增" modal="true"
	closed="true" border="false">
	<div id="index_top" style="width: 450px; height: 30px;">
		<a href="javascript:void(0)" class="easyui-linkbutton"
			iconCls="icon-save" plain="true" onclick="insertsave()">保存</a> <span
			style="float: right; padding-top: 10px;"><input
			type="checkbox" id="newisdel" checked="checked" />是否启用该菜单 </span>
	</div>
	<input id="id" type="hidden" />
	<div
		style="width: 420px; height: 25px; border: 1px solid #CCC; float: left; padding-left: 0; margin-left: 15px; margin-top: 10px; overflow: hidden; padding-top: 15px;">
		<label style="display: inline-block; width: 110px; text-align: right;">名称：</label>
		<input id="name" class="easyui-validatebox textbox"
			style="width: 250px;" />
	</div>
	<div
		style="width: 420px; height: 25px; border: 1px solid #CCC; float: left; padding-left: 0; margin-left: 15px; margin-top: 10px; overflow: hidden; padding-top: 15px;">
		<label style="display: inline-block; width: 110px; text-align: right;">路径：</label>
		<input id="newurl" class="easyui-validatebox textbox"
			disabled="disabled" style="width: 250px;" />
	</div>
	<div
		style="width: 420px; height: 25px; border: 1px solid #CCC; float: left; padding-left: 0; margin-left: 15px; margin-top: 10px; overflow: hidden; padding-top: 15px;">
		<label style="display: inline-block; width: 110px; text-align: right;">文本描述：</label>
		<input id="newdescribe" class="easyui-validatebox textbox"
			style="width: 250px;" />
	</div>

</div>

</html>