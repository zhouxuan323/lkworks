<!-- 公司文化模型 -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String mid = request.getParameter("mid");
	String lng = request.getParameter("lng");
%>

<script type="text/javascript"
	src="<%=path%>/resources/jquery/jquery.form.js"></script>
<script type="text/javascript"
	src="<%=path%>/resources/jquery/ajaxfileupload.js"></script>
<script type="text/javascript" src="<%=path%>/js/management.js"></script>
<script type="text/javascript" src="<%=path%>/js/gongsiwenhua.js"></script>
<script type="text/javascript">
var mid = '<%=mid%>';
var lng = '<%=lng%>';
var path='<%=path%>';

	//加载数据
$(document).ready(function() {
		$.ajax({
			type : "post",
			url : "neirongAction.do",
			dataType : "json",
			data : {
					"menuVO.mid" : mid,
					"menuVO.lng" : lng
					},
					success : function(data) {
					var documentList = data.documentVOList;
					var html = '';
					var j = -1;
					for (var i = 0; i < documentList.length; i++) {
						j++;
						html += ('<div class = "margin" style = "width:95%;height:130px;border:2px solid green;margin-top:15pt;">');
						html += ('<div class="document">'
								+ '<label>标题:<input name="documentList['+j+'].title" type = "text" value ="'+documentList[i].title+'"></input></label>'
								+ '<label>内容:<textarea rows="5" cols="50" name="documentList['+j+'].content">'
								+ documentList[i].content
								+ '</textarea></label>' + '</div><input class="del" type="button" style = "float:right" value="删除"/>');
								html += ('</div>');
						}
						//追加内容
						$(".gongsiwenhua").append(html);
						//var html1 = $(".adder").html();
						$("#add").click(function() {
						//html1 = html1.replace('行动理念', '').replace('专注、专攻、专业、广纳', '');
						j++;
						$(".gongsiwenhua").append('<div class = "margin" style = "width:95%;height:145px;border:2px solid green;margin-top:15pt;">'
												+ '<div class="document"  style = "margin-top:10pt;">'
												+ '<label>标题：<input type = "text" value ="" name="documentList['+j+'].title"></input></label>&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '<label>内容：<textarea rows="5" cols="50" name="documentList['+j+'].content" value=""></textarea></label></div>'
												+ '<input class="del" type="button" style = "float:right" value="删除"/></div>');
								$(".del").click(function() {
										$(this).parent().remove();
									});
							});
						//删除加载内容
						$(".del").click(function() {
							$(this).parent().remove();
						});
				}

		});
});
</script>

<form action="savecpfaAction.do" method="post" enctype="multipart/form-data" id="form">
	<a href="javascript:void(0)" id="add" class="easyui-linkbutton" iconCls="icon-add" plain="true">新增</a>
	
	<a href="javascript:void(0)" id="savewhfa" class="easyui-linkbutton" iconCls="icon-save" plain="true">保存</a> 
	
	<input type="text"name="menuVo.mid" value='<%=mid%>' style="visibility: hidden" />
	
	<input type="text"name="menuVo.lng" value='<%=lng%>' style="visibility: hidden" />

	<div class="gongsiwenhua" align="center"></div>
</form>