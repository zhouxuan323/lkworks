<!-- 公司文化模型 -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String mid = request.getParameter("mid");
	String lng = request.getParameter("lng");
%>

<script type="text/javascript"
	src="<%=path%>/resources/jquery/jquery.form.js"></script>
<script type="text/javascript"
	src="<%=path%>/resources/jquery/ajaxfileupload.js"></script>
<script type="text/javascript" src="<%=path%>/js/management.js"></script>
<script type="text/javascript" src="<%=path%>/js/chanpinfangan.js"></script>
<script type="text/javascript">
var mid = '<%=mid%>';
var lng = '<%=lng%>';
var path='<%=path%>';

	//加载数据
$(document).ready(function() {
		$.ajax({
			type : "post",
			url : "neirongAction.do",
			dataType : "json",
			data : {
					"menuVO.mid" : mid,
					"menuVO.lng" : lng
					},
					success : function(data) {
					var documentList = data.documentVOList;
					var albumList = data.albumVOList;
					var html = '';
					var muble = '';
					var j = -1;
					for (var i = 0; i < documentList.length; i++) {
						j++;
						
						if(documentList[i].title==null){
							
							
							
							html += ('<div class = "margin" style = "width:600px;height:130px;border:2px solid green;margin-top:15pt;">');
							html += ('<div class="document">'
									+ '<label>内容:<textarea rows="5" cols="50" name="documentList['+j+'].content">'
									+ documentList[i].content
									+ '</textarea></label>' + '</div><input class="del" type="button" style = "float:right" value="删除"/>');
									html += ('</div>');
							}else if(documentList[i].content==null){
								html += ('<div class = "margin" style = "width:600px;height:130px;border:2px solid green;margin-top:15pt;">');
								html += ('<div class="document">'
									 + '<label>标题:<textarea rows="3" cols="50" name="documentList['+j+'].title">'+documentList[i].title+'</textarea></label><br>'
														 + '</div><input class="del" type="button" style = "float:right" value="删除"/>');
									 html += ('</div>');
								
							}else{
								
								
								html += ('<div class = "margin" style = "width:600px;height:200px;border:2px solid green;margin-top:15pt;">');
								html += ('<div class="document">'
									 + '<label>标题:<textarea rows="3" cols="50" name="documentList['+j+'].title">'+documentList[i].title+'</textarea></label><br>'
									 + '<label>内容:<textarea rows="5" cols="50" name="documentList['+j+'].content">'
									 + documentList[i].content
									 + '</textarea></label>' + '</div><input class="del" type="button" style = "float:right" value="删除"/>');
									 html += ('</div>');
							}
						}

						//追加内容
						$(".neirong").append(html);
						
						
						//循环遍历取出图片
						for (var j = 0; j < albumList.length; j++) {
							
							muble+=('<div class="tp" value="'+albumList[j].adid+'" style = "width:600px;height:350px;border:2px solid green;"><label>图片<img alt="" src="'+path+albumList[j].imgurl+albumList[j].imgfilename+'" height="330" width="500"></label><input class="deltp" type="button" value="删除"/></div><br><br>');					
						}
						$(".tupian").html(muble);
						
						
						
						
						
						//var html1 = $(".adder").html();
						$("#cadd").click(function() {
						j++;
						$(".neirong").append('<div class = "margin" style = "width:600px;height:220px;border:2px solid green;margin-top:15pt;">'
												+ '<div class="document"  style = "margin-top:10pt;">'
												+ '<label>标题：<textarea rows="3" cols="50" value ="" name="documentList['+j+'].title"></textarea></label><br>'
												+ '<label>内容：<textarea rows="5" cols="50" name="documentList['+j+'].content" value=""></textarea></label></div>'
												+ '<input class="del" type="button" style = "float:right" value="删除"/></div>');
								$(".del").click(function() {
										$(this).parent().remove();
									});
						});
						//删除加载内容
						$(".del").click(function() {
							$(this).parent().remove();
						});
						
						
						//页面增加图片项
						$("#iadd").click(function(){
							$(".tupian").append('<div class="tp"><input type="file" class="fileList" name="file"><input class="deltp" type="button" value="删除"/></div><br>');
							$(".deltp").click(function() {
								$(this).parent().hide();
							});
						});
						
						//页面图片删除
						$(".deltp").click(function() {
							$(this).parent().hide();
						});
				}

		});
});
</script>

<form action="savecpfaAction.do" method="post" enctype="multipart/form-data" id="form">
	<a href="javascript:void(0)" id="cadd" class="easyui-linkbutton" iconCls="icon-add" plain="true">内容新增</a>
	
	<a href="javascript:void(0)" id="iadd" class="easyui-linkbutton" iconCls="icon-add" plain="true">图片新增</a>
	
	<a href="javascript:void(0)" id="savecpfa" class="easyui-linkbutton" iconCls="icon-save" plain="true">保存</a> 
	
	<input type="text"name="menuVo.mid" value='<%=mid%>' style="visibility: hidden" />
	
	<input type="text"name="menuVo.lng" value='<%=lng%>' style="visibility: hidden" />

	<div class="neirong" align="center"></div><br>
	
	<div class="tupian" align="center"></div>
	
	<div style="display: none" class="deltupian" align="center"></div>
</form>