//加载菜单
$(document).ready(function(){
	var lng  = $.trim($('#language').combobox('getValue'));//获取语言的值
	$.ajax({
		    type: "post",
		    //发送请求的路径
		    url:'menuAction.do',
		    //返回数据类型
		    dataType : "json",
		    data : {
				"menuVO.lng" : lng,
				//"menuVO.pmid" : pmid,
				//"menuVO.isdel" : "0"
			},
		    success: function(data){ 
	        	$('#menutree').tree('loadData',data.treeList);       
	        }
		});
	$('#language').combobox({		
		onSelect: function(){		
		 lng = $.trim($('#language').combobox('getValue'));
		  select(lng);
			}
	});
});
//查询菜单方法(选择 下拉列表的语言时用)
function select(lng){
	$.ajax({
	    type: "post",
	    //发送请求的路径
	    url:'menuAction.do',
	    //返回数据类型
	    dataType : "json",
	    	data : {
				"menuVO.lng" : lng,
				//"menuVO.pmid" : pmid,
				"menuVO.isdel" : "0"
			},
		    success: function(data){        	
	        	$('#menutree').tree('loadData',data.treeList);       
	        }
	    
	});
}

//新增菜单
function insert(){
	//清空表单信息
$("#insertDialog").form('clear');
$("#newisdel").prop('checked', false);
	//选中某行数据
var treeList = $('#menutree').tree('getSelected');
if(treeList !=null){
	//弹出一个编辑框
	 $('#insertDialog').dialog({
		width : 500,
		height : 300,
		closed : false,
		cache : false,
		modal : true,
		iconCls : 'icon-edit'
	 });
} else{
	$.messager.alert("提示", "请先选择一条数据！");
}
}
//修改菜单
function update(){
	//清空表单信息
	$("#menuDialog").form('clear');	
	//点击菜单中的某一行获取菜单中的数据的值
	var treeList = $('#menutree').tree('getSelected');
	if(treeList !=null){
		if(treeList.id!=-1){
	$('#menuDialog').dialog({
		width : 500,
		height : 300,
		closed : false,
		cache : false,
		modal : true,
		iconCls : 'icon-edit'
	 });
		//提交表单数据
			$.ajax({
			    type: "post",
			    //url: "updatemenu.do",
			    dataType : "json",
			    data : {},
			});						 			
			if(treeList.isdel==0){
			 $("#isdel").prop("checked",true);
			}
			$('#text').val(treeList.text);
			$('#id').val(treeList.id);
			$('#url').val(treeList.url);
			$('#describe').val(treeList.describe);
		}else{
			$.messager.alert("提示", "根节点无法修改");
		}			
}else{
	$.messager.alert("提示", "请先选择一条数据！");
}
}

//删除菜单
function deletemenu(){
	//选中菜单的节点数
	var treeList = $('#menutree').tree('getChecked');
	//判断有没有选中行数	
	if(treeList.length > 0){
		$.messager.confirm('确认','确认要删除吗？',function(r){
			//可以选中多条数据删除
			if (r == true) {
				var num = "";
				for (var i = 0; i < treeList.length; i++) {
					//判断是否为父节点
				if($('#menutree').tree("isLeaf",treeList[i].target) == true){
						num += "|";
						num += treeList[i].id;
					}					
			}
			$.ajax({
				type: "post",
			    //发送请求的路径
			    url:'deletemenu.do',
			    //返回数据类型
			    dataType : "json",
			    data : {
					"treeVO.params" : num
				},
				success:  function(data){
					//返回值为0时的操作
					if (data.result == 0) {
						$.messager.alert("提示", "操作成功！");
						lng = $.trim($('#language').combobox('getValue'));
						//执行查询操作
						select(lng);
					}else {
						//返回值不为0时的操作
						$.messager.alert("提示", "系统异常，请联系管理员！");
					}
				}
			});
			}
		});
		
	}else {
		$.messager.alert("提示", "请先在复选框中勾选行！");
	}
}
 //保存按钮 （修改时用的）
function save(){
	//将窗口中值传给后台（修改的name值）
	var text = $.trim($('#text').val());
	var id = $.trim($('#id').val());
	var url = $.trim($('#url').val());
	var describe = $.trim($('#describe').val());
	 if($("#isdel").prop("checked")==true || $("#isdel").prop("checked") == 'checked' ){
		isdel = 0;
	       }else{
	    	   isdel=1;
	       }
	$.ajax({
		type : "post",
		url : "updatemenu.do",
		dataType : "json",
		data : {
			//action：  参数值
			"treeVO.text" : text,
			"treeVO.id" : id,
			"treeVO.url" : url,
			"treeVO.describe" : describe,
			"treeVO.isdel" : isdel,
		},
		success:function(data) {
			if (data.result == 1) {
				$.messager.alert("提示", "操作成功！");
				$("#menuDialog").dialog("close");
				//执行查询操作
				lng = $.trim($('#language').combobox('getValue'));
				select(lng);
			}
			else {
				$.messager.alert("提示", "系统异常，请联系管理员！");
			}
		}
});
}   
//保存按钮（新增时用的）	 
function insertsave(){
	//查询是否启用该菜单
	 if($("#newisdel").prop("checked")==true || $("#newisdel").prop("checked") == 'checked' ){
		isdel = 0;
	       }else{
	    	   isdel = 1; 
	       }
	 var mid = $.trim($('#mid').val());//获取mid	
	 alert(mid);
	 var pmid = "";
	 if($('#menutree').tree('getSelected').id !=-1){
		  pmid = $('#menutree').tree('getSelected').id;//根据父节点获取pmid的值		
	 }	
	 var lng  = $.trim($('#language').combobox('getValue'));//获取语言的值
	 var name = $.trim($('#name').val());//获取新增窗口的文本name	
	 var url = $.trim($('#newurl').val());//获取新增窗口的文本url
	 var describe = $.trim($('#newdescribe').val());//获取新增窗口的文本descripe	
	 var node = $('#menutree').tree('getSelected');
	  //初始菜单等级
	 var menulevel= -1 ;
	  //查询当前选中的菜单等级
	 for(var i = 0; i < 999; i++){
		node = $('#menutree').tree("getParent",node.target);
		if(node == null){
			break;
		}
		menulevel = i;
	   };	
	  //查询当前选中的行的子菜单等级(获取新建的子菜单等级)
	   menulevel += 1;
	  //设置菜单类型
	  var menutype = 0;	 	 
	$.ajax({
		type : "post",
		url : "save1menu.do",
		dataType : "json",
		data : {
			//action：  参数值
			"menuVO.mid": mid,
			"menuVO.pmid": pmid,
			"menuVO.lng" : lng,
			"menuVO.isdel": isdel,
			"menuVO.name": name,
			"menuVO.url": url,
			"menuVO.describe": describe,
			"menuVO.menulevel": menulevel,
			"menuVO.menutype": menutype
		},
		success : function(data) {
			if (data.result == 1) {
				$.messager.alert("提示", "操作成功！");
				//新增的子菜单
				$("#insertDialog").dialog("close");
			lng = $.trim($('#language').combobox('getValue'));
				select(lng);
			}
			else {
				$.messager.alert("提示", "系统异常，请联系管理员！");
			}
		}
	});
}	
   



