//图片轮换界面的查询
function selectAdvert(){
	var lng  = $.trim($('#language').combobox('getValue'));//获取语言的值
	//加載下拉列表
	$.ajax({
		type : "post",
		url : "selectUrlAction.do",
		dataType : "json",
		data:{
			"menuVO.lng" : lng
		},
		success: function(comboxData){	
			//var lng  = $.trim($('#language').combobox('getValue'));//获取语言的值
			$.ajax({
				type : "post",
				url : "selectAdvertAction.do",
				dataType : "json",
				data:{
					"advertVO.lng" : lng
				},
				success : function(data) {					
					// 新建一个字符串
					var dd = '';
					// 新建一个list接收action中的advertList
					var list = data.advertList;					
					// 循环输出图片
					for (var i = 0; i < list.length; i++) {
						dd += '<div class="new1">'
						   +'<span class="pic"  value="'+list[i].itid+'"  style = "float: left; border:5px solid white;">'
						   +'<input type="text" name="advertList.itid" value='+list[i].itid+' style="visibility:hidden"><br/>'
						   +'<img  src="'+ path + list[i].url + list[i].filename+ '" alt="" width="240" height="150" ><br/>';
						   //新建下拉列表
						dd+='<label>链接地址：</label>'
							+'<select class="easyui-combobox" style="width:120px" name="menuList.name"></select>&nbsp;&nbsp;&nbsp;';
						   //新建删除按钮
						dd+='<input type="button" class="delete1" value="删除"/></span></div>';					
					}
				// 将字符串拼接起来
				$(".main_image").html(dd);
				//下拉列表加载内容
				var menulist = comboxData.menuList;
			          //判断选择何种语言时用何种提示
				if(lng=="cn"){
				$(".pic").children(".easyui-combobox").append("<option id='all'>请选择链接地址</option>");}
				else{
					$(".pic").children(".easyui-combobox").append("<option id='all'>please choose address</option>");
				}
				for(var i = 0; i < menulist.length; i++){
					$(".pic").children(".easyui-combobox").append("<option id='"+menulist[i].url+"'>"+menulist[i].name+"</option>");				
				}
				
				//加载下拉列表的默认显示项
				  for (var i = 0; i < list.length; i++) {
					  if(list[i].transferurl != null){
							$(".pic").eq(i).children("select").children("option[id='"+list[i].transferurl+"']").attr("selected",true);				
						}else{
							$(".pic").eq(i).children("select").children("option[id='all']").attr("selected",true);
						}
				}				  				  				  				  				  				  
				//新增图片				
				$("#add").click(function(){
					$(".main_image").append(
							'<div class="new1"><span class="pic" style = "float: left; border:5px solid black">'
							+'<input type="file" name="file" class="file" width="240" height="150"/></br>'
							+'<label>链接地址：</label><select  class="easyui-combobox" style="width:100px" name="newmenuList.name"/> &nbsp;&nbsp;&nbsp;'
							+'<input type="button" class="delete1" value="删除"/></span></div>');	
					//加载下拉列表
					var menulist = comboxData.menuList;
					if(lng=="cn"){
						$(".pic").children(".easyui-combobox").append("<option id='all'>请选择链接地址</option>");}
						else{
							$(".pic").children(".easyui-combobox").append("<option id='all'>please choose address</option>");
						}
					for(var i = 0; i < menulist.length; i++){
					$(".pic").children(".easyui-combobox").append("<option id='"+menulist[i].url+"'>"+menulist[i].name+"</option>");
					}	
					$(".delete1").click(function() {
						$(this).parent().hide();
					});
				});
			
				//删除图片
					$(".delete1").click(function() {
						$(this).parent().hide();
					});		
			
				}
		});								
		}		
	});
}
//设置是否滚动，以及滚动时间(属于修改)
function isroll(){
	//加载dialog
	$('#rollDialog').dialog({
		width : 500,
		height : 150,
		closed : false,
		cache : false,
		modal : true,
		iconCls : 'icon-edit'
	 });
}
//保存滚动属性
function savetime(){
	var time = $.trim($('#time').val());
	var mid = $('#menutree').tree('getSelected').id;
	var istime = 0 ;
	 if($("#istime").attr("checked")==true || $("#istime").attr("checked") == 'checked' ){
		istime = 1;
	      }
		$.ajax({
			type : "post",
			url : "savetime.do",
			dataType : "json",
			data : {
				//action：  参数值
				"documentTypeVO.time":time,
				"documentTypeVO.istime":istime,
				"documentTypeVO.mid":mid
			},
			success:function(data) {
				if (data.result == 1) {
					$.messager.alert("提示", "操作成功！");
					$("#rollDialog").dialog("close");
				}
				else {
					$.messager.alert("提示", "系统异常，请联系管理员！");
				}
			}
	});
	
}
	
//图片轮换界面的新增、删除的保存
function advertsave(){
var str="";
$(".pic:hidden").each(function(){	
	str+=$(this).attr("value")+"|";
	});	
	  $(".main_image").append('<div class="delete1"><input type="text" name="adidstr" value="'+str+'" style="visibility:hidden"></div>');	
	  $("#form1").ajaxSubmit({
   	type : "post",
		url : "saveAdvert.do",
		data:$(this).serialize(),
		complete:function(){
			var tabTitle = $('#r_content').tabs('getSelected').panel('options').title;
			var currentTab =$('#r_content').tabs('getTab',tabTitle);
			currentTab.panel('refresh');
		}
});
	return false;
}

//文字快捷链接的查询
function docTransferselect(){
	var lng  = $.trim($('#language').combobox('getValue'));//获取语言的值
	$.ajax({
		type : "post",
		url : "selectUrlAction.do",
		dataType : "json",
		data:{
			"menuVO.lng" : lng
		},
		success: function(comboxData){	
	$.ajax({
		type : "post",
		//指向action的路径
		url : "selectDoctransfer.do",
		//输出类型为json类型
		dataType : "json",
		data:{
			"docTransferVO.lng":lng
		},
		success : function(data) {
			// 新建一个字符串
			var dd = '';
			// 新建一个list接收action中的list
			var list = data.docTransferList;
			//循环输出标题
			for (var i = 0; i < list.length; i++) {
				if(i==0){
				dd += '<div class="title"><label>标题：<textarea rows="1" cols="13" class="titl" name="documentTypeVO.title">' 
				   + list[i].title + '</textarea></label>&nbsp;&nbsp;'
				   +'<label>链接地址：</label><select  class="easyui-combobox"  name="menuVO.name" style="width:90px"/></div>';
				} 
				dd += '<div class="tent" ><div class="cont" value="'+list[i].id+'"><label>内容：<textarea  rows="3" cols="25" class="content" name="docTransferList.content" >' 
				   + list[i].content + '</textarea></label><input type="button" class="delcon" value="删除"/></div></div>';
			}
			//循环输出内容
//			for(var i = 0; i < list.length; i++){
//				if( list[i].content != null){
//					dd += '<div class="tent" ><div class="cont" value="'+list[i].id+'"><label>内容：<textarea  rows="3" cols="25" class="content" name="docTransferList.content" >' 
//					   + list[i].content + '</textarea></label><input type="button" class="delcon" value="删除"/></div></div>';
//					}			
//			}
			$(".Introduction").html(dd);
			
			//加载下拉列表项
			var menulist = comboxData.menuList;
			if(lng=="cn"){
				    $(".title").children(".easyui-combobox").append("<option id='all'>请选择链接地址</option>");}
			else{
					$(".title").children(".easyui-combobox").append("<option id='all'>please choose address</option>");
				}						
			for(var i = 0; i < menulist.length; i++){
			$(".title").children(".easyui-combobox").append("<option id='"+menulist[i].url+"'>"+menulist[i].name+"</option>");
			}
			//加载下拉列表的默认显示(默认第一条数据的transferurl)
			if(list[0].transferurl !=null){
			$(".title").eq(0).children("select").children("option[id='"+list[0].transferurl+"']").attr("selected",true);
			}else{
		         $(".title").eq(0).children("select").children("option[id='all']").attr("selected",true);
				}		
								
			//页面增加内容项
			var a = $(".tent:first .content").val();
			$(".tent:first .content").val("");
			var content = $(".tent:first").html();
			$(".tent:first .content").val(a);
						
			$("#add2").click(function() {
				$(".Introduction").append(content);
				//删除
				$(".delcon").click(function() {
					$(this).parent().remove();
				});
			});						
			//页面内容删除
			 $(".delcon").click(function() {
					$(this).parent().remove();
				}); 
		}
	});
		}
		});
	
}
//文字快捷链接的新增、删除的保存方法
function docTransfersave(){	
//	var str="";
//	$(".cont:hidden").each(function(){		
//		str+=$(this).attr("value")+"|";
//		});	
//		  $(".tent:last").append('<div class="delcon"><input type="text" name="adidstr" value="'+str+'"></div>');
  $("#form2").ajaxSubmit({
	   	type : "post",
		url : "saveDoctransfer.do",
		data:$(this).serialize(),
		complete:function(){
				var tabTitle = $('#r_content').tabs('getSelected').panel('options').title;
				var currentTab =$('#r_content').tabs('getTab',tabTitle);
				currentTab.panel('refresh');
			}
	});
		return false;

}
//图片快捷链接的查询
function imgTransferselect(){
	var lng  = $.trim($('#language').combobox('getValue'));//获取语言的值
	//加載下拉列表
	$.ajax({
		type : "post",
		url : "selectUrlAction.do",
		dataType : "json",
		data:{
			"menuVO.lng" : lng
		},
		success: function(comboxData){	
	$.ajax({
		type : "post",
		// 路径指向Action
		url : "selectImgTransfer.do",
		dataType : "json",
		data:{
			"imgTransferVO.lng":lng
		},
		success : function(data) {
			var dd = '';
			//新建一个变量接收action的list值
			var list = data.imgTransferList;
			//循环输出
			for (var i = 0; i < list.length; i++) {
				dd+='<div class="new2"><span class="pic1" value="'+list[i].itid+'"  style = "float: left; border:10px solid white;">'
				    +'<input type="text" name="imgTransferList.itid" value='+list[i].itid+' style="visibility:hidden"><br/>';
				dd += '<img src="' + path+ list[i].url + list[i].filename + '" alt="" width="230" height="150"/><br/> ';
				dd+='<label>链接地址：</label><select  class="easyui-combobox" name="menuList.name" style="width:120px"/> &nbsp;&nbsp;&nbsp;';
				dd+='<input type="button" class="delete2" value="删除"/></span></div>';
			}
			$(".box").html(dd);
			
			//加载下拉列表
			var menulist = comboxData.menuList;
			 if(lng=="cn"){
			      $(".pic1").children(".easyui-combobox").append("<option id='all'>请选择链接地址</option>");}
		     else{
				  $(".pic1").children(".easyui-combobox").append("<option id='all'>please choose address</option>");
			}
			for(var i = 0; i < menulist.length; i++){
				$(".pic1").children(".easyui-combobox").append("<option id='"+menulist[i].url+"'>"+menulist[i].name+"</option>");
			}
			
			//获取当前的下拉列表的值	
			for (var i = 0; i < list.length; i++) {	
				if(list[i].transferurl !=null){
				$(".pic1").eq(i).children("select").children("option[id='"+list[i].transferurl+"']").attr("selected",true);				
			}else{
				$(".pic1").eq(i).children("select").children("option[id='all']").attr("selected",true);
			}
				}
						
			//点击新增图片
			$("#add1").click(function(){
				$(".box").append('<div class="new2">' 
						        +'<span class="pic1" style = "float: left; border:5px solid white">'
						        +'<input type="file" name="file" class="file"/><br/>'
						        +'<label>链接地址：</label><select class="easyui-combobox" name="newmenuList.name" style="width:100px"/> &nbsp;&nbsp;&nbsp;'
						        +'<input type="button" class="delete2" value="删除"/></span></div>');
				
				//加载下拉列表
				var menulist = comboxData.menuList;
				if(lng=="cn"){
				      $(".pic1").children(".easyui-combobox").append("<option id='all'>请选择链接地址</option>");}
			     else{
					  $(".pic1").children(".easyui-combobox").append("<option id='all'>please choose address</option>");
				}
				for(var i = 0; i < menulist.length; i++){				
					$(".pic1").children(".easyui-combobox").append("<option id='"+menulist[i].url+"'>"+menulist[i].name+"</option>");																		
				  }
				
				$(".delete2").click(function() {
					$(this).parent().hide();
				});
			});
			//点击删除图片
				$(".delete2").click(function() {
					$(this).parent().hide();
				});
		}
	});
}
	});
}


//图片快捷链接的新增、删除的保存方法
function imgTransfersave(){
	var str="";
	$(".pic1:hidden").each(function(){	
		str+=$(this).attr("value")+"|";
		});	
		  $(".box").append('<div class="delete2"><input type="text" name="adidstr" value="'+str+'"></div>');	
		  $("#form3").ajaxSubmit({
	   	type : "post",
			url : "saveImgtransfer.do",
			data:$(this).serialize(),
			complete:function(){
				var tabTitle = $('#r_content').tabs('getSelected').panel('options').title;
				var currentTab =$('#r_content').tabs('getTab',tabTitle);
				currentTab.panel('refresh');
			}
	});
		return false;
}
