function loadFoot(lng){
	if($(".foot").length != 0){
		$(".foot").remove();
	}
	if(lng == 'cn'){
		$("body").append('<div class="foot">地址：上海市徐汇区石龙路345弄23-27号易园创意园区B座421&nbsp;&nbsp;&nbsp;&nbsp;电话：021-34060135&nbsp;&nbsp;&nbsp;&nbsp;传真：021-34060135&nbsp;&nbsp;&nbsp;&nbsp;邮编：200232&nbsp;&nbsp;&nbsp;&nbsp;沪ICP备14004611号-1</div>');
	}else{
		$("body").append('<div class="foot">Address：Building B-421,#23-27 Lane 345,Shilong Road(E-Manor),Xuhui district,Shanghai &nbsp;&nbsp;&nbsp;&nbsp;Tel：021-34060135&nbsp;&nbsp;&nbsp;&nbsp;Fax：021-34060135&nbsp;&nbsp;&nbsp;&nbsp;Postcode：200232</div>');
	}
	
}


/**
 * 加载 标题菜单方法
 * 
 * @param lng
 * @param isdel
 * @param pmid
 * @param selected
 */
function loadMenu(lng, isdel, pmid) {
	$.ajax({
		type : "post",
		url : "containedMenuAction.do",
		dataType : "json",
		data : {
			"menuVO.lng" : lng,
			"menuVO.pmid" : pmid,
			"menuVO.isdel" : isdel
		},
		success : function(data) {
			var htmlstr = '';
			var menuList = data.menuList;
			htmlstr += "<ul>";
			for (var i = 0; i < menuList.length; i++) {
				var menu = menuList[i];
				// 标题中间的间隔图片 第一次不用添加 其它都需要
				if (i > 0) {
					htmlstr += '<li class="s"><img src="' + path+ '/img/s.jpg" width="1" height="12"  /></li>';
				}
				// 是否出现选中效果
				htmlstr += '<li class="menu" ><a style="text-transform:uppercase;cursor:pointer;"  current="'+i+'" pmid=' + menu.pmid + ' mid="' + menu.mid+ '" class="oneDep disB" style="cursor:pointer">'+ menu.name + '</a>';
				// 二级菜单
				var secondMenuList = menu.menuList;
				if (secondMenuList != null && secondMenuList.length > 0) {
					htmlstr += "<ul>";
					for (var j = 0; j < secondMenuList.length; j++) {
						var secondMenu = secondMenuList[j];
						htmlstr += '<li><a style="text-transform:uppercase;cursor:pointer;" pmid="' + secondMenu.pmid + '" pmname="' + menu.name + '" mid="'+ secondMenu.mid + '" class="disB off" style="cursor:pointer">'+ secondMenu.name + '</a></li>';
					}
					htmlstr += "</ul>";
				}
				htmlstr += '</li>';
			}
			htmlstr += "</ul>";
			
			$("#main_nav").html(htmlstr);
			// 所有菜单元素新增完成以后--添加悬浮效果
			//$('#main_nav').allenMenu();
			$("#main_nav").allenMenu();    			         			
			// 一级菜单添加点击效果
			$('#main_nav').children('ul').children('li').children('a')
					.click(
							function() {
								$('#main_nav').children('ul').children('.menu').children('a').removeClass("hover");
								$(this).addClass("hover");
								o_mid = $(this).attr("mid");
								o_pmid = $(this).attr("pmid");
								o_current = $(this).attr("current");
								//model(o_lng, o_mid, $(this).text(), undefined,"", o_isdel);
								clearInterval(timer);
								loadFirstModelContent(o_lng, o_mid, $(this).text(), undefined,"", o_isdel);
							}
					);
			
			
			
			
			// 二级菜单添加点击效果
			$('#main_nav').children('ul').children('li').children('ul')
					.children('li').children('a').click(
							function() {
								$('#main_nav').children('ul').children('li').children('a').removeClass("hover");
								o_current = $(this).parent().parent().parent().children('a').addClass("hover").attr("current");
								o_mid = $(this).attr("mid");
								o_pmid = $(this).attr("pmid");
								//model(o_lng, o_mid, $(this).text(), o_pmid, $(this).attr("pmname"), o_isdel);
								//alert("sorry 当前页面正在维护！！");
								clearInterval(timer);
								loadFirstModelContent(o_lng, o_mid, $(this).text(), o_pmid,$(this).attr("pmname"), o_isdel);
							}

					);
			if(o_current_mid == "null" || o_current_mid == null || o_current_mid == undefined || o_current_mid == 'undefined'){
				$('#main_nav').children('ul').children('.menu:eq('+o_current+')').children('a').click();
			}else{
				if(!flag){
					$('#main_nav').children('ul').children('li').children('a').each(function(){
						if($(this).attr("mid") == o_current_mid){
							$(this).click();
							flag = true;
							return false;
						}
					});
				}
				if(!flag){
					$('#main_nav').children('ul').children('li').children('ul').children('li').children('a').each(function(){
						if($(this).attr("mid") == o_current_mid){
							flag = true;
							$(this).click();
							return false;
						}
					});
				}
				if(!flag){
					$('#main_nav').children('ul').children('li').children('a').each(function(){
						if($(this).attr("mid") == o_current_pmid){
							$(this).click();
							//flag = true;
							return false;
						}
					});
				}
				if(!flag){
					$('#main_nav').children('ul').children('li').children('ul').children('li').children('a').each(function(){
						if($(this).attr("mid") == o_current_pmid){
							//flag = true;
							$(this).click();
							return false;
						}
					});
				}
			}
		}
	});
}



function loadFirstModelContent(lng, mid, mname, pmid, pmname, isdel){
	$.ajax({
		type : "post",
		url : "clientDocTypeAction.do",
		dataType : "json",
		data : {
			"menuVO.lng" : lng,
			"menuVO.pmid" : pmid,
			"menuVO.isdel" : isdel,
			"menuVO.mid" : mid
		},
		success : function(data) {
			var doctype = data.documentTypeVO;
			var modelid = '-1';
			if (doctype != null) {
				modelid = doctype.modelid;
			} else {
				alert("sorry 当前页面正在维护！！");
				return;
			}
			
			if(modelid == '1'){
				if ($("body").children("#r_con4").length > 0) {
					$("body").children("#r_con4").remove();
				}
				if ($("body").children("#r_con1").length == 0) {
					$("body").children("div:first").after(modelIndex());
				}
				modelOne(lng, mid, mname, pmid, pmname, isdel, modelid,data);
			}else{
				if ($("body").children("#r_con1").length > 0) {
					$("body").children("#r_con1").remove();
					$("body").children("#r_con2").remove();
					$("body").children("#r_con3").remove();
				}
				if ($("body").children("#r_con4").length == 0) {
					$("body").children("div:first").after(modelBasis());
				}
				if(pmid == undefined){
					$("body").children("#r_con4").children(".Left").children("h3").html(mname);
					$("body").children("#r_con4").children(".Right").children("h3").children("b").html(mname);
					//$("body").children("#r_con4").children(".Right").children("h3").children("b").css("text-align","justify");
				}else{
					$("body").children("#r_con4").children(".Left").children("h3").html(pmname);
					//$("body").children("#r_con4").children(".Right").children("h3").children("b").html(pmname);
					//$("body").children("#r_con4").children(".Right").children("h3").children("b").css("text-align","justify");
				}
				modelTwo(lng, mid, mname, pmid, pmname, isdel, modelid,data);
			}
		}
	});
}









/**
 * 模板---通用模板
 * @param lng
 * @param mid
 * @param mname
 * @param pmid
 * @param pmname
 * @param isdel
 * @param modelid
 * @param data
 */
function modelTwo(lng, mid, mname, pmid, pmname, isdel, modelid,data){
	var menuList = data.menuList;
	var htmlStr = '';
	for (var i = 0; i < menuList.length; i++) {
		var menu = menuList[i];
		//二级菜单
		htmlStr += '<li style="cursor:pointer"><a style="text-transform:uppercase;cursor:pointer;"  pmid=' + menu.pmid + ' mid=' + menu.mid + '>' + menu.name + '</a></li>';
		var subMenuList = menu.menuList;
		if (subMenuList.length > 0) {
			for (var j = 0; j < subMenuList.length; j++) {
				  var subMenu = subMenuList[j];
				  //三级菜单
				  htmlStr += '<li style="cursor:pointer;"><a style="text-transform:capitalize;cursor:pointer;font-size:12px;line-height:25px;height:25px;text-indent:3em;" id="b" pmname="'+menu.name+'" pmid=' + menu.mid + ' mid=' + subMenu.mid + '>' + subMenu.name + '</a><span></span></li>';
			}
		}
	}
	
	
	if ($("body").children("#r_con4").length != 0) {
		$("body").children("#r_con4").children(".Right").html("<h3><span style='text-transform:capitalize;cursor:pointer;'>"+mname+"</span><b></b> </h3>");
		
		if(htmlStr == ''){
			htmlStr += '<li style="cursor:pointer;text-transform:uppercase;"><a mid=' + mid + '>' + mname+ '</a></li>';
		}
		
		 $("body").children("#r_con4").children(".Left").children("ul").html(htmlStr);
		 //$("body").children("#r_con4").children(".Left").children("ul").css("text-align","justify");
		 
		 var childrenUl = $("body").children("#r_con4").children(".Left").children("ul").children("li").children("span");
		 if(childrenUl.length == 0){
			 $("body").children("#r_con4").children(".Left").children("ul").children("li").children("a").click( function() {
				 $("body").children("#r_con4").children(".Left").children("ul").children("li").children("a").removeClass("hover");
				 $(this).addClass("hover");
				 $("#r_con4").children(".Right").children("h3").children("span").html($(this).text());
				 leftFirstMenuClick(lng, $(this).attr("mid"), "", "", "", isdel, modelid,data);
			 });
			 if(pmid == undefined){		
				 $("body").children("#r_con4").children(".Left").children("ul").children("li:first").children("a").click();
			 }else{
				 $("body").children("#r_con4").children(".Left").children("ul").children("li").children("a").each(function(){
					 if($(this).attr("mid") == mid){
						 $(this).click();
					 }
				 });
			 }			 
			 if(!flag){
				 $("body").children("#r_con4").children(".Left").children("ul").children("li").children("a").each(function(){
					 if($(this).attr("mid") == o_current_mid){
						 flag = true;
						 $(this).click();
						 return false;
					 }
				 });
				 
			 }
			 
		 }else{
			 childrenUl.each(function(){
				 $(this).prev().click(function(){
					 $("body").children("#r_con4").children(".Left").children("ul").children("li").children("a").removeClass("hover");
					 $(this).addClass("hover");
					 $("#r_con4").children(".Right").children("h3").children("span").html($(this).text());
					 leftFirstMenuClick(lng, $(this).attr("mid"), "", "", "", isdel, modelid,data);
				 });
			 });
			 
			 if(pmid == undefined){
				 $("body").children("#r_con4").children(".Left").children("ul").children("li").children("span:first").prev().click();
				 
			 }else{
				 childrenUl.each(function(){
					 if($(this).prev().attr("pmid") == mid){
						 $(this).prev().click();
						 return false;
					 }
					 
				 });
			 }
			 if(!flag){
				 childrenUl.each(function(){
					// alert($(this).prev().attr("mid") + "   " + o_current_mid);
					 if($(this).prev().attr("mid") == o_current_mid){
						 flag = true;
						 $(this).prev().click();
						 return false;
					 }
					 
				 });
			 }
		 }
		 
		 
		 
	}

	
}
/**
 * 左边只有一级菜单的点击事件
 * @param lng
 * @param mid
 * @param mname
 * @param pmid
 * @param pmname
 * @param isdel
 * @param modelid
 * @param data
 */
function leftFirstMenuClick(lng, mid, mname, pmid, pmname, isdel, modelid,data){
	$.ajax({
		type : "post",
		url : "clientContentTypeAction.do",
		dataType : "json",
		data : {
			"menuVO.lng" : lng,
			"menuVO.pmid" : pmid,
			"menuVO.isdel" : isdel,
			"menuVO.mid" : mid
		},
		success : function(data) {
			var doctype = data.documentTypeVO;
			var modelid = '-1';
			if (doctype != null) {
				modelid = doctype.modelid;
			} else {
				alert("sorry 当前页面正在维护！！");
				return;
			}
			var conStr=loadContent(modelid,data); 
			var htmlStrh3 = $("body").children("#r_con4").children(".Right").children("h3").html();
			$("body").children("#r_con4").children(".Right").html("<h3></h3>");
			$("body").children("#r_con4").children(".Right").children("h3").html(htmlStrh3);
			$("body").children("#r_con4").children(".Right").append(conStr);
		}
		
	});
}




function loadContent(modelid,data){
	var documentList = data.documentList;
	var albumList = data.albumList;	
	var contactList = data.contactList;
	var customList = data.customList;
	var htmlStr = '';
	if(modelid == 4){
		for (var i = 0; i < documentList.length; i++) {
			 var doc = documentList[i];
			 htmlStr += '<p style="text-align:justify">';
			 if(doc.title != null && doc.title != ""){
				 htmlStr += doc.title;
				 htmlStr += '<br/>';
			 }
			 htmlStr += doc.content;
			 htmlStr += '</p><br/>';
		}
		htmlStr += '<p>'; 
		for (var i = 0; i < albumList.length; i++) {
			var ablum = albumList[i];
			htmlStr += '<img src="' + path + ablum.imgurl + ablum.imgfilename + '" alt="' + ablum.imgalt + '" width="232" height="167" />&nbsp;'; 
		}
		htmlStr += '</p>';
	}else if(modelid == 5){
		for (var i = 0; i < documentList.length; i++) {
			 var doc = documentList[i];
			 htmlStr += '<p style="text-align:justify"><strong>' + doc.title + '：</strong>' + doc.content + ';</p>';
		}
		
		htmlStr += '<p>'; 
		for (var i = 0; i < albumList.length; i++) {
			var ablum = albumList[i];
			htmlStr += '<img src="' + path + ablum.imgurl + ablum.imgfilename + '" alt="' + ablum.imgalt + '" width="232" height="167" />&nbsp;'; 
		}
		htmlStr += '</p>';
		
	}else if(modelid == 6){
		htmlStr += '<div class="Team">';
		for (var i = 0; i < documentList.length; i++) {
			var doc = documentList[i];
			htmlStr += '<dl style="text-align:justify">';
			htmlStr += '<a><img src="' + path + doc.imgurl + doc.imgfilename+ '" width="200" height="130" /></a>';
			htmlStr += '<dt><a>' + doc.title + '</a></dt>';
			htmlStr += '<dd><p>' + doc.content + '</p></dd>';
			htmlStr += '</dl>';
		}
		htmlStr += '</div>';
	}else if(modelid == 7){
		htmlStr += '<div class="Partner">';
		htmlStr += ' <ul>';
		for (var i = 0; i < documentList.length; i++) {
			var doc = documentList[i];
			htmlStr += '<li style="text-align:justify">';
			htmlStr += '<a><img src="' + path + doc.imgurl+ doc.imgfilename + '" alt="" width="132" height="53" /></a>';
			htmlStr += '&nbsp;&nbsp;<a>' + doc.title + '</a>';
			htmlStr += '</li>';
		}
		htmlStr += ' </ul>';
		htmlStr += '</div>';
	}if(modelid == 8){
		htmlStr += '<div style="text-align:justify">';
		for (var i = 0; i < documentList.length; i++) {
			var doc = documentList[i];
				if(doc.title != null && doc.title != 'null'){
					htmlStr += '<p style="font-size: 16px;line-height: 35px;margin-top:19px">' + doc.title + '</p>';
				}
				if(doc.content != null && doc.content != 'null'){
					htmlStr += '<p style="font-size:14px;">&middot;' + doc.content + '</p>';
				}
			
		}
		//alert(documentList.length);
		if(albumList.length > 0){
			for (var i = 0; i < albumList.length; i++) {
				var ablum = albumList[i];
				htmlStr += '<p>';
				htmlStr += '<img src="' + path + ablum.imgurl+ ablum.imgfilename + '" alt="图片"   width="663px" height="450px" />&nbsp;';
				htmlStr += '</p>';
			}
		}
		
		htmlStr += '</div>';
	}if(modelid == 0){
		htmlStr += '<div style="text-align:justify">';
		for (var i = 0; i < documentList.length; i++) {
			var doc = documentList[i];
			if(doc.title != null && doc.title != 'null'){
				htmlStr += '<p style="font-size: 16px;color:#4C7CB7;line-height: 35px;margin-top:19px">' + doc.title + '</p>';
			}
			if(doc.content != null && doc.content != 'null'){
				htmlStr += '<p>' + doc.content + '</p>';
			}
		}
		if(albumList.length > 0){
			for (var i = 0; i < albumList.length; i++) {
				var ablum = albumList[i];
				htmlStr += '<p>';
				htmlStr += '<img src="' + path + ablum.imgurl+ ablum.imgfilename + '" alt="图片"   width="663px" height="450px" />&nbsp;';
				htmlStr += '</p>';
			}
		}
		
		htmlStr += '</div>';
	}else if(modelid == 2){
		var  contacttype = "";
		htmlStr += '<div class="Contact">';
		htmlStr += '<div class="tp">';
		for (var i = 0; i < contactList.length; i++) {
			var contact = contactList[i];
			if(contacttype != contact.type){
				htmlStr += '<h3>联系方式</h3>';
			}
			contacttype = contact.type;
			if(contact.imgrul == null || contact.imgrul == ""){
				htmlStr += '<img src="'+path+contact.imgrul+contact.filename+'" alt="" width="14" height="17" />';
			}
			htmlStr += contact.name+'：'+contact.describe+'<br /> ';
		}
		htmlStr += '</div>';
		htmlStr += '</div>';
	}else if(modelid == 3){
		for (var i = 0; i < customList.length; i++) {
			var custom = customList[i];
			htmlStr += custom.content;
		}
	}
	return htmlStr;
}



/**
 * 模板1--林康首页模板
 * @param lng
 * @param mid
 * @param mname
 * @param pmid
 * @param pmname
 * @param isdel
 * @param modelid
 * @param data
 */
function modelOne(lng, mid, mname, pmid, pmname, isdel, modelid,data){
	 if ($("body").children("#r_con1").length != 0) {
		 var  parentHtml = '<div class="main_image"><a href="javascript:;" id="btn_prev"></a> <a href="javascript:;"id="btn_next"></a></div>';
		 $("#r_con1  .main_image").remove();
		 $("#r_con1").append(parentHtml);
		 var imgTransferList = data.imgTransferList;
		 var docTransferList = data.docTransferList;
		 var advertList = data.advertList;
		 var doctype = data.documentTypeVO;
		 // =====1.首页广告模块加载
		 var htmlStr = '';
		 var imgStr = '<ul>';
		 for (var i = 0; i < advertList.length; i++) {
			 var advert = advertList[i];
			 htmlStr += "<a>" + (i + 1) + "</a>";
			 imgStr += '<li><a href="' + path + advert.transferurl + '" target="_blank"><span class="img" style="background:url(' + path + advert.url + advert.filename + ')"></span></a></li>';
		 }
		 imgStr += '</ul>';
		 $(".main_visual .flicking_inner").html(htmlStr);
		 $(".main_visual .main_image ul").remove();// 删除原有数据;
		 $(".main_visual .main_image").prepend(imgStr);// 追加现有数据
		 $(".main_visual .main_image .img").css( "background-repeat", 'no-repeat');
		 $(".main_visual .main_image .img").css("background-position", 'center');
		 $(".main_visual .main_image .img").css("background-position", 'top');
		 //===2.给图片添加点击切换效果
		 $(".main_visual").hover(function() {
			 $("#btn_prev,#btn_next").fadeIn();
		 }, function() {
			 $("#btn_prev,#btn_next").fadeOut();
		 });
		 
		 $(".main_image").touchSlider({
			 flexible : true,
			 speed : 1000,
			 btn_prev : $("#btn_prev"),
			 btn_next : $("#btn_next"),
			 paging : $(".flicking_con a"),
			 counter : function(e) {
				 $(".flicking_con a").removeClass("on").eq(e.current - 1).addClass("on");
			 }
		});
		 interval(doctype.time);			//追加定时切换效果
//		$(".main_image").bind("mousedown", function() {
//		});
//		$(".main_image").bind("dragstart", function() {
//		});
//		$(".main_image a").click(function() {
//		});
		
		//====3.首页文字快捷链接数据
		var doc_content = '';
		for (var i = 0; i < docTransferList.length; i++) {
			var docTransfer = docTransferList[i];
			doc_content += "<p  style='cursor:pointer'>" + docTransfer.content + "</p>";
		 }
		$(".Content .Introduction").children("h3").children("span").html(doctype.title);
		//设置首字母大写的样式
		$(".Content .Introduction").children("h3").children("span").css("text-transform","capitalize");
		$(".Content .Introduction").children("h3").children("a").attr("href",path+doctype.transferurl);
		$(".Content .Introduction").children("h3").children("a").css("cursor","pointer");
		$(".Content .Introduction").children("a").html(doc_content);
		//设置分散对齐样式
		$(".Content .Introduction").children("a").css("text-align","justify");
		

		//===4.首页图片快捷链接数据
			var  threeHtml = '<div id="pic_list_2" class="scroll_horizontal"><div class="box"></div></div>';
			$("#r_con2  #pic_list_2").remove();
			$("#r_con2").append(threeHtml);
		var imgTransferHtml = '<ul class="list">';
		for (var i = 0; i < imgTransferList.length; i++) {
		var imgTransfer = imgTransferList[i];
			imgTransferHtml += '<li>';
			imgTransferHtml += '<a class="typelink" href="'+ path + imgTransfer.transferurl + '" target="_blark">';
			imgTransferHtml += '<img src="' + path + imgTransfer.url + imgTransfer.filename+ '" />';imgTransferHtml += '</a></li>';
		}
		imgTransferHtml += '</ul>';
		$("#pic_list_2 .box").html(imgTransferHtml);
		//===5.首首页图片快捷链接切换效果
//		if(doctype.istime != "null" && doctype.istime != null && doctype.istime != 0 ){
//			if(doctype.time == "null" || doctype.time == null){
//				doctype.time = 2000;
//			}
			$("#pic_list_2").cxScroll({
				direction:"left",
				speed:1500,
				step:1,
				time:doctype.time,
				});
		}
	 }


/**
 * 图片滚动事件
 * 
 * @param time
 *            间隔时间
 */
function interval(time) {
		timer = setInterval(function() {
			$("#btn_next").click();
		}, time);
		//鼠标悬停时停止计时器，鼠标拿开时，打开计时器
		$(".main_image").hover(function() {
			clearInterval(timer);
		}, function() {
			timer = setInterval(function() {
				$("#btn_next").click();
			},time);			
		});		
		//触摸事件，触摸时停止定时器，移开时启动定时器
		$(".main_image").bind("touchstart", function() {			
			clearInterval(timer);
		}).bind("touchend", function() {
			timer = setInterval(function() {
				$("#btn_next").click();
			}, time);
		});

	}
/**
 * 模型1使用
 * 
 * @returns {String}
 */
function modelIndex() {
	var htmlStr = '<div id="r_con1" class="main_visual"><div class="flicking_con"><div class="flicking_inner"></div></div></div>';
	htmlStr += '<div id="r_con2" class="Content"><div class="Introduction"><h3><span></span><a href=""  style="cursor:pointer" target="_blark"> <img  style="cursor:pointer" src="'
			+ path
			+ '/img/more.jpg" width="63" height="20" /> </a></h3><a style="cursor:pointer"></a></div>';
	htmlStr += '<center id="r_con3"><div id="pic_list_2" class="scroll_horizontal"><div class="box"></div></div></center>';
	htmlStr += '</div>';
	return htmlStr;
}
/**
 * 模型2使用
 * 
 * @returns {String}
 */
function modelBasis() {
	var htmlStr = '<div id="r_con4" class="Content">';
	htmlStr += '<div class="Left"><h3  style="text-transform:uppercase;"></h3><ul></ul></div>';
	htmlStr += '<div class="Right"><h3 style="text-transform:uppercase;"><span  style="text-transform:uppercase;"></span><b></b> </h3></div>';
	htmlStr += '</div>';
	return htmlStr;
}
