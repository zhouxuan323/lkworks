 <%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
 <%
 String path = request.getContextPath();
 String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
 %>
<%@ taglib prefix="s" uri="/struts-tags"%>
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
     <base href="<%=basePath%>">
     
     <title>My JSP 'index.jsp' starting page</title>
     <meta http-equiv="pragma" content="no-cache">
     <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">    
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
     <meta http-equiv="description" content="This is my page">
     <!--
     <link rel="stylesheet" type="text/css" href="styles.css">
    -->
   </head>
  
   <body>
 
         <s:form action="upload" theme="simple" enctype="multipart/form-data">
             <table align="center" width="50%" border="0">
                 <tr>
                     <td>内容</td>
                     <td id="more1">
                        <%-- <s:text name="content"></s:text> --%>
                        <input type="text" name="contents" length=100>
                        <input type="button" value="增加" onclick="addMore1()">
                     </td>
                 </tr>
                 
                  <tr>
                     <td>图片</td>
                     <td id="more2">
                        <s:file name="file"></s:file>
                        <input type="button" value="增加" onclick="addMore2()">
                     </td>
                 </tr>
                 <tr>
                     <td><s:submit value=" 确认 "></s:submit></td>
                     <td><s:reset value=" 重置 "></s:reset></td>
                 </tr>
             </table>
        </s:form>
   </body>
 <script type="text/javascript">        
 function addMore1()
 {
     var td = document.getElementById("more1");
     
     var br = document.createElement("br");
     var input = document.createElement("input");
     var button = document.createElement("input");
     
     input.type = "text";
     input.name = "text";
    
     button.type = "button";
     button.value = "移除";
    
     button.onclick = function()
     {
         td.removeChild(br);
         td.removeChild(input);
         td.removeChild(button);
     }
     td.appendChild(br);
     td.appendChild(input);
     td.appendChild(button);
 }
 
 function addMore2()
 {
     var td = document.getElementById("more2");
     
     var br = document.createElement("br");
     var input = document.createElement("input");
     var button = document.createElement("input");
     
     input.type = "file";
     input.name = "file";
    
     button.type = "button";
     button.value = "移除";
    
     button.onclick = function()
     {
         td.removeChild(br);
         td.removeChild(input);
         td.removeChild(button);
     }
     td.appendChild(br);
     td.appendChild(input);
     td.appendChild(button);
 }
 </script>
 
 
</html>