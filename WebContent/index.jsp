<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">


<head>
<title>医生处方行为在线审核平台-登录</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript"src="resources/jquery/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="resources/jquery/jquery.form.js"></script>
<script type="text/javascript" src="resources/jquery/jquery.pager.js"></script>

<style type="text/css">
.style1 {
	font-size: 32px;
	float: left;
	font-family: 楷体;
	padding-left: 200px;
	padding-top: 35px;
	color: Gray;
	color: #0066CC;
	width: 750px;
	padding-bottom: 5px;
}

.style2 {
	font-size: 14px;
	float: right;
	font-family: 楷体;
	width: 740px;
	margin-top: 25px;
	margin-left: 220px;
}

.style3 {
	font-size: 14px;
	float: left;
	font-family: 楷体;
	width: 960px;
	height: 42px;
	line-height:42px;
}

.btnLogin {
	background-image: url(img/images/login_btn.gif);
	width: 124px;
	height: 25px;
}

#status {
	height: 35px;
}

.selectbox {
	width: 180px;
	height: 20px;
	line-height: 20px;
	color: #333;
	border: 1px solid #cacaca;
	background: #fff;
	padding: 0px;
	margin-right: 5px;
	display: inline;
	vertical-align: middle;
}
</style>
<script>
			// 如果浏览器窗口大小改变，则更新窗口大小
	$(function() {
		$('#content').css('margin-top', ($(window).height() - 380) / 2);
	});

	$(window).wresize(
			function() {
				$('#content').css('margin-top',($(window).height() - 380) / 2);
			});
		</script>
<script type="text/javascript">
</script>
</head>
<body id="cas" style="background-color: #0063b8; height: 100%;"
	class="login">

	<div id="content" style="margin-top: 200px;">
		<form id="loginForm" method="post"  action="loginAction.do">
		<div id="ContainerNoBImg" style="background-color: #0063b8;">
			<table align="center">
				<tr>
					<td>
						<div id="Content">
							<div id="main">
								<div
									style="background: url(img/images/login_bg.jpg); width: 960px; height: 380px;">
									<div class="style1">
										<strong style="margin-left: 20px;">林康信息技术有限公司网站后台管理<strong
											style="font-size: 14px; margin-left: 20px;"></strong></strong>
									</div>
									<div class="style2" style="height:223px;overflow:hidden;">
										
										<table align="center" style="width: 320px;height:190px;">
											<tr style="height: 70px;">
												<td style="width: 100px; text-align: right;"><label
													for="user.username">用户名:</label></td>
												<td style="width: 220px; text-align: center;">
													<div class="row">
														<input  name="username" tabindex="1"
															accesskey="n" type="text" value="" size="25"
															autocomplete="false" />
													</div>
												</td>
											</tr>


											<tr style="height: 50px;">
												<td style="width: 100px; text-align: right;"><label
													for="password">密 码:</label></td>
												<td style="width: 220px; text-align: center;">
													<div class="row">
														<input name="password" tabindex="2"
															accesskey="p" type="password" value="" size="25"
															autocomplete="off" />
													</div>
												</td>
											</tr>
											 <c:if test="${error!= null}">
											 	<tr style="height:auto;">
													<td style="width:100px;overflow:hidden;"></td>
													<td style="width:220px;overflow:hidden;line-height:20px;font-size:14px;color:red;"><strong>${error}</strong></td>
												</tr>
											 </c:if>
											 
											<tr style="height: 40px;">
												<td colspan="2" style="width: 300px;">
													<div class="row btn-row"
														style="margin-top: 20px; margin-left: 25px;">
														<input type="submit" class="btnLogin" id="btnlogin" name="submit"
															accesskey="l" value="登录" tabindex="2" 
															style="float: left; color: White; border-style: None; font-family: Broadway; text-decoration: none;" />&nbsp;
														<input class="btnLogin" id="chongzhi" name="chongzhi"
															accesskey="c" value="重置" tabindex="5" type="reset"
															style="color: White; border-style: None; font-family: Broadway; text-decoration: none;" />
													</div>
												</td>
											</tr>

										</table>
									</div>
									<div class="style3" >
										<table align="center" style="width:560px;">
											<tr>
												<td style="margin-left:10px;"><img src="img/images/LOGO.gif" width="174px;"height="33px"/></td>
												<td>Copyright © 2013-2014 LinkonWorks Inc.</td>
											</tr>
										</table>

									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>

		</form>
		
	</div>

<script type="text/javascript">
	$(function(){
		$("input").
	})
	
</script>

</body>
</html>