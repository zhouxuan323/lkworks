<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<title>上海林康医疗信息技术有限公司</title>
<meta name="keywords" content="上海林康医疗信息技术有限公司" />
<meta name="description" content="" />
<link rel="stylesheet" type="text/css" href="<%=path%>/css/css.css" />
<link rel="stylesheet" type="text/css" href="<%=path%>/css/common.css" />
<script type="text/javascript" src="<%=path%>/resources/jquery/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<%=path%>/resources/jquery/jquery.touchSlider.js"></script>
<script type="text/javascript" src="<%=path%>/resources/jquery/jquery.cxscroll.js"></script>
<script type="text/javascript">
	var path = '<%=path%>';
	var o_lng = 'cn';
	var o_isdel = '0';
	var o_current = 0;
	var o_pid;
	var o_pmid;
	var timer;
	var o_current_mid;
	var o_current_pmid;
	var flag = false;
	
	var url = location.search; 
	if (url.indexOf("?") != -1) {
		var str = "&";
		o_current_mid = url.split(str)[0].split("=")[1];
		o_current_pmid = url.split(str)[1].split("=")[1];
		o_lng =  url.split(str)[2].split("=")[1];
	}
	
	//alert(o_lng);
</script>
<script type="text/javascript" src="<%=path%>/js/main.js"></script>
<script type="text/javascript" src="<%=path%>/js/client.js"></script>
<script type="text/javascript">
	$(function() {
		loadMenu(o_lng, o_isdel, undefined);
		loadFoot(o_lng);
		$("#languageContainer ul li").click(function(){
			//clearInterval(timer);
			o_lng = $(this).attr("class");
			//alert(o_current);
			loadMenu(o_lng, o_isdel, undefined);
			loadFoot(o_lng);
		}); 
		
		

	});
</script>

</head>
<body>
	<!-- 头部开始 -->
	<div id="wrapper">
		<div id="gnbDiv">
			<div class="gnbDiv">
				<div class="gnbWrap">
					<h1 class="logo">
						<img class="cursor" src="<%=path%>/img/logo.jpg" width="237" height="79"  style="cursor:pointer"/>
					</h1>
					<div id="wrap">
						<div id="main_nav"></div>
						<!-- <!-- <div class="clear"></div> -->
						<nav id="languageContainer">
							<ul>
								<li class="en" style="cursor:pointer;line-height:15px;"><a>english</a></li>
								<li class="cn" style="cursor:pointer"><a>中国的</a></li>
							</ul>
						</nav>
					</div>
				</div>
				<div class="twoDbg" style="display:"></div>
			</div>
		</div>
	</div>

	
	<!-- 头部结束 -->
	<div id="r_con1" class="main_visual"><div class="flicking_con"><div class="flicking_inner"></div></div><div class="main_image"><a href="javascript:;" id="btn_prev"></a> <a href="javascript:;"id="btn_next"></a></div></div>
	
	<div id="r_con2" class="Content"><div class="Introduction"><h3><span ></span><a href="" target="_blark"> <img src="<%=path%>/img/more.jpg" width="63" height="20" /> </a></h3><a ></a></div>
		<center id="r_con3" ><div id="pic_list_2" class="scroll_horizontal"><div class="box"></div></div></center>
	</div>
	<!-- 底部开始-->
	
	<!-- 底部结束 -->
</body>
</html>